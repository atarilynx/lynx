#define DOZE

#include <stdio.h>
#include <stdlib.h>
#ifdef UNIX
#  include <fcntl.h>
#endif

#include <stdarg.h>
#include <string.h>

#ifdef UNIX
#  include <unistd.h>
#endif

#ifdef DOZE
#  include <io.h>
#  include <sys\stat.h>
#  include <fcntl.h>
#  include <ctype.h>
#endif

#ifndef O_BINARY
#  define O_BINARY 0
#endif