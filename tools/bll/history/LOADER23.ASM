;-
;neuer Loader f�r 'gro�e' Karten
; als 'Titel-Sprite' nach $200 geladen
;-
; letzte �nderung : 130993
; 2.1	Titel-Bild ein- und ausFADEn
; 2.2	Anpassung an 512er-EPROM-Karten (AUDIOin sel. obere Bank)
; 	AUDIOin als Ausgang und log. high schalten 
; 2.3	Loader verk�rzt / Check rausgeworfen
;	Entpacker f�r TP_GEM-gepackte Programme eingebaut

	ORG $0
CurrBlock
DirSpace	ds 8
DestPtr	dw 0
BlockByte	dw 0
;--------------------
file	ds 2
count	ds 2
byte	ds 1
offset	ds 2
TempPtr	ds 2
laenge	ds 2
;--------------------
BlockSize	equ $Fc	; 1024 Bytes
BlockSize4	equ $FE
;BlockSize	equ $F8	; 2048 Bytes
;BlockSize4	equ $FC
;CHECK	SET 1
	run $200	; ab hier geht's ab
start
IFD CHECK
	jsr Check
ENDIF
	ldx #%11110111	; 2. DIR-Eintrag lesen
	jsr ReadDir
;***************
;* Datei laden
;***************
	jsr SelectBlock
	ldx DirSpace+1
	ldy DirSpace+2
	jsr ReadOver
	jsr ReadByte
	sta DirSpace+5
	sta DestPtr+1
	jsr ReadByte
	sta DirSpace+4
	sta DestPtr	; Zieladresse
	jsr ReadByte
	sta laenge+1
	jsr ReadByte
	sta laenge
ReadPackedBytes::stz count
.loop0	asl count
	bne .loop1
	jsr ReadByte
	sta byte
	dec count
.loop1	asl byte
	bcs .cont0
;---------------
	jsr ReadByte
	jsr StoreByte
	bra .loop0
;---------------
.cont0	jsr ReadByte
	stz offset+1
	tay
	ldx #4
.loop12	  asl
	  rol offset+1
	  dex
	bne .loop12
	jsr ReadByte
	sta offset
	sec
	lda DestPtr
	sbc offset
	sta TempPtr
	lda DestPtr+1
	sbc offset+1
	sta TempPtr+1
	tya
	ldy #0
	and #$f
	inc
	inc
	tax
.loop2	lda (TempPtr),y
	jsr StoreByte
	iny
	dex
	bpl .loop2
	bra .loop0
	
;***************
;* Bytes '�berlesen'
;***************
ReadOver::	inx
	bne .cont
	iny
	beq .exit
.cont	jsr ReadByte
	bra ReadOver
;***************
;* DIR-Eintrag lesen
;***************
ReadDir	stz CurrBlock
	jsr SelectBlock
	stz DestPtr
	stz DestPtr+1
	ldy #$FF-2
	jsr ReadOver
	ldx #$ff-8
	ldy #$ff
;***************
;* Bytes nach DestPtr laden
;***************
ReadBytes	inx
	bne .cont1
	iny
	beq .exit
.cont1	jsr ReadByte	; ein Byte von der Karte
	sta (DestPtr)
	inc DestPtr
	bne ReadBytes
	inc DestPtr+1
	bra ReadBytes
;***************
;* Byte von Karte lesen
;***************
ReadByte	lda $fcb2
	inc BlockByte
	bne .exit
	inc BlockByte+1
	bne .exit
;***************
;* Block anw�hlen
;***************
SelectBlock	pha
	phx
	phy
	lda CurrBlock
	inc CurrBlock
	ldy #$1c
	ldx #$1e
	SEC
	BRA SBL2
SLB0	BCC SLB1
	STX $FD8B
	CLC
SLB1	INX
	STX $FD87
	DEX
SBL2	STX $FD87
	ROL
	STY $FD8B
	BNE SLB0
	stz BlockByte
	lda #BlockSize
	sta BlockByte+1
	ply
	PLX
	pla
.exit	RTS
StoreByte	sta (DestPtr)
	inc DestPtr
	bne .cont3
	inc DestPtr+1
.cont3	inc laenge
	bne .exit
	inc laenge+1
	bne .exit
IFD CHECK
	jmp (DirSpace+4)
;***************
;* Checksumme �ber die gesammte Karte
;***************
Check	ldx #7
.loop	  stz CurrBlock,x
	  dex
	bpl .loop
	jsr SelectBlock
.loop0	  inc DirSpace+5
	  bne .cont2
	  inc DirSpace+6
	  bne .cont2
	    lda DirSpace+1
	    ora DirSpace+2
	    ora DirSpace+3
	    ora DirSpace+4
	    beq .exit
	    jmp ($fff8)	; RESET
.cont2	  ldx #3
.loop1	    jsr ReadByte
	    pha
	    dex
	  bpl .loop1
	  clc
	  ldx #1
	  ldy #3
.loop2	     pla
	     adc DirSpace,x
	     inx
	     dey
	  bpl .loop2
	bra .loop0
ELSE
;-- Sprite ausblenden
FadeOut::	lda #$f
	sta 0
.loop1	ldx #$f
.loop2	  lda $fda0,x
	  beq .cont01
	  dec $fda0,x
	  bmi .cont2
.cont01	  lda $fdb0,x
	  tay
	  and #$f
	  beq .cont02
	  dec
	  bmi .cont2
.cont02	  sta 1
	  tya
	  and #$f0
	  beq .cont03
	  sec
	  sbc #$10
	  bcc .cont2
.cont03	  ora 1
	  sta $fdb0,x
	  bra .cont3
.cont2	  stz $fda0,x
	  stz $fdb0,x
.cont3	  dex
	bpl .loop2
	ldy #80
.wait	lda $fd0a
	bne .wait
	dey
	bne .wait
	dec 0
	bne .loop1
	jmp (DirSpace+4)
ENDIF
;---------------

frei	set $341-12-*
	IF frei>0
	ds frei
echo "Bereich 1 : %Dfrei Bytes frei !"
	ENDIF
;***************
;* Reste des alten BootLoaders
;***************
_3D9	LDA $FCB2
	STA ($46)
	INC $46
	BNE _3E4
	INC $47
_3E4	RTS
;---------------
_341	INC $50	; Dateil�nge
	BNE _349
	INC $51
	BEQ _35A	; fertig =>
_349	JSR _3D9	; READ BYTE
	INX
	BNE _341
	INY
	BNE _341
;***************
;* ab hier frei
;***************
_35A::	lda #$1a
	sta $fd8a
	lda #1
	sta $fc90
	ldx #$ff	; 1. DIR-Eintrag lesen
	jsr ReadDir
	stz DestPtr
	lda #$24
	sta DestPtr+1
	jsr SelectBlock
	ldx DirSpace+1
	ldy DirSpace+2
	jsr ReadOver
	ldx DirSpace+6
	ldy DirSpace+7
	jsr ReadBytes	; Sprite laden

	stz $fd94
	lda #4
	sta $fd95
	ldx #$1f	; Farben setzen
.loop1	  lda $2400,x
	  sta $fda0,x
	  dex
	bne .loop1

	ldx #10
.loop	  lda Val,x
	  ldy Reg,x
	  sta $fc00,y
	  dex
	bpl .loop
	STZ $FD90
.WAIT	STZ $FD91
	nop
	LDA $FC92
	LSR
	BCS .WAIT
	STZ $FD90
	jmp start
;***************


Reg	db $91,$11,$10,$09,$08,$28,$2a,$04,$06,$92,$90
Val	db $01,$24,$20,$04,$00,$7f,$7f,$00,$00,$20,$01
	db ^^DATE
	db ^^TIME
	db "Loader 2.3 (c) 1993 Bastian Schick"
;***************
;* ein paar Infos
;***************
frei	set frei+$400-8-*


	IF _341<>$341
	  echo "Fehler bei _341 !! : %H_341"
	ENDIF
	IF (*-$200)>($200-8)
	echo "Loader zu lang !!"
	else	
echo "Insgesammt noch %Dfrei Bytes frei !"
	endif
	END
;****************************************************************************
