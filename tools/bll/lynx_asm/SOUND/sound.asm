***************
* SOUND.ASM
* simple body of a Lynx-program
*
* created : 5.3.97
*
****************

                path "e:\bll_work\"             ; global path
                
BRKuser         set 1
Baudrate        set 62500

SND_TIMER       set 7

                path "macros"
                isyms "hardware.sym"            ; get hardware-names
****************
* macros
                include "help.mac"
                include "if_while.mac"
                include "font.mac"
                include "mikey.mac"
                include "suzy.mac"
                include "irq.mac"
                include "newkey.mac"
                include "debug.mac"
****************
* variables
                path "..\vardefs\"
                include "debug.var"
                include "help.var"
                include "font.var"
                include "mikey.var"
                include "suzy.var"
                include "irq.var"
                include "newkey.var"
                include "serial.var"
****************
BEGIN_ZP
t1              ds 1
t2              ds 2
VBLsema         ds 1

SndSema         ds 1            ; <>0 => Sound IRQ in progress
SndPtrTmp       ds 2
SndTmp          ds 2

SndEnvPtr       ds 2
END_ZP


BEGIN_MEM
                ALIGN 4
screen0         ds SCREEN.LEN
screen1         ds SCREEN.LEN
irq_vektoren    ds 16

SndVol          ds 4
SndFrq          ds 4
SndPtrLo        ds 4
SndPtrHi        ds 4

SndNoteCnt      ds 4            ; counts note-lenght
SndLoopCnt      ds 4
SndLoopPtrLo    ds 4
SndLoopPtrHi    ds 4

SndActive       ds 4            ; <>0 => channel active
SndReqStop      ds 4
SndEnvVol       ds 4            ; <>0 => use envelope x
SndEnvFrq       ds 4            ; dito
SndEnvPly       ds 4            ; dito

* volume env.
SndEnvVolCnt    ds 4
SndEnvVolInc    ds 4
SndEnvVolOff    ds 4
SndEnvVolSve    ds 4
SndMinVol       ds 4
SndMaxVol       ds 4
SndEnvVolLoop   ds 4
SndEnvVolParts  ds 4


*
* frequenz env.
SndEnvFrqCnt    ds 4
SndEnvFrqInc    ds 4
SndEnvFrqOff    ds 4
SndEnvFrqSve    ds 4
SndMinFrq       ds 4
SndMaxFrq       ds 4
SndEnvFrqLoop   ds 4
SndEnvFrqParts  ds 4

*

SndEnvVolPtrLo  ds 16
SndEnvVolPtrHi  ds 16
SndEnvFrqPtrLo  ds 16
SndEnvFrqPtrHi  ds 16
SndEnvPlyPtrLo  ds 16
SndEnvPlyPtrHi  ds 16

END_MEM
                run LOMEM       ; code directly after variables
*
* system init
Start::         START_UP             ; Start-Label needed for reStart
                CLEAR_MEM
                CLEAR_ZP +STACK ; clear stack ($100) too

                INITMIKEY
                INITSUZY
                FRAMERATE 60
                SETRGB pal
                INITIRQ irq_vektoren
                INITBRK                         ; if we're using BRK #X, init handler
                INITKEY ,_FIREA|_FIREB          ; repeat for A & B
                INITFONT LITTLEFNT,RED,WHITE
                jsr InitComLynx
                SETIRQ 2,VBL
                SETIRQ 0,HBL

                cli
                SCRBASE screen0
                SET_MINMAX 0,0,160,102
                jsr SndInit

*
Main::
                LDAY DefEnv
                ldx #0
                jsr SndStartSound

.loop           stz CurrX
                stz CurrY
                ldx #3
.0                lda SndNoteCnt,x
                  jsr PrintHex
                  dex
                bpl .0
                stz CurrX
                lda #20
                sta CurrY
                lda t2
                jsr PrintHex
                lda t1
                jsr PrintHex

                stz CurrX
                lda #30
                sta CurrY
                ldx #1
.1
                lda SndVol,x
                jsr PrintHex

                lda SndEnvFrq,x
                jsr PrintHex

                lda SndEnvFrqCnt,x
                jsr PrintHex

                lda SndEnvFrqOff,x
                jsr PrintHex

                lda SndEnvFrqLoop,x
                jsr PrintHex
                lda SndMaxFrq,x
                jsr PrintHex
                inc CurrX
                inc CurrX
                dex
                bpl .1

                jsr ReadKey
                beq .loop
                stz t1
                stz t2
                lda CurrentButton
                bit #_FIREA
                _IFNE
                  pha
                  LDAY TestSnd
                  ldx #0
                  jsr SndStartSound
                  LDAY TestSnd2
                  ldx #2
                  jsr SndStartSound
                  pla
                _ENDIF
                bit #_FIREB
                _IFNE
                  pha
                  LDAY TestSnd1
                  ldx #1
                  jsr SndStartSound
                _ENDIF
                jmp .loop
*
DefEnv::
                dc.b 8,1,0,$ff,0,2
                dc.b 40,1
                dc.b 10,20

                dc.b 8,2,0,$ff,0,2
                dc.b 10,-10
                dc.b 10,10

                dc.b 5,1,0,$7f,99,3
                dc.b 5,10
                dc.b 10,-10
                dc.b 5,10
                dc.b 0


TestSnd
                dc.b 9,1
                dc.b 6,1
                dc.b 1,50,%100,100,1,40
                dc.b 1,50,%100,80,1,40
                dc.b 1,50,%100,60,1,40
                dc.b 9,2
                dc.b 1,50,%100,40,1,40
                dc.b 1,50,%100,20,1,40
                dc.b 0

TestSnd2        dc.b 9,1
                dc.b 6,1
                dc.b 1,50,%100,101,1,40
                dc.b 1,50,%100,81,1,40
                dc.b 1,50,%100,61,1,40
                dc.b 9,2
                dc.b 1,50,%100,41,1,40
                dc.b 1,50,%100,21,1,40
                dc.b 0

TestSnd1:
                dc.b 6,1
                dc.b 1,250,%100,20,1,80
                dc.b 0


****************
SndInit::
                php
                sei
                lda #%10011000|_31250Hz
                sta $fd01+SND_TIMER*4
                lda #129
                sta $fd00+SND_TIMER*4           ; set up a 240Hz IRQ
                SETIRQVEC SND_TIMER,SndIRQ

                stz $fd20
                stz $fd28
                stz $fd30
                stz $fd38       ; all volumes zero

                stz $fd44       ; all channels full volume
                lda #$ff
                stz $fd50       ; channels off

                lda #%01011000
                sta $fd20+5
                sta $fd28+5
                sta $fd30+5
                sta $fd38+5

                ldx #3
.0                stz SndActive,x
                  stz SndReqStop,x
                  stz SndEnvVol,x
                  stz SndEnvFrq,x
                  stz SndEnvPly,x
                  dex
                bpl .0
                stz SndSema
                plp
                rts

SndVolOff:      dc.b $0,$8,$10,$18
SndShftOff      dc.b $1,$8+1,$10+1,$18+1
SndFrqOff       dc.b $4,$8+4,$10+4,$18+4
SndCntrlOff     dc.b $5,$8+5,$10+5,$18+5


SndIRQ::
inc t1
lda t1
cmp #250
_IFEQ
inc t2
stz t1
_ENDIF

                lda #$ff
                tsb SndSema
                _IFNE
                  dec $fda0
                  END_IRQ
                _ENDIF
                phy
                cli
                ldx #3
.0                phx
                  lda SndActive,x
                  _IFNE
                    _IFNE {SndEnvVol,x}
                      jsr SndChangeVol
                    _ENDIF
                    _IFNE {SndEnvFrq,x}
                      jsr SndChangeFrq
                    _ENDIF
                    dec SndNoteCnt,x
                    _IFEQ
                      _IFNE {SndReqStop,x}
                        stz SndActive,x
                      _ELSE
                        jsr SndGetCmd
                      _ENDIF
                    _ENDIF
                  _ENDIF
                  plx
                  dex
                bpl .0
                ply
                stz SndSema
                END_IRQ

SndGetCmd::
                lda SndPtrLo,x
                sta SndPtrTmp
                lda SndPtrHi,x
                sta SndPtrTmp+1

.0              lda (SndPtrTmp)
                tay
                jsr SndCallCmd

                clc
                tya
                and #$7f
                adc SndPtrTmp
                sta SndPtrLo,x
                sta SndPtrTmp

                lda #0
                adc SndPtrTmp+1
                sta SndPtrHi,x
                sta SndPtrTmp+1

                tya
                bmi .0
                rts

SndCallCmd::    lda SndCmdsHi,y
                pha
                lda SndCmdsLo,y
                pha
                ldy #1
                rts
*
SndStop::
                  stz SndActive,x
                  ldy SndVolOff,x
                  lda #0
                  sta $fd20,y
                  stz SndVol,x
                  rts
*
SndNewNote::
                lda (SndPtrTmp),y
                  sta SndNoteCnt,x
                  iny
                  lda (SndPtrTmp),y             ; 7EEEEppp
                  pha
                   iny
                   lda (SndPtrTmp),y             ; reload (freq)
                   pha
                    iny
                    lda (SndPtrTmp),y             ; shifter feedback
                    pha
                     iny
                     lda (SndPtrTmp),y             ; volume
                     ldy SndVolOff,x
                     sta $fd20,y
                     sta SndVol,x
                    pla
                    ldy SndShftOff,x
                    sta $fd20,y
                   pla
                   ldy SndFrqOff,x
                   sta $fd20,y
                   sta SndFrq,x
                   ldy SndCntrlOff,x
                   lda $fd20,y
                   and #$78
                   sta SndTmp
                  pla
                  and #$87
                  ora SndTmp
                  sta $fd20,y
                  _IFNE {SndEnvVol,x}
                    jsr SndSetEnvVol1
                  _ENDIF
                  _IFNE {SndEnvFrq,x}
                    jsr SndSetEnvFrq1
                  _ENDIF
                  ldy #6

                  rts
*
SndLoop::
                lda (SndPtrTmp),y
                sta SndLoopCnt,x
                lda SndPtrTmp
                sta SndLoopPtrLo,x
                lda SndPtrTmp+1
                sta SndLoopPtrHi,x
                ldy #$82
                rts
*
SndDo::
                dec SndLoopCnt,x
                _IFNE
                  lda SndLoopPtrLo,x
                  sta SndPtrTmp
                  lda SndLoopPtrHi,x
                  sta SndPtrTmp+1
                  ldy #$82
                _ELSE
                  ldy #$81
                _ENDIF
                rts
*
SndChgNote::
                  lda (SndPtrTmp),y
                  sta SndNoteCnt,x
                  iny
                  lda (SndPtrTmp),y
                  ldy SndFrqOff,x
                  sta SndFrq,x
                  sta $fd20,y
                  ldy #3
                  rts
*
SndDefEnvVol::
*
                lda (SndPtrTmp),y               ; env #
                tay

                lda SndPtrTmp
                sta SndEnvVolPtrLo,y
                lda SndPtrTmp+1
                sta SndEnvVolPtrHi,y            ; Ptr to [inc,cnt]

                ldy #5
                lda (SndPtrTmp),y               ; parts
                asl
;>                clc                           ; # parts <= 122 => C = 0
                adc SndPtrTmp
                sta SndPtrTmp
                lda #0
                adc SndPtrTmp+1
                sta SndPtrTmp+1
                ldy #$86
                rts
*
SndSetEnvVol::
                lda (SndPtrTmp),y               ; # env
                sta SndEnvVol,x                 ; save
                _IFEQ
                  ldy #$82
                  rts
                _ENDIF

SndSetEnvVol1   and #$7f
                sta SndEnvVol,x                 ; save

                tay

                lda SndEnvVolPtrLo,y
                sta SndEnvPtr
                lda SndEnvVolPtrHi,y
                sta SndEnvPtr+1

                ldy #2
                lda (SndEnvPtr),y
                sta SndMinVol,x
                iny
                lda (SndEnvPtr),y
                sta SndMaxVol,x
                iny
                lda (SndEnvPtr),y
                sta SndEnvVolLoop,x
                iny
                lda (SndEnvPtr),y
                sta SndEnvVolParts,x

                lda #1
                sta SndEnvVolCnt,x
                lda #6
                sta SndEnvVolOff,x
                lda SndVol,x
                sta SndEnvVolSve,x

                ldy #$82
                rts
*
SndChgVol::
                lda (SndPtrTmp),y
                sta SndVol,x
                ldy SndVolOff,x
                sta $fd20,y
                ldy #$82
                rts
*
SndDefEnvFrq::
*
                lda (SndPtrTmp),y               ; env #
                tay

                lda SndPtrTmp
                sta SndEnvFrqPtrLo,y
                lda SndPtrTmp+1
                sta SndEnvFrqPtrHi,y            ; Ptr to [inc,cnt]

                ldy #5
                lda (SndPtrTmp),y
                asl
;>                clc
                adc SndPtrTmp
                sta SndPtrTmp
                lda #0
                adc SndPtrTmp+1
                sta SndPtrTmp+1
                ldy #$86
                rts
*
SndSetEnvFrq::
                lda (SndPtrTmp),y               ; # env
                sta SndEnvFrq,x                 ; save
                _IFEQ
                  ldy #$82
                  rts
                _ENDIF

SndSetEnvFrq1   and #$7f
                sta SndEnvFrq,x                 ; save

                tay

                lda SndEnvFrqPtrLo,y
                sta SndEnvPtr
                lda SndEnvFrqPtrHi,y
                sta SndEnvPtr+1

                ldy #2
                lda (SndEnvPtr),y
                sta SndMinFrq,x
                iny
                lda (SndEnvPtr),y
                sta SndMaxFrq,x
                iny
                lda (SndEnvPtr),y
                sta SndEnvFrqLoop,x
                iny
                lda (SndEnvPtr),y
                sta SndEnvFrqParts,x

                lda #1
                sta SndEnvFrqCnt,x
                lda #6
                sta SndEnvFrqOff,x
                lda SndFrq,x
                sta SndEnvFrqSve,x

                ldy #$82
                rts

*

*
SndCmdsHi::     dc.b >SndStop,>SndNewNote,>SndLoop
                dc.b >SndDo,>SndChgNote,>SndDefEnvVol
                dc.b >SndSetEnvVol,>SndChgVol,>SndDefEnvFrq
                dc.b >SndSetEnvFrq

SndCmdsLo::     dc.b <(SndStop-1),<(SndNewNote-1) ,<(SndLoop-1)
                dc.b <(SndDo-1)       ,<(SndChgNote-1) ,<(SndDefEnvVol-1)
                dc.b <(SndSetEnvVol-1),<(SndChgVol-1),<(SndDefEnvFrq-1)
                dc.b <(SndSetEnvFrq-1)

*
SndChangeVol::
                tay
                _IFMI
                  rts
                _ENDIF
                lda SndEnvVolPtrLo,y
                sta SndEnvPtr
                lda SndEnvVolPtrHi,y
                sta SndEnvPtr+1

                dec SndEnvVolCnt,x
                _IFEQ
                  dec SndEnvVolParts,x
                  _IFMI
                    _IFNE {SndEnvVolLoop,x}
                      dec SndEnvVolLoop,x
                      ldy #5
                      lda (SndEnvPtr),y
                      sta SndEnvVolParts,x
                      iny
                      tya
                      sta SndEnvVolOff,x
                      bra .1
                    _ELSE
                      tya
                      ora #$80
                      sta SndEnvVol,x
                    _ENDIF
                  _ELSE
.1                  ldy SndEnvVolOff,x
                    lda (SndEnvPtr),y
                    _IFEQ
                      lda SndEnvVolSve,x
                      sta SndVol,x
                      phy
                      ldy SndVolOff,x
                      sta $fd20,y
                      lda #0
                      ply
                    _ENDIF
                    inc
                    sta SndEnvVolCnt,x
                    iny
                    lda (SndEnvPtr),y
                    sta SndEnvVolInc,x
                    iny
                    tya
                    sta SndEnvVolOff,x
                  _ENDIF
                _ELSE
                  clc
                  lda SndVol,x
                  adc SndEnvVolInc,x
                  cmp SndMaxVol,x
                  _IFGE
                    lda SndMaxVol,x
                  _ELSE
                    cmp SndMinVol,x
                    _IFLT
                      lda SndMinVol,x
                    _ENDIF
                  _ENDIF
                  sta SndVol,x
                  ldy SndVolOff,x
                  sta $fd20,y
                _ENDIF
                rts

*
SndChangeFrq::
                tay
                _IFMI
                  rts
                _ENDIF
                lda SndEnvFrqPtrLo,y
                sta SndEnvPtr
                lda SndEnvFrqPtrHi,y
                sta SndEnvPtr+1

                dec SndEnvFrqCnt,x
                _IFEQ
                  dec SndEnvFrqParts,x
                  _IFMI
                    _IFNE {SndEnvFrqLoop,x}
                      dec SndEnvFrqLoop,x
                      ldy #5
                      lda (SndEnvPtr),y
                      sta SndEnvFrqParts,x
                      iny
                      tya
                      sta SndEnvFrqOff,x
                      bra .1
                    _ELSE
                      tya
                      ora #$80
                      sta SndEnvFrq,x
                    _ENDIF
                  _ELSE
.1                  ldy SndEnvFrqOff,x
                    lda (SndEnvPtr),y
                    inc
                    sta SndEnvFrqCnt,x
                    iny
                    lda (SndEnvPtr),y
                    sta SndEnvFrqInc,x
                    iny
                    tya
                    sta SndEnvFrqOff,x
                  _ENDIF
                _ELSE
                  clc
                  lda SndFrq,x
                  adc SndEnvFrqInc,x
                  cmp SndMaxFrq,x
                  _IFGE
                    lda SndMaxFrq,x
                  _ELSE
                    cmp SndMinFrq,x
                    _IFLT
                      lda SndMinFrq,x
                    _ENDIF
                  _ENDIF
                  sta SndFrq,x
                  ldy SndFrqOff,x
                  sta $fd20,y
                _ENDIF
                rts


*
SndStartSound::
*
                php
                pha
                _IFNE {SndActive,x}
                  dec SndReqStop,x
                  lda #1
                  sta SndNoteCnt,x
.0                lda SndActive,x
                  bne .0
                _ENDIF
                sei
                pla
                sta SndPtrLo,x
                tya
                sta SndPtrHi,x
                lda #1
                sta SndNoteCnt,x
                stz SndEnvVol,x
                stz SndEnvFrq,x
                stz SndEnvPly,x
                sta SndActive,x
                stz SndReqStop,x
                plp
                rts
****************
VBL::           lda #$ff
                tsb VBLsema
                _IFEQ
                  cli
                  jsr Keyboard                    ; read buttons
                  stz VBLsema
                _ENDIF
                stz $fdb0
                END_IRQ
                
HBL::
                dec $fdb0
                END_IRQ
****************
* INCLUDES
                path "..\includes\"
                include "draw_spr.inc"
                include "hexdez.inc"
                include "serial.inc"
                include "debug.inc"
                include "font.inc"
                include "irq.inc"
                include "font2.hlp"
                include "newkey.inc"

****************
pal             STANDARD_PAL
