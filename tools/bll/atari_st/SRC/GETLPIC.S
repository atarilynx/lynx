* GETLynxPICture
* 27.9.94
* 21.06.95        Farbpalette am Anfang
*                 nur noch 102 Zeilen
                  OUTPUT 'F:\LYNX\GETLPIC.TTP'
                  TEXT
                  >PART 'test_cmdline'
start:            lea     start-128(pc),a0
                  moveq   #0,d0
                  move.b  (a0)+,d0
                  beq.s   no_cmd
                  clr.b   0(a0,d0.w)
                  cmpi.b  #" ",(a0)
                  beq.s   no_cmd
                  lea     fn(pc),a1
loop:             move.b  (a0)+,(a1)+
                  bne.s   loop
no_cmd:
                  ENDPART
                  >PART 'A6=PhysBase'
                  move.w  #2,-(sp)
                  trap    #14
                  addq.l  #2,sp
                  movea.l d0,a6
                  ENDPART
                  >PART 'clear'
clear:            pea     $010001
                  trap    #13
                  addq.l  #4,sp
                  tst.w   d0
                  beq.s   end_clear
                  pea     $020001
                  trap    #13
                  addq.l  #4,sp
                  bra.s   clear
end_clear:
                  ENDPART
                  >PART 'GetPix'
                  move.w  #$81,d0
                  bsr.s   SendD0
                  bsr     ReceiveD0
                  moveq   #"S",d0
                  bsr.s   SendD0
wait_S:           bsr.s   ReceiveD0
                  cmp.b   #"S",d0
                  bne.s   wait_S
                  IF 0
wait:             bsr.s   ReceiveD0
                  beq.s   exit
                  addq.b  #1,d0
                  bne.s   wait             ; auf $FF warten
                  bsr.s   ReceiveD0
                  beq.s   exit
                  move.b  d0,d7
                  bsr.s   ReceiveD0
                  beq.s   exit
                  lsl.w   #8,d7
                  move.b  d0,d7            ; Anzahl der Zeilen
                  ELSE
                  move.w  #102,d7
                  ENDC
                  lea     puffer(pc),a0
;                  move.w  d7,(a0)+
                  subq.w  #1,d7
                  moveq   #31,d1	;load LYNX palette
loopc:            bsr.s   ReceiveD0
                  beq.s   exit
                  move.b  d0,(a0)+
                  dbra    d1,loopc
                                                                                                                                                                                                                                                               
 
loop0:            moveq   #79,d1           ; 80 Bytes
loop1:            bsr.s   ReceiveD0
                  beq.s   exit
                  move.b  d0,(a0)+
                  move.b  d0,(a6)+
                  dbra    d1,loop1
                  dbra    d7,loop0
                  ENDPART
                  >PART 'SavePix'
                  move.l  a0,d7
                  clr.w   -(sp)
                  pea     fn(pc)
                  move.w  #60,-(sp)
                  trap    #1
                  addq.l  #8,sp
                  move.w  d0,d6
                  bmi.s   exit
                  lea     puffer(pc),a0
                  pea     (a0)
                  sub.l   a0,d7
                  move.l  d7,-(sp)
                  move.w  d6,-(sp)
                  move.w  #64,-(sp)
                  trap    #1
                  lea     12(sp),sp
                  move.w  d6,-(sp)
                  move.w  #62,-(sp)
                  trap    #1
                  addq.l  #4,sp
                  ENDPART
                  >PART 'exit'
exit:             clr.w   -(sp)
                  trap    #1
                  ENDPART
                  >PART 'SendD0'
SendD0:           movem.l d0-d2/a0-a2,-(sp)
                  move.w  d0,-(sp)
                  pea     $030001
                  trap    #13
                  addq.l  #6,sp
                  movem.l (sp)+,d0-d2/a0-a2
                  rts
                  ENDPART
                  >PART 'ReceiveD0'
ReceiveD0:        movem.l d1-d2/a0-a2,-(sp)
loop_r:           pea     $010001
                  trap    #13
                  addq.l  #4,sp
                  tst.w   d0
                  bne.s   ok_r

                  pea     $0600FF
                  trap    #1
                  addq.l  #4,sp
                  cmp.b   #27,d0
                  bne.s   loop_r
                  moveq   #0,d0
                  movem.l (sp)+,d1-d2/a0-a2
                  rts
ok_r:             pea     $020001
                  trap    #13
                  addq.l  #4,sp
                  moveq   #-1,d1
                  movem.l (sp)+,d1-d2/a0-a2
                  rts
                  ENDPART
fn:               DC.B "f:\lynx.pil",0
                  EVEN
                  BSS
                  DS.B 128
puffer:           DS.B 512*80
                  END
