/* lynx sprite packer
	 written 97/02/17 42Bastian Schick

	 last change : 97/02/20

*/

#include <stdio.h>
#include <stdlib.h>

#define BYTE unsigned char
/*#define DEBUG*/

/* function-prototypes */
void   error(char *w);
void 	 SaveSprite(char *filename,BYTE *data,int len);
long	 LoadFile(char *filename,BYTE **adr);

BYTE * intobyte(int bits,BYTE val,BYTE *where);
BYTE * packline(BYTE *in,BYTE *out,int len,int size,int dir);
BYTE * unpackline(BYTE *in,BYTE *out,int len,int size,int dir);
void   intobuffer(BYTE *in,BYTE *buf,int len,int size);
int    packit(BYTE *raw,int  iw, /*int  ih,*/ BYTE **spr,
              BYTE size,int  packed,
				 			int  w,int  h,int  act_x,int act_y);

/*
   insert 'bits' bits from 'val' into a byte
   if a byte is complete, write it to 'where'
*/
BYTE * intobyte(int bits,BYTE val,BYTE *where)
{
static BYTE bit_counter = 8,byte =0;

	if ( !bits ){
		bit_counter = 8;
		byte =0;
		return NULL;
	}

	if ( bits == 8){
			byte <<= bit_counter;
			*where++ = byte;
			if (byte & 0x1f )
				*where++ = 0;
		return (where);
	}


	val <<= 8-bits;

	do
	{
		byte <<= 1;

		if ( val & 0x80  )
			++byte;

		if (! (--bit_counter) )
		{
			*where++ = byte;
			byte = 0;
			bit_counter = 8;
		}

		val  <<= 1;

	}while(--bits);

	return (where);
}
/*
   packe one line either to the left or right
*/
BYTE * packline(BYTE *in,			/* src  */
								BYTE *out,    /* dest.*/
								int len,      /* ???  */
								int size,     /* bits/pel */
								int dir)      /* -1 = left */
{
	BYTE differ[16],	          /* buffer for literal bytes */
	     *d_ptr,								/* ptr in this buffer       */
	     *out0,							    /* to save the dest.        */
	     last;									/* last byte from src       */
	     
	int  counter;								/* counts the bytes         */

	out0  = out++;							/* save dest. ptr           */
	*out0 = 0;									/* set byte count to zero   */

	intobyte(0,0,0);						/* init.                    */

	/* pack-loop */
	while ( len ){
		last = *in;								/* get a byte               */
		in += dir;							  /* step                     */
		--len;
		counter = 0;							/* init counter             */
		
		if ( (last == *in) && len)
		{
		/**/
		/* count the equal bytes, up to 16 */
		/**/
		
			do{
				++counter;
				in += dir;
				--len;
			}while ( last == *in && len && counter != 15);
			
			out = intobyte(5,counter,out);
			out = intobyte(size,last,out);
			
		}else{
		
		/**/
		/* count different bytes , up to 16 */
		/**/
		
			d_ptr = differ;
			while ( last != *in && len && counter != 15)
			{
				*d_ptr++ = last ; ++counter;
				last = *in;
				in += dir; --len;
				
			}/*while*/
			if ( !len || counter == 15)
				*d_ptr = last;
			else	
				if ( last == *in )
				{
					--counter;
					in -=dir;
					++len;
				}
				  
			out = intobyte(5,counter|0x10,out);
			d_ptr = differ;
			do{
				out = intobyte(size,*d_ptr++,out);
			}while (--counter >= 0);
			
		}/* if last == *in */
	}/* while */

	/* exit */
	out = intobyte(8,0,out);

	*out0 = out-out0;
	return out;
}

/*
	 pack the raw data into a literal sprite line
*/
BYTE * unpackline(BYTE *in,
									BYTE *out,
									int len,
									int size,
									int dir)
{
	BYTE *out0 = out++;

	intobyte(0,0,0);

	while (len){
		out = intobyte(size,*in,out);
		in += dir;
		--len;
	}
	out = intobyte(8,0,out);

	*out0 = out-out0;
	return out;
}
/*
	convert a line of raw data into a line of pixels
	reduced to the current bit-size
*/
void intobuffer(BYTE *in,
								BYTE *buf,
								int len,
								int size)

{
int bit_mask = (1<<size)-1;

		while ( len ){
			*buf++ = (*in>>4) & bit_mask;
			if (! (--len) ) break;
			*buf++ = (*in++) & bit_mask;
			--len;
		}
		*buf = 0xff;	/* mark EOL */
}
/*
	convert raw input into sprite data
*/
int packit(BYTE *raw,  /* input data     */
					 int  iw,
/*					 int  ih, */   /* input size     */
					 BYTE **spr, /* output data    */
					 BYTE size,  /* bits per pixel */
					 int  packed,/* <>0 => pack it */
					 int  w,
					 int  h,     /* size of sprite */
					 int  act_x,
					 int act_y)  /*   action point */

{
	BYTE buffer[514], /* max. 512 pels/line */
			 *spr0;

	int	 y;

	if ( (*spr = spr0 = malloc((w+1)*h+1)) == NULL ) return 0;

/*** down/right ***/
	for ( y = act_y ; y<h ; ++y )
	{
		intobuffer(raw+(y*iw>>1),buffer,w,size);
		if (packed)
			spr0 = packline(buffer+act_x,spr0,w-act_x,size,1);
		else
			spr0 = unpackline(buffer+act_x,spr0,w-act_x,size,1);
	}
/*** up/right ***/
  if ( act_y || act_x )
  {
		*spr0++ = 0x01;
		
		for (y = act_y-1 ; y >= 0 ; --y)
		{
			intobuffer(raw+(y*iw>>1),buffer,w,size);
			if (packed)
				spr0 = packline(buffer+act_x,spr0,w-act_x,size,1);
			else
				spr0 = unpackline(buffer+act_x,spr0,w-act_x,size,1);
		}
/*** up/left ***/
		if (act_x)
		{
			*spr0++ = 0x01;

			for (y = act_y-1 ; y >= 0  ; --y)
			{
				intobuffer(raw+(y*iw>>1),buffer,w,size);
				if (packed)
					spr0 = packline(buffer+act_x-1,spr0,act_x,size,-1);
				else
					spr0 = unpackline(buffer+act_x-1,spr0,act_x,size,-1);
			}
/*** down/left ***/
			*spr0++ = 0x01;
			for (y = act_y ; y < h ; ++y)
			{
				intobuffer(raw+(y*iw>>1),buffer,w,size);
				if (packed)
					spr0 = packline(buffer+act_x-1,spr0,act_x,size,-1);
				else
					spr0 = unpackline(buffer+act_x-1,spr0,act_x,size,-1);
			}
		}
	}
	*spr0++ = 0; /* end of sprite-data */
	return ((int)(spr0-*spr));
}

/*------------------------------------------------------------------*/

main(int argc,char *argv[])
{
/* batch */
FILE *batch_handle;
int batch_flag = 0;
char *my_argv[11],cmdline[128];

/* input */
char *infile;
BYTE *in;
long in_size;
int  in_w,in_h;

/* output */
char *outfile;
BYTE *out;
int w,h,action_x,action_y,size,packed;

/* misc */
int ret,i,val,err;
char *c_ptr;


		if (argc == 1 )
		{
			printf("Usage : sprpck [-s#] [-x#] [-y#] [-w#] [-h#] [-u] in inw inh out\n"
						 "-s      : size 4,3,2,1 bit(s) per pixel (4 default)\n"
						 "-x      : action point x (0 default)\n"
						 "-y      : action point y (0 default)\n"
						 "-w,-h   : sprite size (input-size is default)\n"
						 "-u      : unpacked (packed is default)\n"
						 "in      : input data (raw 4 bit per pixel image)\n"
						 "inw,inh : input data size\n"
						 "out     : output filename\n");
			exit(0);
		}

	printf("--------------------------\n"
				 "Lynx Sprite Packer Ver 1.0\n"
				 "(c) 1997 42Bastian Schick \n"
				 "--------------------------\n");

	--argc;
  if (argc == 1)
  {
    if ( (batch_handle = fopen(argv[1],"r")) > NULL)
    {
    	batch_flag = 1;
    	for (i = 0; i < 10 ; ++i)
    	  my_argv[i]=malloc(32);
    	  
    } else
      error("Wrong no batch file found !\n");
  }
  do
  {
   	in_w = in_h = action_x = action_y = 0;
   	size = 4;
    w = h = -1;
		packed = 1;
		
  	if (batch_flag)
  	{ 
  	  fgets(cmdline,127,batch_handle);
    	argc = sscanf(cmdline,"%s %s %s %s %s %s %s %s %s %s",
    	              my_argv[1],my_argv[2],my_argv[3],my_argv[4],my_argv[5],
    	              my_argv[6],my_argv[7],my_argv[8],my_argv[9],my_argv[10]);
    	              
			if ( argc <= 0) break;
			
    } else
      for(i = 0; i < 10 ; ++i)
        my_argv[i] = argv[i];
    
    i = 1;
   	c_ptr = my_argv[1];
   	
		while ( *c_ptr == '-' && argc)
		{
			switch (*(c_ptr+1)){
			case 's' :
			case 'S' : err = sscanf(c_ptr+2,"%d",&val);
								 if (err && val>0 && val <5)
								 {
#ifdef DEBUG
									 printf("Set size to :%d\n",val);
#endif									 
									 size = val;
								 }
								 break;
			case 'x' :
			case 'X' : err = sscanf(c_ptr+2,"%d",&val);
								 if (err)
								 {
#ifdef DEBUG
									 printf("Set action_x to :%d\n",val);
#endif									 
									 action_x = val;
								 }
								 break;
			case 'y' :
			case 'Y' : err = sscanf(c_ptr+2,"%d",&val);
								 if (err)
								 {
#ifdef DEBUG
									 printf("Set action_y to :%d\n",val);
#endif									 
									 action_y = val;
								 }
								 break;
			case 'w' :
			case 'W' : err = sscanf(c_ptr+2,"%d",&val);
								 if (err)
								 {
#ifdef DEBUG
									 printf("Set sprite width to :%d\n",val);
#endif									 
									 w = val;
								 }
								 break;
			case 'h' :
			case 'H' : err = sscanf(c_ptr+2,"%d",&val);
								 if (err)
								 {
#ifdef DEBUG
									 printf("Set sprite height to :%d\n",val);
#endif									 
									 h = val;
								 }
								 break;
			case 'u' :
			case 'U' : packed = 0;
#ifdef DEBUG
								 printf("Unpacked sprite !\n");
#endif								 			
								 break;

			}/*switch*/
			c_ptr=my_argv[++i];
			--argc;
		}

		if (argc < 4 ) break;
			
		infile = my_argv[i];
		
		err = sscanf(my_argv[++i],"%d",&in_w);
		if (!err)	error("No width!\n");
		
		err = sscanf(my_argv[++i],"%d",&in_h);
		if (!err) error("No Height!\n");
		
		outfile = my_argv[++i];

		if ( w == -1 ) w = in_w;
		if ( h == -1 ) h = in_h;
 
  	if ( w > in_w || h > in_h )
    	error("Sprite > input picture !\n");
  
  	if ( in_size = LoadFile(infile,&in) )
  	{
    	if (in_size != (long)(in_w * in_h >>1))
    	{
    		free(in);
      	error("Wrong pictures size !\n");
    	}
           
    	ret = packit(in,in_w,/*in_h,*/ &out,
    	             size,packed,w,h,action_x,action_y);
    	free(in);
    	if ( ret )
    	{
		  	SaveSprite(outfile,out,ret);
		  	free(out);
			}else
				error("Packed size = 0!");
		} else
		  error("Couldn't load input file!");
	}while (batch_flag && !feof(batch_handle) );
	if (batch_flag)
	  fclose(batch_handle);
	  
	return 0;
}

long LoadFile(char fn[],BYTE **ptr)
{	long len;
  int f;
	if ((f = open(fn,O_RDONLY)) >= 0)
	{	len = lseek(f,0L,SEEK_END);	
		lseek(f,0L,SEEK_SET);				
		if ( ( *ptr=malloc(len) ) == NULL) return 0;
		len  = read(f,*ptr,len);
		close(f);
		return (len);
	}else
		return 0;
}

void SaveSprite(char *filename,BYTE *ptr,int size)
{
int handle;
	if ( (handle = open(filename,O_CREAT|O_RDWR|O_TRUNC)) <0 )
	{
	  printf("Error: Couldn't open %s !\n",filename);
	  return;
	}
	if ( write(handle,ptr,size) != size )
	  printf("Error: Couldn't write %s !\n",filename);
	close(handle);
}

void error(char *f)
{
	printf("Error :%s",f);
	exit(-1);
}