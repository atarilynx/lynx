;****************
; CC65 Lynx Library
;
;****************
;* LYNX LED strip driver for WS2801 LED's
;*
;* created : 25.12.2020
;* last modified :
;*
;* CLK = A0 (17)
;* DO  = AUDIN (32)
;*
;* How to contact the WS2801 LED strip :
;*
;* WS2801
;*   GND |- GND
;*   DI  |- AUDIN (32)
;*   CI  |- A0    (17)
;*   5V  |- +5V
;*
;* The reason for using A0 is the speed requirement of the LED chip
;* Any delay over 500us means that the transfer is complete and the data
;* is latched. So you will get a lot of wrong colours on the LED strip
;* if the driver cannot keep up with this speed requirement.
;* Once the data is all transferred the colour will be correct.
;* Using A0 for the clock is the fastest signal available here.
;****************

    .export         _lynx_ledinit_ws2801
    .export         _lynx_ledwrite_ws2801
    .import         popa
    .importzp       ptr1

    .include        "lynx.inc"
    .include        "extzp.inc"

; ------------------------------------------------------------------------
; LYNX CART IO acions macros -> use Y register

.macro IO_CLOCKHIGHLOW
    stz     RCART0
    stz     RCART0
.endmacro

.macro IO_INITCLOCK
    lda __iodat
    and #$fc
    ora #2
    ina
    sta SYSCTL1
    dea
    sta SYSCTL1
.endmacro

.macro IO_SETOUTPUT
    ldy     #$1a
    sty     IODIR   ; set AUDIN for output
.endmacro

.macro IO_LINELOW
    ldy     #$0b
    sty     IODAT
.endmacro

.macro IO_LINEHIGH
    ldy     #$1b
    sty     IODAT
.endmacro

; ------------------------------------------------------------------------
; LED output function

E2_put8:            ;data in A
    ldx    #8
E2_P8loop:
    rol
    bcc    E2_T0
E2_T1:
    IO_LINEHIGH
    bra E2_P8strobeclock
E2_T0:
    IO_LINELOW
E2_P8strobeclock:
    IO_CLOCKHIGHLOW
    dex
    bne    E2_P8loop
E2_P8ACK:
    IO_LINELOW
    rts             ;Z flag is set if NO ACK, clear if ACK received

; ------------------------------------------------------------------------
; void __fastcall__ _lynx_ledinit_ws2801 ();
; ------------------------------------------------------------------------
; /* Set CLK to right polarity and AUDIN out */
;
_lynx_ledinit_ws2801:
    IO_INITCLOCK
    IO_SETOUTPUT
    IO_LINELOW
    rts

; ------------------------------------------------------------------------
; void __fastcall__ _lynx_ledwrite_ws2801 (unsigned char red, unsigned char green, unsigned char blue);
; ------------------------------------------------------------------------
; /* Write RGB */
;
_lynx_ledwrite_ws2801:
    pha     ; Push blue on stack
    jsr popa
    pha     ; Push green on stack
    jsr popa
            ; red is now in A
    jsr    E2_put8
    pla     ; green is now in A
    jsr    E2_put8
    pla     ; blue is now in A
    jsr    E2_put8
    rts

