;****************
; CC65 Lynx Library
;
;****************
;* LYNX CART EEPROM driver
;* for 24AAXXX (up to 512Kbit => 64K 8-bit words)
;*
;* created : 16.11.2020
;* last modified :
;*
;* CLK   = A1 (11)
;* DI/DO = AUDIN (32)
;*
;* How to contact the EEPROM :
;*
;*                -----\/-----     24AAXXX
;*   GND  --------| A0   VCC |- +5V
;*   GND  --------| A1   WP  |- GND
;*   GND  --------| A2   SCL |- A1    (11)
;*   GND  --------| VSS  SDA |- AUDIN (32)
;*                ------------
;*
;****************

    .export         _lynx_eeread_24aa512
    .export         _lynx_eereadnext_24aa512
    .export         _lynx_eereadbytes_24aa512
    .export         _lynx_eewrite_24aa512
    .export         _lynx_eepageread_24aa512
    .export         _lynx_eepagewrite_24aa512
    .import         popa
    .importzp       ptr1

    .include        "lynx.inc"


; ------------------------------------------------------------------------
; EEPROM command list

E2_C_WRITE      =   $a0
E2_C_READ       =   $a1

; ------------------------------------------------------------------------
; LYNX CART IO acions macros -> use Y register

.macro IO_CHANGECLOCK
    stz     RCART0
    stz     RCART0
.endmacro

.macro IO_INITCLOCK
    ldy     #3      ;strobing bit 0 of SYSCTL1 changes the high part of the cart address, and this resets the low part of the address controlled by the ripple counter
    sty     SYSCTL1
    dey
    sty     SYSCTL1
.endmacro

.macro IO_SETINPUT
    ldy     #$a
    sty     IODIR   ; set AUDIN to Input
.endmacro

.macro IO_SETOUTPUT
    ldy     #$1a
    sty     IODIR   ; set AUDIN for output
.endmacro

.macro IO_LINELOW
    ldy     #$0b
    sty     IODAT
.endmacro

.macro IO_LINEHIGH
    ldy     #$1b
    sty     IODAT
.endmacro

; ------------------------------------------------------------------------
; EEPROM Low level macros

.macro E2_SENDACK
    IO_SETOUTPUT    ;draw line low
    IO_LINELOW
    IO_CHANGECLOCK  ;clock high
    IO_CHANGECLOCK  ;clock low
    IO_SETINPUT     ;releasing channel after the clock
.endmacro

.macro E2_SENDNOACK
    IO_SETOUTPUT    ;draw line low
    IO_LINEHIGH
    IO_CHANGECLOCK  ;clock high
    IO_CHANGECLOCK  ;clock low
    IO_SETINPUT     ;releasing channel after the clock
.endmacro

.macro E2_CHECKACK
    IO_CHANGECLOCK  ;clock high
    lda    IODAT
    IO_CHANGECLOCK  ;clock low
    and    #$10
    cmp    #$10
.endmacro

.macro E2_READDATA
    IO_CHANGECLOCK  ;clock high
    lda    IODAT
    IO_CHANGECLOCK  ;clock low
    and    #$10
    rol
    rol
    rol
    rol             ;bit 4 (data read) is in the Carry
.endmacro

.macro E2_STOP
    IO_SETOUTPUT    ;drive the line low
    IO_LINELOW
    IO_CHANGECLOCK  ;clock high
    IO_LINEHIGH     ;releasing channel drive the data line high. STOP signal is a LOW->HIGH transition during clock HIGH
    IO_CHANGECLOCK  ;clock low
    IO_SETINPUT
.endmacro

.macro E2_INIT      ;this is the only command resetting the clock to low as first action
    IO_SETINPUT     ;release channel -> data line pulled up
    IO_INITCLOCK
    IO_LINELOW      ;needed?
.endmacro

.macro E2_START
    IO_SETOUTPUT
    IO_LINEHIGH
    IO_CHANGECLOCK  ;clock high
    IO_LINELOW      ;START signal is a HIGH->LOW transition during clock HIGH
    IO_CHANGECLOCK  ;clock low
    IO_SETINPUT     ;releasing channel after the clock -> data line pulled up
.endmacro

; ------------------------------------------------------------------------
; EEPROM IO functions

E2_put8:            ;data in A
    ldx    #8
E2_P8loop:
    rol
    bcc    E2_T0
E2_T1:
    IO_SETOUTPUT
    IO_LINEHIGH
    bra E2_P8strobeclock
E2_T0:
    IO_SETOUTPUT
    IO_LINELOW
E2_P8strobeclock:
    IO_CHANGECLOCK  ;clock high
    IO_CHANGECLOCK  ;clock low
    dex
    bne    E2_P8loop
E2_P8ACK:
    IO_SETINPUT     ;release the channel
    E2_CHECKACK
    rts             ;Z flag is set if NO ACK, clear if ACK received

E2_get8_val:
    .byte    0
E2_get8:
    pha
    stz    E2_get8_val
    ldx    #8
E2_G8loop:
    E2_READDATA     ;read bit is in the Carry flag
    rol    E2_get8_val
    dex
    bne    E2_G8loop

    pla
    bne    E2_G8NOACK ;if A is != 0 don't send the ACK (transmission to be closed)
    E2_SENDACK
    bra    E2_G8end
E2_G8NOACK:
    E2_SENDNOACK
E2_G8end:
    lda    E2_get8_val
    rts

E2_setAddr:
    pha
    phx
    lda    #E2_C_WRITE
    jsr    E2_put8
    beq    E2_setAddr_Exit1
    pla
    jsr    E2_put8
    beq    E2_setAddr_Exit2
    pla
    jsr    E2_put8
    rts
E2_setAddr_Exit1:
    pla
E2_setAddr_Exit2:
    pla
    lda #0	; to set the Z flag indicating an error. PLA could have changed it	   
    rts

p2a_val:
    .byte    0
E2_page2addr:
    stz p2a_val
    ldx #7
p2aloop:
    clc
    rol
    rol p2a_val
    dex
    bne p2aloop
    ldx p2a_val
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eeread_24aa512 (unsigned char* val, unsigned int addr);
; ------------------------------------------------------------------------
; /* Read one byte */
;
_lynx_eeread_24aa512:
    E2_INIT
    E2_START
    jsr    E2_setAddr
    beq    byteRead_error
    jsr popa
    sta read_retval+1
    jsr popa
    sta read_retval+2
    E2_START
    lda    #E2_C_READ
    jsr    E2_put8
    beq    byteRead_error
    lda    #1   ;only 1 byte to read, so not mantaining the transmission active
    jsr    E2_get8
    E2_STOP
read_retval:
    sta $ffff   ;address dynamically changed  
    lda #0
    tax
    rts
byteRead_error:
    E2_STOP
    lda #1
    ldx #0
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eereadnext_24aa512 (unsigned char* val);
; ------------------------------------------------------------------------
; /* Read one byte */
;
_lynx_eereadnext_24aa512:
    sta readnext_retval+1
    stx readnext_retval+2
    E2_INIT
    E2_START
    lda    #E2_C_READ
    jsr    E2_put8
    beq    byteReadNext_error
    lda #1			;only 1 byte to read, so not mantaining the transmission active
    jsr E2_get8
    E2_STOP
readnext_retval:
    sta $ffff   ;address dynamically changed  
    lda #0
    tax
    rts
byteReadNext_error:
    E2_STOP
    lda #1
    ldx #0
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eewrite_24aa512 (unsigned char data, unsigned int addr);
; ------------------------------------------------------------------------
; /* Write one byte */
;
_lynx_eewrite_24aa512:
    E2_INIT
    E2_START
    jsr    E2_setAddr
    beq    byteWrite_error
;    lda    sreg  ;get data to write from the sreg zp pseudo register
    jsr popa
    jsr    E2_put8
    beq    byteWrite_error
    E2_STOP
    lda    #0   ;return value 0 = OK
    tax
    rts
byteWrite_error:
    E2_STOP
    lda    #1   ;return value 1 = KO
    ldx    #0
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eepageread_24aa512 (char * buf, unsigned int page);
; ------------------------------------------------------------------------
; /* Read all the 128 bytes of a page */
;
_lynx_eepageread_24aa512:
    pha
    jsr popa
    sta ptr1
    jsr popa
    sta ptr1+1
    pla
    jsr E2_page2addr
    E2_INIT
    E2_START
    jsr    E2_setAddr
    beq    pageRead_error
    E2_START
    lda    #E2_C_READ
    jsr    E2_put8
    beq    pageRead_error
    ldy    #0
pagereadloop:
    phy
    lda    #0
    cpy    #127   ; is it the last byte
    bne    pageread_nostop
    lda    #1	; a == 1 for last byte
pageread_nostop:
    jsr    E2_get8
    ply
    sta    (ptr1),y
    iny
    cpy    #128
    bne    pagereadloop
    E2_STOP
    lda    #0   ;return value 0 = OK
    tax
    rts
pageRead_error:
    E2_STOP
    lda    #1   ;return value 1 = KO
    ldx    #0
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eereadbytes_24aa512 (char * buf, unsigned int len, unsigned int addr);
; ------------------------------------------------------------------------
; /* Read all n bytes */
;
bytes2read:
    .byte    0
    .byte    0
_lynx_eereadbytes_24aa512:
    pha
    jsr popa
    sta bytes2read
    jsr popa
    sta bytes2read+1
    jsr popa
    sta buff_addr+1
    jsr popa
    sta buff_addr+2
    pla
    E2_INIT
    E2_START
    jsr    E2_setAddr
    beq    pageRead_error
    E2_START
    lda    #E2_C_READ
    jsr    E2_put8
    bne    readbytesloop
    jmp    pageRead_error
readbytesloop:
    lda    #0
;    lda    #1	; a == 1 for last byte
;readbytes_nostop:
    jsr    E2_get8
buff_addr:
    sta    $FFFF ; address dynamically updated
    inc    buff_addr+1
    bne    pageread_declen
    inc    buff_addr+2
pageread_declen:
    lda    bytes2read
    dec
    sta    bytes2read
    bne    readbytesloop
    lda    bytes2read+1
    dec
    sta    bytes2read+1
    beq    pageread_stop
    bra    readbytesloop
pageread_stop:
    E2_STOP	
    lda    #0   ;return value 0 = OK
    tax
    rts
bytesread_error:
    E2_STOP
    lda    #1   ;return value 1 = KO
    ldx    #0
    rts

; ------------------------------------------------------------------------
; unsigned char __fastcall__ _lynx_eepagewrite_24aa512 (char * buf, unsigned int page);
; ------------------------------------------------------------------------
; /* Writes all the 128 bytes of a page */
;
_lynx_eepagewrite_24aa512:
    pha
    jsr popa
    sta ptr1
    jsr popa
    sta ptr1+1
    pla
    jsr E2_page2addr
    E2_INIT
    E2_START
    ldx    #0
    jsr    E2_setAddr
    beq    pageWrite_error1
    ldy    #0
pagewriteloop:
    phy
    lda    (ptr1),y
    jsr    E2_put8
    beq    pageWrite_error2
    ply
    iny
    cpy    #128
    bne    pagewriteloop
    E2_STOP
    lda    #0   ;return value 0 = OK
    tax
    rts
pageWrite_error1:
    E2_STOP
    lda    #1   ;return value 1 = KO
    ldx    #0
    rts
pageWrite_error2:
    ply
    E2_STOP
    lda    #1   ;return value 1 = KO
    ldx    #0
    rts
