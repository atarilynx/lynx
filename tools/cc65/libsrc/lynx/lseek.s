;
; Karri Kaksonen, 2010
;
; This function is used to place the Lynx hardware to point to any byte in
; the Lynx cart.
;
; This function supports all available block sizes (512, 1024 and 2048 bytes).
; No other block sizes have been used afaik.
;
; Only SEEK_SET and SEEK_CUR operation modes are implemented.
; SEEK_END will work as SEEK_SET. We have no concept of a file.
; This routine will always set the block pointer.
; So if you want to use small skips while reading you need to do it by
; dummy reads in your code.
;
; off_t __fastcall__ lseek(int fd, off_t offset, int whence);

	.importzp	sp, sreg, regsave, regbank, tmp1, ptr1, ptr2
	.macpack	longbranch
	.export		_lseek, _lseek_set, _lseek_cur
	.import		pushax, push0ax, pusheax, ldaxysp, ldeax0sp, incsp6, popeax
	.import		tosandeax, axlong, tosorax, tosasreax, tosasleax, tosaddeax
	.import		lynxskip0, lynxblock
	.import		__BLOCKSIZE__
	.importzp	_FileStartBlock, _FileCurrBlock, _FileBlockByte, _FileBlockOffset

.segment	"CODE"

.proc	_lseek_set: near

.segment	"CODE"

_lseek_set:
	jsr     ldeax0sp
	jsr     pusheax
	ldx     #$00
	lda     #<(__BLOCKSIZE__/1024 + 9)
	jsr     tosasreax
	sta     _FileCurrBlock
	stx     _FileCurrBlock+1
	jsr     lynxblock
	jsr     ldeax0sp
	jsr     pusheax
	lda     #<(__BLOCKSIZE__-1)
	ldx     #>(__BLOCKSIZE__-1)
	jsr     axlong
	jsr     tosandeax
	eor	#$FF
	pha
	txa
	eor	#$FF
	tay
	plx
	jsr     lynxskip0
	jsr     ldeax0sp
	jmp     incsp6

.endproc

.segment	"CODE"

.proc	_lseek_cur: near

.segment	"CODE"

_lseek_cur:
	; Long push StartBlock
	ldx	_FileStartBlock + 1
	lda	_FileStartBlock
	jsr     push0ax

	; Expand it to bytes and long push
	ldx     #0
	lda     #<(__BLOCKSIZE__/1024 + 9)
	jsr     tosasleax
	jsr     pusheax

	; Get the BlockBytes 'or' the word with the low part of the shifted block
	ldx	_FileBlockOffset + 1
	lda	_FileBlockOffset
	jsr     tosorax
	jsr     pushax

	; Add the two long offsets with each other to get the cart offset
	jsr     popeax
	jsr	tosaddeax
	jsr     pusheax
	jmp	_lseek_set

.endproc

.segment	"CODE"

.proc	_lseek: near

.segment	"CODE"

_lseek:
	ldx     #$05
	jsr     ldaxysp
	beq	@seek_cur
	jmp	_lseek_set
@seek_cur:
	jmp	_lseek_cur

.endproc
