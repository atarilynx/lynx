static const char CfgNES [] = 
"SYMBOLS {\n"
"__STACKSIZE__: type = weak, value = $0300;\n" /* 3 pages stack */
"}\n"
"MEMORY {\n"
"ZP:     file = \"\", start = $0002, size = $001A, type = rw, define = yes;\n"
"\n"
"\n"    /* INES Cartridge Header */
"HEADER: file = %O, start = $0000, size = $0010, fill = yes;\n"
"\n"
"\n"    /* 2 16K ROM Banks */
"\n"    /* - startup */
"\n"    /* - code */
"\n"    /* - rodata */
"\n"    /* - data (load) */
"ROM0:   file = %O, start = $8000, size = $7FF4, fill = yes, define = yes;\n"
"\n"
"\n"    /* Hardware Vectors at End of 2nd 8K ROM */
"ROMV:   file = %O, start = $FFF6, size = $000C, fill = yes;\n"
"\n"
"\n"    /* 1 8k CHR Bank */
"ROM2:   file = %O, start = $0000, size = $2000, fill = yes;\n"
"\n"
"\n"    /* standard 2k SRAM (-zeropage) */
"\n"    /* $0100-$0200 cpu stack */
"\n"    /* $0200-$0500 3 pages for ppu memory write buffer */
"\n"    /* $0500-$0800 3 pages for cc65 parameter stack */
"SRAM:   file = \"\", start = $0500, size = __STACKSIZE__, define = yes;\n"
"\n"
"\n"    /* additional 8K SRAM Bank */
"\n"    /* - data (run) */
"\n"    /* - bss */
"\n"    /* - heap */
"RAM: file = \"\", start = $6000, size = $2000, define = yes;\n"
"}\n"
"SEGMENTS {\n"
"HEADER:   load = HEADER,          type = ro;\n"
"STARTUP:  load = ROM0,            type = ro,  define = yes;\n"
"LOWCODE:  load = ROM0,            type = ro,                optional = yes;\n"
"INIT:     load = ROM0,            type = ro,  define = yes, optional = yes;\n"
"CODE:     load = ROM0,            type = ro,  define = yes;\n"
"RODATA:   load = ROM0,            type = ro,  define = yes;\n"
"DATA:     load = ROM0, run = RAM, type = rw,  define = yes;\n"
"VECTORS:  load = ROMV,            type = rw;\n"
"CHARS:    load = ROM2,            type = rw;\n"
"BSS:      load = RAM,             type = bss, define = yes;\n"
"ZEROPAGE: load = ZP,              type = zp;\n"
"}\n"
"FEATURES {\n"
"CONDES: segment = INIT,\n"
"type    = constructor,\n"
"label   = __CONSTRUCTOR_TABLE__,\n"
"count   = __CONSTRUCTOR_COUNT__;\n"
"CONDES: segment = RODATA,\n"
"type    = destructor,\n"
"label   = __DESTRUCTOR_TABLE__,\n"
"count   = __DESTRUCTOR_COUNT__;\n"
"CONDES: segment = RODATA,\n"
"type    = interruptor,\n"
"label   = __INTERRUPTOR_TABLE__,\n"
"count   = __INTERRUPTOR_COUNT__,\n"
"import  = __CALLIRQ__;\n"
"}\n"
;
