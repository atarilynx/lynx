static char MacGeneric[] = 
/* add - Add without carry */
".macro  add     Arg1, Arg2\n"
"clc\n"
".if .paramcount = 2\n"
"adc     Arg1, Arg2\n"
".else\n"
"adc     Arg1\n"
".endif\n"
".endmacro\n"
/* sub - subtract without borrow */
".macro  sub     Arg1, Arg2\n"
"sec\n"
".if .paramcount = 2\n"
"sbc     Arg1, Arg2\n"
".else\n"
"sbc     Arg1\n"
".endif\n"
".endmacro\n"
/* bge - jump if unsigned greater or equal */
".macro  bge     Arg\n"
"bcs     Arg\n"
".endmacro\n"
/* blt - Jump if unsigned less */
".macro  blt     Arg\n"
"bcc     Arg\n"
".endmacro\n"
/* bgt - jump if unsigned greater */
".macro  bgt     Arg\n"
".local  L\n"
"beq     L\n"
"bcs     Arg\n"
"L:\n"
".endmacro\n"
/* ble - jump if unsigned less or equal */
".macro  ble     Arg\n"
"beq     Arg\n"
"bcc     Arg\n"
".endmacro\n"
;
