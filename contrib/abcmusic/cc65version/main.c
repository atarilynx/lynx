#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <time.h>
#include <stdlib.h>

extern void abcstop ();
extern void abcplay (unsigned char channel, char *tune);
extern unsigned char abcactive[4];
// Special low-level calls to set up underlying hardware
// abcoctave legal values 0..6
extern void __fastcall__ abcoctave(unsigned char chan, unsigned char val);
// abcpitch legal values 0..255
extern void __fastcall__ abcpitch(unsigned char chan, unsigned char val);
// abctaps legal values 0..511
extern void __fastcall__ abctaps(unsigned char chan, unsigned int val);
// abcintegrate legal values 0..1
extern void __fastcall__ abcintegrate(unsigned char chan, unsigned char val);
// abcvolume legal values 0..127
extern void __fastcall__ abcvolume(unsigned char chan, unsigned char val);

static void
init ()
{
  tgi_install (&tgi_static_stddrv);
  joy_install (&joy_static_stddrv);
  tgi_init ();
  CLI ();
}

static void
range ()
{
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_GREEN);
  tgi_outtextxy (0, 10, "In this test we run");
  tgi_outtextxy (0, 20, "C E G from octave 5");
  tgi_outtextxy (0, 30, "up to octave 0");
  tgi_outtextxy (0, 40, "Then we continue");
  tgi_outtextxy (0, 50, "with ceg and c'e'g'");
  tgi_updatedisplay ();
  abcplay (0,
	   "X7 I0 V100 T20 R0 H8 K4 O5CEG O4CEG O3CEG O2CEG O1CEG O0CEG cegc'e'g'");
  while (abcactive[0]);
}

static void
twoVoices ()
{
  abcplay (0,
	   "X7 O1 I1 V100 T20 R0 H0 K3 CDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCCcBAABAGGAGFFEDEEcBAABAGGAGFFEDCCECDDFDEEGEFGABccECDDFDEEGEFGABccCDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCC");
  abcplay (1,
	   "X7 O2 I1 V100 T20 R0 H0 K3 zzzzzzzzzzzzzzzzCDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCCcBAABAGGAGFFEDEEcBAABAGGAGFFEDCCECDDFDEEGEFGABccECDDFDEEGEFGABccCDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCC");
  while (abcactive[0] || abcactive[1])
    {
      clock_t now = clock () + 4 * 75;
      while (tgi_busy ());
      tgi_clear ();
      tgi_setcolor (COLOR_GREEN);
      tgi_outtextxy (0, 10, "Here we have a");
      tgi_outtextxy (0, 20, "Finnish kantele");
      tgi_outtextxy (0, 30, "melody for two");
      tgi_outtextxy (0, 40, "players");
      tgi_updatedisplay ();
      while (abcactive[1] && clock () < now);
      now = clock () + 4 * 75;
      tgi_clear ();
      tgi_setcolor (COLOR_GREEN);
      tgi_outtextxy (0, 10, "The plucking");
      tgi_outtextxy (0, 20, "sound comes from");
      tgi_outtextxy (0, 30, "R0 - no attack");
      tgi_outtextxy (0, 40, "H0 - no hold");
      tgi_outtextxy (0, 50, "K3 - decay");
      tgi_outtextxy (0, 60, "I1 - integrate on");
      tgi_outtextxy (0, 70, "means triangular");
      tgi_outtextxy (0, 80, "waveform");
      tgi_updatedisplay ();
      while (abcactive[1] && clock () < now);
      now = clock () + 4 * 75;
      tgi_clear ();
      tgi_setcolor (COLOR_GREEN);
      tgi_outtextxy (0, 10, "The first kantele");
      tgi_outtextxy (0, 20, "plays from");
      tgi_outtextxy (0, 30, "O1 - high octave");
      tgi_outtextxy (0, 40, "While the second");
      tgi_outtextxy (0, 50, "plays from");
      tgi_outtextxy (0, 60, "O2 - lower octave");
      tgi_updatedisplay ();
      while (abcactive[1] && clock () < now);
    }
}

static void
sharps ()
{
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_GREEN);
  tgi_outtextxy (0, 10, "In this tune we");
  tgi_outtextxy (0, 20, "have different");
  tgi_outtextxy (0, 30, "note lengths");
  tgi_outtextxy (0, 40, "like e2 a4.");
  tgi_outtextxy (0, 50, "There is also lots");
  tgi_outtextxy (0, 60, "of sharps ^c");
  tgi_updatedisplay ();
  abcplay (0,
	   "X7I1V127T10R6H10K1O2 e2|a4 e3d|^c6 A2|B^c de ^f^g ab|^g2 e3^e ^f^g| a2 e^c ^f2 dB|e2 ^cA ^GA Bc|de ^f^g ab ^c'd'|e'6 e2| d3^c B^c de|^c2 A4 e2|^f2 ed e^f ^ga|b6 e2| a2 e^c ^f2 dB|e2 ^cA ^GA Be|^f4 ^g3e|a6");
  while (abcactive[0]);
}

static void
sweep ()
{
  unsigned char pitch;
  unsigned char loop;
  unsigned char vol;
  clock_t now;
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_YELLOW);
  tgi_outtextxy (0, 10, "Sweep generated");
  tgi_outtextxy (0, 20, "directly by");
  tgi_outtextxy (0, 30, "banging registers");
  tgi_updatedisplay ();
  abctaps(0, 7);
  abcoctave(0, 2);
  now = clock() + 1;
  vol = 50;
  for (loop = 0; loop < 10; loop++) {
      vol += 10;
      abcvolume(0, vol);
      if (loop & 1 )
          abcintegrate(0, 1);
      else
          abcintegrate(0, 0);
      for (pitch = 100; pitch < 240; pitch += 8) {
          abcpitch(0, pitch);
          while (clock() < now) ;
          now = clock() + 1;
      }
  }
  abcvolume(0, 0);
}

static void
engine ()
{
  unsigned char pitch;
  unsigned char loop;
  unsigned char vol;
  clock_t now;
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_YELLOW);
  tgi_outtextxy (0, 10, "Engine generated");
  tgi_outtextxy (0, 20, "directly by");
  tgi_outtextxy (0, 30, "banging registers");
  tgi_updatedisplay ();
  abctaps(0, 0x107);
  abcoctave(0, 2);
  abcvolume(0, 80);
  abcintegrate(0, 0);
  now = clock() + 200;
  for (pitch = 100; pitch > 8; pitch -= 8) {
      abcpitch(0, pitch);
      while (clock() < now) ;
      now = clock() + 10;
  }
  now = clock() + 200;
  while (clock() < now) ;
  abcvolume(0, 0);
}

static void
repeats ()
{
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_GREEN);
  tgi_outtextxy (0, 10, "Repeats are nice");
  tgi_outtextxy (0, 20, "You can repeat");
  tgi_outtextxy (0, 30, "patterns forever");
  tgi_outtextxy (0, 40, "or set a count");
  tgi_outtextxy (0, 50, "Here is a");
  tgi_outtextxy (0, 60, "tasteless band");
  tgi_outtextxy (0, 70, "playing funk");
  tgi_outtextxy (0, 80, "background to");
  tgi_outtextxy (0, 90, "a medieval melody");
  tgi_updatedisplay ();
  abcplay (0,
	   "X200 I0 V30 T8 R0H0K2 O0 |: CzCz|a2za|zaCa|aCzz:");
  abcplay (1,
	   "X7 I0 V50 T8 R0H4K4 O3 |:2 z4z4z4z4:|:C2C2|z4|zzG2|z^A2z:");
  abcplay (2,
	   "X7I1V127T8R6H10K1O2 |:3 z4z4z4z4:| z4z4z4z2 e2|a4 e3d|^c6 A2|B^c de ^f^g ab|^g2 e3^e ^f^g| a2 e^c ^f2 dB|e2 ^cA ^GA Bc|de ^f^g ab ^c'd'|e'6 e2| d3^c B^c de|^c2 A4 e2|^f2 ed e^f ^ga|b6 e2| a2 e^c ^f2 dB|e2 ^cA ^GA Be|^f4 ^g3e|a6");
  while (abcactive[2]);
  abcstop();
}

static void
thanks ()
{
  while (tgi_busy ());
  tgi_clear ();
  tgi_setcolor (COLOR_GREEN);
  tgi_outtextxy (0, 50, "Enjoy the code");
  tgi_updatedisplay ();
}

void
main (void)
{
  init ();
  range ();
  twoVoices ();
  sharps ();
  sweep ();
  engine ();
  repeats ();
  thanks ();
  while (1);
}
