#define NR_OF_CHANNELS 4

typedef unsigned char bool;
extern char *score_ptr[NR_OF_CHANNELS];
extern void abc_metronome();
extern void abcmusic(unsigned char chan, char *score);
extern void abc_setinstrument(unsigned char chan, int instrument);
extern void abc_note(unsigned char chan, int height, int note_duration);
extern void abc_rest(unsigned char chan, int note_duration);
extern void abc_setvolume(unsigned char chan, int volume);
extern void abc_sound();
extern void abc_pause();
extern void abc_stop(unsigned char chan);
extern char abc_ended(unsigned char chan);
extern void abc_stopall();
extern void abc_refresh();
extern void abc_pitch(unsigned char chan, unsigned char pitch);
