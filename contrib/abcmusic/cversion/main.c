#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h> 
#include <time.h>

extern void silence();
extern void abcmusic(unsigned char channel, char *tune);
extern void update_music();
extern void abc_metronome();
extern void abc_sound();
extern void abc_pitch(char chan, char pitch);
extern void abc_setinstrument(char chan, int instrument);

extern void abc_setvolume(unsigned char channel, int volume);
extern int active_instrument[4];

char *abcmusic0 = "I5V40T18|:CDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCCcBAABAGGAGFFEDEEcBAABAGGAGFFEDCCECDDFDEEGEFGABccECDDFDEEGEFGABccCDEEDEFFEFGEFDGGCDEEDEFFEFGEFDCC:";
char *abcmusic1 = "I1V80T18|:C,4D,4E,4F,4 C,4D,4E,4F,4 C4B,4A,4E,4 C4B,4A,4E,4 E,4F,4G,4A,4 E,4F,4G,4A,4 C,4D,4E,4F,4 C,4D,4E,4F,4:";
char *abcmusic2 = "I6V40T18 zzzzzzzz |:cdeedeffefgefdgg CDEEDEFFEFGEFDCC cBAABAGGAGFFEDEE cBAABAGGAGFFEDCC ECDDFDEEGEFGABcc ECDDFDEEGEFGABcc CDEEDEFFEFGEFDGG CDEEDEFFEFGEFDCC:";
char *abcmusic3 = "I1V80T18 z4z4z2 |:C4D4E4F4 C4D4E4F4 c4B4A4E4 c4B4A4E4 E4F4G4A4 E4F4G4A4 C4D4E4F4 C4D4E4F4:";

void main(void) {
  clock_t now;
  unsigned char pitch = 0;
  tgi_install(&tgi_static_stddrv);
  joy_install(&joy_static_stddrv);
  tgi_init();
  CLI();

  silence();
  abcmusic(0,abcmusic0);
  abcmusic(1,abcmusic1);
  //abcmusic(2,abcmusic2);
  //abcmusic(3,abcmusic3);

  now = clock();
  while (1) {
      if (now != clock()) {
          now++;
          abc_metronome();
          abc_sound();
      }
  }
}

