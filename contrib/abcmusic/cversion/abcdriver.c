#include "abcinstr.h"
// The Atari Lynx audio system has 8 registers per audio
// channel that can be programmed to produce different
// waveforms.

#define STEREO_REG       (*(char *)0xfd50)
#define VOLUME_REG(chan) (*(char *)(0xfd20 + 0 + (chan & 3) * 8))
#define FEED_REG(chan)   (*(char *)(0xfd20 + 1 + (chan & 3) * 8))
#define OUT_REG(chan)    (*(char *)(0xfd20 + 2 + (chan & 3) * 8))
#define SHIFT_REG(chan)  (*(char *)(0xfd20 + 3 + (chan & 3) * 8))
#define BKUP_REG(chan)   (*(char *)(0xfd20 + 4 + (chan & 3) * 8))
#define CTLA_REG(chan)   (*(char *)(0xfd20 + 5 + (chan & 3) * 8))
#define CNT_REG(chan)    (*(char *)(0xfd20 + 6 + (chan & 3) * 8))
#define CTLB_REG(chan)   (*(char *)(0xfd20 + 7 + (chan & 3) * 8))

static char oct[] = {
    1,
    1,
    1,
    0,
    0,
    0,
    4,
    0,
    0,
    0,
    0,
    2
};

static char delays[] = {
    146,
    138,
    130,
    246,
    232,
    219,
    12,
    195,
    184,
    174,
    164,
    38
};

#define NR_OF_CHANNELS 4
#define ATTACK_MODE 1
#define HOLD_MODE   2
#define DECAY_MODE  3

static char envelope_mode[NR_OF_CHANNELS];
static char wanted_volume[NR_OF_CHANNELS];
static char vol[NR_OF_CHANNELS];
static int duration[NR_OF_CHANNELS];
static int hold_duration[NR_OF_CHANNELS];
int active_instrument[NR_OF_CHANNELS] = {
	0, 0, 0, 0
};

// To keep the sounds related to each other you
// have to use the same loop length or a multiple
// of the looplengths.

// Example use 2, 4, 8, 16, 32...
// or 6, 12, 18

int orchestra[] = {
//piano wave 0x001b looplen 8
	0x0019,	// taps
	0x00B1,	// backup
	2,	// octave
	1,	// intergrate
	0x43,	// volume
	67,	// attack
	2,	// hold
	2,	// decay
//bass wave 0x0018 looplen 8
	0x0008,	// taps
	0x0078,	// backup
	3,	// octave
	1,	// intergrate
	0x43,	// volume
	127,	// attack
	0,	// hold
	3,	// decay , was 1
//trumpet wave 0x0022 looplen 8
	0x002A,	// taps
	0x0065,	// backup
	2,	// octave
	0,	// intergrate
	0x40,	// volume
	127,	// attack
	127,	// hold
	0,	// decay
//drums wave
	0xCBD,		// taps
	0x800,		// backup
	3,		// octave
	0,		// intergrate
	0x20,		// volume
	127,		// attack
	0,		// hold
	127,		// decay
//piano wave 0x001b looplen 8
	0x0019,	// taps
	0x00B1,	// backup
	1,	// octave
	1,	// intergrate
	0x43,	// volume
	67,	// attack
	2,	// hold
	2,	// decay
};

static void set_sound_engine(char chan, int *settings)
{
	int taps, backup;

	taps = settings[0];
	backup = settings[1];

	VOLUME_REG(chan) = 0;

	STEREO_REG = 0;

	// Disable count
	CTLA_REG(chan) = 0x10;

	// Setup new sound engine
	FEED_REG(chan) = (taps & 0x003f) + ((taps >> 4) & 0xc0);
	BKUP_REG(chan) = 100;	// Very high note, hopefully outside of what you hear
	SHIFT_REG(chan) = backup & 0xff;
	CTLB_REG(chan) = (backup >> 4) & 0xf0;
	CTLA_REG(chan) = (taps & 0x0080) + 0x18 +	// Enable count
	orchestra[active_instrument[chan] * 8 + 2] + (orchestra[active_instrument[chan] * 8 + 3] << 5);

	// Define the volume envelope
	wanted_volume[chan] = settings[4];
}

void abc_setinstrument(char chan, int instrument)
{
	if ((instrument > 0) && (instrument < 6)) {
		instrument--;
		active_instrument[chan] = instrument;
		set_sound_engine(chan, &orchestra[8*instrument]);
        }
}

void abc_refresh()
{
	char instrument;
	char chan;
	for (chan = 0; chan < 4; chan++) {
		instrument = active_instrument[chan];
		set_sound_engine(chan, &orchestra[8*instrument]);
	}
}

void abc_pitch(char chan, char pitch)
{
	BKUP_REG(chan) = pitch;
	VOLUME_REG(chan) = orchestra[active_instrument[chan] * 8 + 4];
}

void abc_note(char chan, int height, int note_duration)
{
	char tmp, octave;
	tmp = CTLA_REG(chan) & 0xf8;
	octave = orchestra[active_instrument[chan] * 8 + 2];
        while (height < 0) {
               height += 12;
               octave++;
        }
        while (height > 11) {
               height -= 12;
               octave--;
        }
	CTLA_REG(chan) = tmp | (octave + oct[height]);
	BKUP_REG(chan) = delays[height];	// Set the timer reload value
	envelope_mode[chan] = ATTACK_MODE;
	duration[chan] = note_duration;
}

void abc_rest(char chan, int note_duration)
{
	envelope_mode[chan] = DECAY_MODE;
	duration[chan] = note_duration;
	VOLUME_REG(chan) = 0;
}

void abc_pause()
{
	VOLUME_REG(0) = 0;
	VOLUME_REG(1) = 0;
	VOLUME_REG(2) = 0;
	VOLUME_REG(3) = 0;
}

void abc_setvolume(char chan, int volume)
{
	wanted_volume[chan] = (char) volume;
	VOLUME_REG(chan) = (char) volume;
}

// This is just a helper function if the Audio hardware cannot
// take care of playing the sound without CPU help.
// If I want to use envelopes for notes I need to call this
// routine once whenever I call the abc_metronome routine.
void abc_sound(void)
{
	char chan;
	int hold, decay;

	for (chan = 0; chan < NR_OF_CHANNELS; chan++) {
		if (duration[chan] > 0)
			duration[chan]--;
		switch (envelope_mode[chan]) {
		case ATTACK_MODE:
			vol[chan] += orchestra[active_instrument[chan] * 8 + 5];
			if (vol[chan] >= wanted_volume[chan]) {
				envelope_mode[chan] = HOLD_MODE;
				vol[chan] = wanted_volume[chan];
				hold = orchestra[active_instrument[chan] * 8 + 6];
				if (duration[chan] < hold)
					hold_duration[chan] = duration[chan];
				else
					hold_duration[chan] = hold;
			}
			VOLUME_REG(chan) = vol[chan];
			break;
		case HOLD_MODE:
			hold_duration[chan]--;
			if (hold_duration[chan] <= 0) {
				envelope_mode[chan] = DECAY_MODE;
			}
			break;
		case DECAY_MODE:
			if (vol[chan] > 0) {
				decay = orchestra[active_instrument[chan] * 8 + 7];
				if (vol[chan] > decay) {
					vol[chan] -= decay;
					if (decay == 0)
						vol[chan]--;
				} else {
					vol[chan] = 0;
				}
				VOLUME_REG(chan) = vol[chan];
			}
			break;
		}
	}
}
