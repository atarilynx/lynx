; ABC music definition file

abcsilence  db 0

; Set all scores to silent at startup
silence::
  lda #0
  sta $fd50 ; all channels to left and right
  lda #<abcsilence
  ldy #>abcsilence
  ldx #0
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #1
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #2
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #3
  jmp _abc_set_score

; Read in ASCII number
; result is in abc_tmp
abc_read_number::
  stz abc_tmp
  stz abc_tmp+1
.abc_read_number1
  jsr abc_read_char
  cmp #$2f
  bcc .abc_read_number2
  cmp #$39
  bcs .abc_read_number2
  and #$0f
  pha
  lda abc_tmp
  clc
  rol
  sta abc_tmp
  clc
  rol
  clc
  rol
  _IFCS
    inc abc_tmp+1
  _ENDIF
  clc
  adc abc_tmp
  _IFCS
    inc abc_tmp+1
  _ENDIF
  sta abc_tmp
  pla
  clc
  adc abc_tmp
  _IFCS
    inc abc_tmp+1
  _ENDIF
  sta abc_tmp
  bra .abc_read_number1
.abc_read_number2
  jsr abc_dec_score
  lda abc_tmp
  rts

; Activate score on channel X
; A - low address
; Y - high address
_abc_set_score::
  inx
  dex
  _IFEQ
    ldx sound_channel_busy
    _IFEQ
      sta abc_score_ptr0
      sty abc_score_ptr0+1
      sty abc_repeat_hoffs
      stz abc_music_ptr
      stz sound_channel_duration
    _ENDIF
    rts
  _ENDIF
  dex
  _IFEQ
    ldx sound_channel_busy+1
    _IFEQ
      sta abc_score_ptr1
      sty abc_score_ptr1+1
      sty abc_repeat_hoffs+1
      stz abc_music_ptr+1
      stz sound_channel_duration+1
    _ENDIF
    rts
  _ENDIF
  dex
  _IFEQ
    ldx sound_channel_busy+2
    _IFEQ
      sta abc_score_ptr2
      sty abc_score_ptr2+1
      sty abc_repeat_hoffs+2
      stz abc_music_ptr+2
      stz sound_channel_duration+2
    _ENDIF
    rts
  _ENDIF
  ldx sound_channel_busy+3
  _IFEQ
    sta abc_score_ptr3
    sty abc_score_ptr3+1
    sty abc_repeat_hoffs+3
    stz abc_music_ptr+3
    stz sound_channel_duration+3
  _ENDIF
  rts

; Once at each frame we can update the music
; You should call this routine frequently.
; Once in a frame is a good idea.
update_music::
  ldx #0
update_channel_x:
  lda sound_channel_duration,x
  _IFEQ
    ; note has ended, fetch next
    lda abc_music_ptr,x
    tay
    bra parse_abc
  _ENDIF
  ; note is playing
  cmp #255 ; Duration 255 is forever, good for engines
  _IFNE
    dec
    sta sound_channel_duration,x
  _ENDIF
update_channel_tail:
  lda sound_channel_maxlen,x
  _IFNE
    dec
    sta sound_channel_maxlen,x
    _IFEQ
      sta sound_channel_max_volume,x
    _ENDIF
  _ENDIF
  lda sound_channel_max_volume,x
  _IFEQ
    ; silence
    lda sound_channel_volume,x
    _IFNE
      ; decay time still going on
      sec
      sbc abc_instrument_decr,x
      _IFCC
        ; silence
        lda #0
      _ENDIF
      sta sound_channel_volume,x
    _ENDIF
  _ENDIF
  lda sound_channel_volume,x
  cmp sound_channel_max_volume,x
  _IFLO
    ; attack time
    clc
    adc abc_instrument_incr,x
    _IFCS
      ; desired volume reached
      lda sound_channel_max_volume,x
    _ENDIF
    cmp sound_channel_max_volume,x
    _IFHI
      ; desired volume reached
      lda sound_channel_max_volume,x
    _ENDIF
    sta sound_channel_volume,x
  _ENDIF
  lda sound_channel_volume,x
  phx
  pha
  txa
  clc
  rol
  clc
  rol
  clc
  rol
  tax
  pla
  sta $fd20,x
  plx
  inx
  txa
  cmp #4
  bne update_channel_x
  rts

; Parse score enough to get next note
; X - channel to use
; Y - abc music pointer
parse_abc::
  jsr abc_read_char
  cmp #$0 ; End of music
  _IFEQ
    sta sound_channel_busy,x
    bra update_channel_tail
  _ENDIF
  cmp #$20 ;' ' ignore spaces
  _IFEQ
    bra parse_abc
  _ENDIF
  cmp #$7c ;'|'
  _IFEQ
    jsr abc_read_char
    cmp #$3a ;':'
    _IFEQ
      tya
      sta abc_repeat_offs,x
      phx
      txa
      asl
      txa
      lda abc_score_ptr0+1,x
      plx
      sta abc_repeat_hoffs,x
      lda #2
      sta abc_repeat_cnt,x
    _ELSE
      jsr abc_dec_score
    _ENDIF
    jmp parse_abc
  _ENDIF
  cmp #$3a ;':'
  _IFEQ
    lda abc_repeat_cnt,x
    dec
    sta abc_repeat_cnt,x
    _IFNE
      phy
      lda abc_repeat_hoffs,x
      pha
      txa
      asl
      txa
      pla
      sta abc_score_ptr0+1,x
      txa
      clc
      ror
      tax
      lda abc_repeat_offs,x
      tay
      pla
      sta abc_repeat_offs,x
      jmp parse_abc
    _ENDIF
  _ENDIF
  cmp #$50 ;'P' priority - wait until sound has ended
  _IFEQ
    sta sound_channel_busy,x
    jmp parse_abc
  _ENDIF
  cmp #$56 ;'V' volume
  _IFEQ
    jsr abc_read_number
    sta abc_note_volume,x
    jmp parse_abc
  _ENDIF
  cmp #$52 ;'R' ramp up
  _IFEQ
    jsr abc_read_number
    sta abc_instrument_incr,x
    jmp parse_abc
  _ENDIF
  cmp #$48 ;'H' hold
  _IFEQ
    jsr abc_read_number
    sta abc_instrument_maxlen,x
    jmp parse_abc
  _ENDIF
  cmp #$4a ;'K' hold
  _IFEQ
    jsr abc_read_number
    sta abc_instrument_decr,x
    jmp parse_abc
  _ENDIF
  cmp #$49 ;'I' incremental
  _IFEQ
    jsr abc_read_number
    phx
    txa
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    lda abc_tmp
    cmp #0
    _IFEQ
      lda $fd25,x
      and #$df
    _ELSE
      lda $fd25,x
      ora #$20
    _ENDIF
    ora #$18
    sta $fd25,x
    plx
    jmp parse_abc
  _ENDIF
  cmp #$54 ;'T' tempo
  _IFEQ
    jsr abc_read_number
    sta abc_note_length,x
    jmp parse_abc
  _ENDIF
  cmp #$4f ;'O' octave
  _IFEQ
    jsr abc_read_number
    phx
    txa
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    lda $fd25,x
    and #$f8
    ora #$18
    ora abc_tmp
    sta $fd25,x
    plx
    jmp parse_abc
  _ENDIF
  cmp #$58 ;'X' XOR taps
  _IFEQ
    jsr abc_read_number
    phx
    txa ; modify X to point to sound channnel
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    lda abc_tmp
    ; The two topmost bits are shifted one right
    _IFMI
      ora #$40
    _ELSE
      and #$bf
    _ENDIF
    dec abc_tmp+1
    _IFNE
       and #$7f
    _ELSE
       ora #$80
    _ENDIF
    sta $fd21,x
    ; Bit $40 is in a different register
    lda abc_tmp
    and #$40
    _IFEQ
      lda $fd25,x
      and #$7f
    _ELSE
      lda $fd25,x
      ora #$80
    _ENDIF
    ora #$18
    sta $fd25,x
    lda #0
    sta $fd23,x
    plx
    jmp parse_abc
  _ENDIF
  cmp #$7a ;'z'
  _IFEQ
    lda #0
    bra set_music_ptr
  _ENDIF
  ; Find out the pitch of the note
  stz cur_note
  inc cur_note
  cmp #$3d ;'='
  _IFEQ
    inc cur_note
    jsr abc_read_char
  _ENDIF
  cmp #$7e ;'~'
  _IFEQ
    dec cur_note
    jsr abc_read_char
  _ENDIF
  sec
  sbc #$41 ;'A'
  cmp #8 ;'H'-'A'
  _IFLO
    clc
    asl
    clc
    adc cur_note
    sta cur_note
  _ELSE
    sec
    sbc #$20 ;'a'-'A' + 15
    clc
    asl
    clc
    adc cur_note
    clc
    adc #15
    sta cur_note
  _ENDIF
  lda cur_note
  phy
  tay
  lda _delays,y
  phx
  pha
  txa
  clc
  rol
  clc
  rol
  clc
  rol
  tax
  pla
  sta $fd24,x
  plx
  ply
  ; Find out the volume of the note
  lda abc_note_volume,x
set_music_ptr:
  sta sound_channel_max_volume,x
  ; Find out the duration of the note
  jsr abc_read_char
  cmp #$34 ; "4"
  _IFEQ
    lda abc_note_length,x
    clc
    rol
    clc
    rol
  _ELSE
    cmp #$33 ; "3"
    _IFEQ
      lda abc_note_length,x
      clc
      rol
      clc
      adc abc_note_length,x
    _ELSE
      cmp #$32 ; "2"
      _IFEQ
        lda abc_note_length,x
        clc
        rol
      _ELSE
        jsr abc_dec_score
        lda abc_note_length,x
      _ENDIF
    _ENDIF
  _ENDIF
  sta sound_channel_duration,x
  tya
  sta abc_music_ptr,x
  lda abc_instrument_maxlen,x
  sta sound_channel_maxlen,x
  jmp update_channel_x

; This table is used to cover the delays needed for 2 octaves
_delays db 161 ; Ab
        db 152 ; A
        db 143 ; A# Bb
        db 135 ; B
        db 128 ;
        db 255 ; C
        db 241 ; C# Db
        db 227 ; D
        db 214 ; D# Eb
        db 202 ; E
        db 191 ;
        db 191 ; F
        db 180 ; F# Gb
        db 170 ; G
        db 161 ; G#
_delays2
        db 80  ; ab
        db 76  ; a
        db 72  ; a# bb
        db 68  ; b
        db 128 ;
        db 128 ; c
        db 120 ; c# db
        db 114 ; d
        db 107 ; d# eb
        db 101 ; e
        db 96  ;
        db 96  ; f
        db 90  ; f# gb
        db 85  ; g
        db 80  ; g#

; Read a character from the score. Advance ptr if it is not 0
; X - channel
; Y - score offset
abc_read_char::
  txa
  inc
  dec
  _IFEQ
    lda (abc_score_ptr0),y
    iny
    _IFEQ
      inc abc_score_ptr0+1
    _ENDIF
  _ELSE
    dec
    _IFEQ
      lda (abc_score_ptr1),y
      iny
      _IFEQ
        inc abc_score_ptr1+1
      _ENDIF
    _ELSE
      dec
      _IFEQ
        lda (abc_score_ptr2),y
        iny
        _IFEQ
          inc abc_score_ptr2+1
        _ENDIF
      _ELSE
        lda (abc_score_ptr3),y
        iny
        _IFEQ
          inc abc_score_ptr3+1
        _ENDIF
      _ENDIF
    _ENDIF
  _ENDIF
  rts

; Decrement score pointer
; X - channel
; Y - score offset
abc_dec_score::
  phy
  txa
  tay
  pla
  iny
  dey
  _IFEQ
    sec
    sbc #1
    _IFCC
      dec abc_score_ptr0+1
    _ENDIF
  _ELSE
    dey
    _IFEQ
      sec
      sbc #1
      _IFCC
        dec abc_score_ptr1+1
      _ENDIF
    _ELSE
      dey
      _IFEQ
        sec
        sbc #1
        _IFCC
          dec abc_score_ptr2+1
        _ENDIF
      _ELSE
        sec
        sbc #1
        _IFCC
          dec abc_score_ptr3+1
        _ENDIF
      _ENDIF
    _ENDIF
  _ENDIF
  tay
  rts

abc_music_ptr   db 0,0,0,0
abc_repeat_cnt db 0,0,0,0
abc_repeat_offs db 0,0,0,0
abc_repeat_hoffs db 0,0,0,0
abc_note_length db 6,6,6,6
abc_note_volume db 64,64,64,64
abc_instrument_incr   db 4,4,4,4
abc_instrument_maxlen db 4,4,4,4
abc_instrument_decr   db 4,4,4,4
sound_channel_busy    db 0,0,0,0
sound_channel_max_volume db 60,127,127,127
sound_channel_volume   db 4,4,4,4
sound_channel_maxlen   db 4,4,4,4
sound_channel_duration db 0,0,0,0
cur_note    dc.b 0
abc_tmp db 0,0

