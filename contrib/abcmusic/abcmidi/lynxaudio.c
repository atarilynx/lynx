#include <stdio.h>

// What the shift hardware really does.
short sh(short reg, short taps)
{
    short tp, bit;
    tp = taps;
    bit = (1 ^ (tp & reg) ^
          ((tp >> 1) & (reg >> 1)) ^
          ((tp >> 2) & (reg >> 2)) ^
          ((tp >> 3) & (reg >> 3)) ^
          ((tp >> 4) & (reg >> 4)) ^
          ((tp >> 5) & (reg >> 5)) ^
          ((tp >> 6) & (reg >> 7)) ^
          ((tp >> 7) & (reg >> 10)) ^
          ((tp >> 8) & (reg >> 11))) & 1;
    return ((reg << 1) | bit) & 4095;
}

int main(int argc, char *argv[])
{
    short reg0 = 0;
    short tap0 = 7;

    int i;
    tap0 = atoi(argv[1]);
    for (i = 0; i < 1000; i++) {
        reg0 = sh(reg0, tap0);
        if (reg0 & 1)
            printf("100\n");
        else
            printf("0\n");
    }
}
