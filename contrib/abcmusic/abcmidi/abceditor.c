/*
 * This is a simplified abc music library
 */
#include <string.h>

static unsigned char delay_register;
static int taps_register;
static int backup_register;
static unsigned char integrate_register;
static unsigned char volume_register;

/* Lynx specific part
   These timer delays are just relative stuff. The real delays could be
   calibrated by entering real values in this table.
   But we also need a different table for every loop length.
 */
char delays[] = {
        255, // C
        241, // C# Db
        227, // D
        214, // D# Eb
        202, // E
        191, // F
        180, // F# Gb
        170, // G
        161, // G# Ab
        152, // A
        143, // A# Bb
        135, // B
        128, // c
        120, // c# db
        114, // d
        107, // d# eb
        101, // e
        96 , // f
        90 , // f# gb
        85 , // g
        80 , // g# ab
        76 , // a
        72 , // a# bb
        68   // b
};

/*
  Generic part
 */
char note_indexes[] = {
	8, // Ab
        9, // A
        10, // A# Bb
        11, // B
        12, //
        0, // C
        1, // C# Db
        2, // D
        3, // D# Eb
        4, // E
        5, //
        5, // F
        6, // F# Gb
        7, // G
        8, // G#
        20 , // ab
        21 , // a
        22 , // a# bb
        23 , // b
        12, //
        12, // c
        13, // c# db
        14, // d
        15, // d# eb
        16, // e
        17, //
        17, // f
        18, // f# gb
        19, // g
        20  // g#
};

static unsigned char control;
static int sound_length;
static unsigned char sound_volume;
static unsigned char sound_octave;
static int repeat_count;
static char *section_start;
static unsigned char instrument;

char *parse_string(
    char *tune,
    int *nr,
    int *height,
    char *control
    )
{
    *control = 1;
    *height = 1;
    if (*tune == '^') {
	++(*height);
	++tune;
    }
    if (*tune == '_') {
	--(*height);
	++tune;
    }
    if (*tune >= 'A' && *tune <= 'G') {
	*height = note_indexes[*height + (*tune - 'A') * 2];
	++tune;
	while (*tune == ',') {
	    ++tune;
	    *height -= 15;
	}
    } else {
	if (*tune >= 'a' && *tune <= 'g') {
            *height = note_indexes[*height + (*tune - 'a') * 2 + 15];
	    ++tune;
	    while (*tune == '\'') {
	        ++tune;
	        *height += 15;
	    }
	} else {
	    *control = *tune;
	    if (*tune)
		++tune;
	}
    }
    *nr = 0;
    while (*tune >= '0' && *tune <= '9') {
        *nr = 10 * (*nr) + *tune - '0';
	++tune;
    }
    if (!*nr)
        ++(*nr);
    return tune;
}

static char *get_token(char *tune, char chan)
{
    int nr;
    int sound_index;

    tune = parse_string(tune, &nr, &sound_index, &control);
    switch (control) {
    case 0:
	break;
    case 1:
	delay_register = delays[sound_index];
        sound_octave = octave[chan];
	sound_length = nr * (tempo[chan]);
	sound_volume = vol[chan];
	break;
    case 'z':
	sound_volume = 0;
	sound_length = nr * (tempo[chan]);
	break;
    case 'O':
	octave[chan] = nr;
	break;
    case 'T':
	tempo[chan] = nr;
	break;
    case 'V':
	vol[chan] = nr;
	break;
    case 'R':
	attack[chan] = nr;
	break;
    case 'H':
	hold[chan] = nr;
	break;
    case 'K':
	decay[chan] = nr;
	break;
    case '|':
	repeat_count = nr;
	section_start = tune;
	break;
    case ':':
	if (repeat_count)
	    --repeat_count;
	if (repeat_count)
	    tune = section_start;
	break;
    // The instrument setting will replace the Lynx specific part
    // in the future.
    case 'I':
	instrument = nr;
	break;
    // Start of Lynx-specific registers
    case 'X':
	taps_register = nr;
	break;
    case 'Y':
	backup_register = nr;
	break;
    case 'S':
	integrate_register = nr;
	break;
    }
    return tune;
}
