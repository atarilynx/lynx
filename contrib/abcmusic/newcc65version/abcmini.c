/*
 * This is a Public Domain template for creating simple games for
 * the Atari Lynx in C.
 */
#include <lynx.h>
#include <lynxlib.h>
#include <stdlib.h>

/* Routines defined in abcmusic.m65 */
extern void silence();
extern void abcmusic();
extern void update_music();

extern char abcmusic0[];
extern char abcmusic1[];
extern char abcmusic2[];
extern char abcmusic3[];

#asm
          xref _abcmusic0
          xref _abcmusic1
          xref _abcmusic2
          xref _abcmusic3

_abcmusic0  
            dc.b "O1X7I0V45T20R40H2K3" 
            dc.b "|:CDEEDEFFEFGEFDGG"
            dc.b "I1CDEEDEFFEFGEFDCC"
            dc.b "I0cBAABAGGAGFFEDEE"
            dc.b "cBAABAGGAGFFEDCC"
            dc.b "ECDDFDEEGEFGABcc"
            dc.b "ECDDFDEEGEFGABcc"
            dc.b "CDEEDEFFEFGEFDGG"
            dc.b "CDEEDEFFEFGEFDCC:"
            dc.b 0

_abcmusic1  
            dc.b "O2X7I0V45T20R80H8K1" 
            dc.b "|:C4D4E4F4"
            dc.b "C4D4E4F4"
            dc.b "c4B4A4E4"
            dc.b "c4B4A4E4"
            dc.b "E4F4G4A4"
            dc.b "E4F4G4A4"
            dc.b "C4D4E4F4"
            dc.b "C4D4E4F4:"
            dc.b 0

_abcmusic2  
            dc.b "O1X7I0V45T20R40H2K3" 
            dc.b "zzzzzzzz"
            dc.b "|:CDEEDEFFEFGEFDGG"
            dc.b "I1CDEEDEFFEFGEFDCC"
            dc.b "I0cBAABAGGAGFFEDEE"
            dc.b "cBAABAGGAGFFEDCC"
            dc.b "ECDDFDEEGEFGABcc"
            dc.b "ECDDFDEEGEFGABcc"
            dc.b "CDEEDEFFEFGEFDGG"
            dc.b "CDEEDEFFEFGEFDCC:"
            dc.b 0

_abcmusic3  
            dc.b "O2X7I0V45T20R80H8K1" 
            dc.b "z4z4z2"
            dc.b "|:C4D4E4F4"
            dc.b "C4D4E4F4"
            dc.b "c4B4A4E4"
            dc.b "c4B4A4E4"
            dc.b "E4F4G4A4"
            dc.b "E4F4G4A4"
            dc.b "C4D4E4F4"
            dc.b "C4D4E4F4:"
            dc.b 0

#endasm

uchar SCREEN1[8160]       at (0xfff8-2*8160); // screen 1
uchar SCREEN2[8160]       at (0xfff8-1*8160); // screen 2

char pal[]={
	0x00,0x00,0x01,0x05,0x08,0x0b,0x0e,0x00,0x02,0x00,0x03,0x07,0x0a,0x0d,0x0f,0x00,
	0x00,0xd0,0x11,0x55,0x88,0xbb,0xee,0x0d,0x22,0xf0,0x33,0x77,0xaa,0xdd,0xff,0x0f};
 
/*
  Using drawPending and swapping buffers in the interrupt
  creates a flicker-free display. Idea borrowed from Tom Schenks.
*/
char drawPending;

VBL() interrupt
{
    if (drawPending) {
        SwapBuffers();
        drawPending = 0;
    }
    update_music();
}

int main()
{
    uchar ret;

    InitIRQ();
    /*
       To save the cart connector during developement
       The next line allows you to upload the next version without
       swapping carts.
    */
    InstallUploader(_9600Bd);
    CLI;
    SetBuffers(SCREEN1, SCREEN2, 0);
    _SetRGB(pal);
    InstallIRQ(2,VBL);
    EnableIRQ(2);
    silence();
    ret = 1;
    abcmusic(0,abcmusic0);
    abcmusic(1,abcmusic1);
    abcmusic(2,abcmusic2);
    abcmusic(3,abcmusic3);
    while (ret) {
        if (!drawPending) {
            /* DrawSprite(Sbackgrnd); */
            drawPending = 1;
        }
    }
    /* Jump to cart loader */
}
