////////////////////////////////////////////////////////////////////////////////
//
// Includes
//
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <6502.h>
#include <lynx.h>
#include <tgi.h>
#include <peekpoke.h>
#include <string.h>
#include <joystick.h>
#include "LynxSD.h"

extern char lynxtgi[];
extern char lynxjoy[];

////////////////////////////////////////////////////////////////////////////////
//
// Lynx registers
//
////////////////////////////////////////////////////////////////////////////////

volatile u8 * const MSTERE0 = (volatile u8 *) 0xFD50;
volatile u8 * const MAPCTL = (volatile u8 *) 0xFFF9;

////////////////////////////////////////////////////////////////////////////////
//
// Globals
//
////////////////////////////////////////////////////////////////////////////////

static SDirEntry gsDirEntry[256];
static u8 ganDirOrder[256];
static u8 gnNumDirEntries = 0;
static u8 gnTopLine = 0, gnSelectedLine = 0;
static char gszCurrentDir[256] = "";
static char gszCurrentFile[13] = "";

////////////////////////////////////////////////////////////////////////////////
//
// Game menu code
//
////////////////////////////////////////////////////////////////////////////////

void __fastcall__ AddDirEntry(const char *pIn, u8 bIsDir)
{
	u8 nEntry = 0;
	SDirEntry *pDir;

	if (gnNumDirEntries == 255) return;

//-- Find place to insert entry

	while (nEntry < gnNumDirEntries)
	{
		pDir = &gsDirEntry[ganDirOrder[nEntry]];

		if (bIsDir && !pDir->bDirectory) // directories first
		{
			break;
		}

		if ((bIsDir == pDir->bDirectory) && (stricmp(pIn, pDir->szFilename) < 0))
		{
			break;
		}

		nEntry++;
	}

//-- Shuffle order list as needed

	if (nEntry < gnNumDirEntries)
	{
		u8 nEnd = gnNumDirEntries;
		while (nEnd > nEntry)
		{
			ganDirOrder[nEnd] = ganDirOrder[nEnd-1];
			nEnd--;
		}
	}

//-- Create the physical new entry

	pDir = &gsDirEntry[gnNumDirEntries];
	strcpy(pDir->szFilename, pIn);
	pDir->bDirectory = bIsDir;

//-- Insert into sort list

	ganDirOrder[nEntry] = gnNumDirEntries;
	gnNumDirEntries++;
}

void __fastcall__ ReadDirectory(const char *pDir)
{
	SFileInfo sInfo;
	const char *pExt;
	u8 bIsLnxFile;

	tgi_clear();
	tgi_outtextxy(40, 46, "Reading...");
	tgi_updatedisplay();
 
//-- If this is a subdir, add ".." as first entry

	gnNumDirEntries = 0;
	if (pDir[0] != 0)
	{
		AddDirEntry("..", 1);
	}

//-- Open and read the dir

	if (LynxSD_OpenDir(pDir) == FR_OK)
	{
		while (LynxSD_ReadDir(&sInfo) == FR_OK)
		{
			bIsLnxFile = 0;
			pExt = sInfo.fname;
			while (*pExt && *pExt != '.')
			{
				pExt++;
			}
			if (*pExt == '.')
			{
				pExt++;
				if (pExt[0] == 'L' && pExt[1] == 'N' && pExt[2] == 'X')
				{
					bIsLnxFile = 1;
				}
			}

			if (bIsLnxFile || (sInfo.fattrib & AM_DIR))
			{
				AddDirEntry(sInfo.fname, sInfo.fattrib & AM_DIR);
			}

		}
	}
}

void DisplayDirectory()
{
	SDirEntry *pDir;
	u8 line = 0;
	u8 nMax = gnNumDirEntries - gnTopLine;

	if (nMax > 12)
	{
		nMax = 12;
	}
	nMax += gnTopLine;
	line = gnTopLine;
 
	tgi_clear();

	while (line < nMax)
	{
		u8 nScreenLine = line - gnTopLine;

		pDir = &gsDirEntry[ganDirOrder[line]];
		if (pDir->bDirectory)
		{
			tgi_outtextxy(8, 3+(nScreenLine*8), "[");
			tgi_outtextxy(16, 3+(nScreenLine*8), pDir->szFilename);
			tgi_outtextxy(16+(strlen(pDir->szFilename)*8), 3+(nScreenLine*8), "]");
		}
		else
		{
			tgi_outtextxy(8, 3+(nScreenLine*8), pDir->szFilename);
		}

		if (gnSelectedLine == line)
		{
			tgi_outtextxy(0, 3+(nScreenLine*8), ">");
		}

		line++;
	}

	tgi_updatedisplay();
}

void initialize()
{
	tgi_install(&lynxtgi);
	tgi_init();
	CLI();
	
	while (tgi_busy()) 
	{ 
	};

	tgi_setpalette(tgi_getdefpalette());	
	tgi_setcolor(COLOR_WHITE);
	tgi_setbgcolor(COLOR_BLACK); 
	tgi_clear();

	joy_install(&lynxjoy);
}

void main(void) 
{	
	u8 nJoyDown = 0, nJoyUp = 0, nLastJoy = 0, nJoyDownDelay = 0, nJoyUpDelay = 0;

	LynxSD_Init();

	initialize();

	tgi_setcolor(COLOR_WHITE);
	tgi_setbgcolor(0);

	tgi_clear();
	tgi_updatedisplay();


	if (LynxSD_OpenFile("lastrom") == FR_OK)
	{
		char *pFile;

		LynxSD_ReadFile(gszCurrentDir, 256);
		LynxSD_CloseFile();

		pFile = gszCurrentDir + strlen(gszCurrentDir);
		while (pFile > gszCurrentDir && *pFile != '/') pFile--;
		if (*pFile == '/')
		{
			*pFile++ = 0;
			strcpy(gszCurrentFile, pFile);
		}
		else
		{
			strcpy(gszCurrentFile, gszCurrentDir);
			gszCurrentDir[0] = 0;
		}
	}

	ReadDirectory(gszCurrentDir);

	if (gszCurrentFile[0])
	{
		u8 n;
		for (n = 0; n < gnNumDirEntries; n++)
		{
			SDirEntry *pDir = &gsDirEntry[ganDirOrder[n]];
			if (stricmp(pDir->szFilename, gszCurrentFile) == 0)
			{
				gnSelectedLine = n;
				break;
			}
		}
	}

	while (1)
	{
		if (!tgi_busy())
		{
			u8 nThisJoy = joy_read(JOY_1);
			u8 nPressed = nThisJoy & ~nLastJoy;
			u8 nReleased = ~nThisJoy & nLastJoy;
			nLastJoy = nThisJoy;

		//-- Reset the repeat counters

			if (JOY_BTN_UP(nPressed|nReleased))
			{
				nJoyUp = 0;
				nJoyUpDelay = 14;
			}
			if (JOY_BTN_DOWN(nPressed|nReleased))
			{
				nJoyDown = 0;
				nJoyDownDelay = 14;
			}

		//-- Update the repeat counters

			if (JOY_BTN_UP(nThisJoy))
			{
				nJoyUp++;
				if (nJoyUp > nJoyUpDelay)
				{
					nJoyUp = 1;
					nJoyUpDelay = 2;
				}
			}
			if (JOY_BTN_DOWN(nThisJoy))
			{
				nJoyDown++;
				if (nJoyDown > nJoyDownDelay)
				{
					nJoyDown = 1;
					nJoyDownDelay = 2;
				}
			}

		//-- Do up / down

			if (nJoyUp == 1)
			{
				if (gnSelectedLine > 0)
				{
					gnSelectedLine--;
				}
			}

			if (nJoyDown == 1)
			{
				if (gnSelectedLine < (gnNumDirEntries-1))
				{
					gnSelectedLine++;
				}
			}

			if (gnSelectedLine < gnTopLine)
			{
				gnTopLine = gnSelectedLine;
			}
			else if (gnSelectedLine >= (gnTopLine + 12))
			{
				gnTopLine = gnSelectedLine-11;
			}

		//-- Selection

			if (JOY_BTN_FIRE(nReleased))
			{
				SDirEntry *pDir = &gsDirEntry[ganDirOrder[gnSelectedLine]];

			//-- Check for directory actions

				if (pDir->bDirectory)
				{
					u8 bDoRead = 0;

				//-- Do back up directory

					if (pDir->szFilename[0] == '.' && pDir->szFilename[1] == '.' && pDir->szFilename[2] == 0)
					{
						char *pEnd = gszCurrentDir + strlen(gszCurrentDir);
						while (--pEnd >= gszCurrentDir)
						{
							if ((*pEnd == '/') || (pEnd == gszCurrentDir))
							{
								*pEnd = 0;
								bDoRead = 1;
								break;
							}
						}

					}

				//-- Do forward directory
					
					else
					{
						if ((strlen(gszCurrentDir) + strlen(pDir->szFilename) + 2) <= 255)
						{
							if (gszCurrentDir[0])
							{
								strcat(gszCurrentDir, "/");
							}
							strcat(gszCurrentDir, pDir->szFilename);
							bDoRead = 1;
						}
					}

					if (bDoRead)
					{
						ReadDirectory(gszCurrentDir);
						gnTopLine = 0;
						gnSelectedLine = 0;
						continue;
					}
				}

			//-- Otherwise its a file

				else
				{
					if ((strlen(gszCurrentDir) + strlen(pDir->szFilename) + 2) <= 255)
					{
						char *pCurrentEnd = gszCurrentDir + strlen(gszCurrentDir);
						if (gszCurrentDir[0])
						{
							strcat(gszCurrentDir, "/");
						}
						strcat(gszCurrentDir, pDir->szFilename);

						tgi_clear();
						tgi_outtextxy(24, 46, "Programming...");
						tgi_updatedisplay();

						if (LynxSD_ProgramROM(gszCurrentDir) == FR_OK)
						{
							u8 *ptr;
							u8 count;

						//-- Write out the last rom played to the sd card

							if (LynxSD_OpenFile("lastrom") == FR_OK)
							{
								LynxSD_WriteFile(gszCurrentDir, 256);
								LynxSD_CloseFile();
							}

							LynxSD_LowPowerMode();
							*MSTERE0 = 0; // enable all audio channels
							asm("sei");
							*MAPCTL = 0; // memory mapping for boot state

							ptr = (u8*) 0xfd00; // timers and audio
							count = 0x40;
							while (count--)
							{
								*ptr++ = 0;
							}

							*((u8*) 0xFD80) = 0;
							*((u8*) 0xFD81) = 0;
							*((u8*) 0xFD92) = 0;
							*((u8*) 0xFD9C) = 0;
							*((u8*) 0xFD9D) = 0;
							*((u8*) 0xFD9E) = 0;
							*((u8*) 0xFD9D) = 0;

							ptr = (u8*) 0xfda0; // palette
							count = 0x20;
							while (count--)
							{
								*ptr++ = 0;
							}

							asm("brk");
						}
						*pCurrentEnd = 0;
					}
				}
			}

			DisplayDirectory();
		}
	};
}