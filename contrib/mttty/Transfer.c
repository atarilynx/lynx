/*-----------------------------------------------------------------------------
    This is a part of the Microsoft Source Code Samples. 
    Copyright (C) 1995 Microsoft Corporation.
    All rights reserved. 
    This source code is only intended as a supplement to 
    Microsoft Development Tools and/or WinHelp documentation.
    See these sources for detailed information regarding the 
    Microsoft samples programs.

    MODULE: Transfer.c

    PURPOSE: Transfer a file (receive or send).

    FUNCTIONS:
        TransferRepeatCreate   - Preps program for a repeated send
        TransferRepeatDestroy  - Completes a repeated send
        TransferRepeatDo       - Sends the data to the writer thread
        TransferFileLynxStart  - Preps program for a Lynx file send
        TransferFileTextStart  - Preps program for a text file send
        TransferFileTextEnd    - Completes a file transfer
        TransferThreadProc     - Thread procedure to do actual transfer
        TransferFileText       - Preps program for a text file send
        ReceiveFileText        - Preps program for a text file capture
        OpenTheFile            - Opens a file
        CreateTheFile          - Creates a file
        GetTransferSizes       - Determines transfer metrics from file and buffer sizes
        GetLynxTransferSizes   - Determines transfer metrics from file and buffer sizes
        ShowTransferStatistics - Displays transfer stats
        CheckForMessges        - Peek message check to keep things flowing
                                 during a transfer
        SendFile               - Send a file
        CaptureFile            - Sets the receive state for file capture
       
-----------------------------------------------------------------------------*/

#include <windows.h>
#include <commctrl.h>
#include "mttty.h"
#include "head.h"
#include "tail.h"
#include "uploadheader.h"

//
// Globals used in this file only
//
HANDLE hFile;
HANDLE hFile2Handle;
HANDLE hTransferAbortEvent;
HANDLE hTransferThread;
UINT   uTimerId;
UINT   uLynxBlockSize;
DWORD  dwLynxTransfers;
DWORD  dwTransferNr;
MMRESULT mmTimer = (MMRESULT)NULL;
char * lpBuf;
//
// Prototypes for functions called only within this file
//
DWORD WINAPI TransferThreadProc(LPVOID);
HANDLE OpenTheFile( LPCTSTR );
HANDLE CreateTheFile( LPCTSTR );
void CaptureFile( HANDLE, HWND );
UINT CheckForMessages( void );
BOOL GetTransferSizes( HANDLE, DWORD *, DWORD *, DWORD *);
BOOL GetLynxTransferSizes( HANDLE, DWORD *, DWORD *, DWORD *);


/*-----------------------------------------------------------------------------

FUNCTION: TransferRepeatCreate(LPCTSTR, DWORD)

PURPOSE: Prepares program for a repeated text file transfer (send)

PARAMETERS:
    lpstrFileName - name of file selected to send
    dwFrequency   - frequency of timer

COMMENTS: This function sets up a window timer to fire off
          every so often.  When it fires, TransferRepeatDo is
          called with the same name as above.  This causes the file transfer
          to actuall take place.
          TransferRepeatDestroy is called to kill the timer.
          This function disables certain menu items that should not be
          available for the duration of a repeated send even if the actual
          Tx is not taking place.

HISTORY:   Date:      Author:     Comment:
            1/29/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void TransferRepeatCreate(LPCTSTR lpszFileName, DWORD dwFrequency)
{
    HMENU hMenu;
    UINT  MenuFlags ;
    DWORD dwFileSize;
    DWORD dwMaxPackets;
    DWORD dwPacketSize;
    DWORD dwRead;

    //
    // open the file
    //
    hFile = OpenTheFile(lpszFileName);
    if (hFile == INVALID_HANDLE_VALUE)
        return;

    //
    // modify transfer menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_DISABLED | MF_GRAYED;
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_ABORTREPEATEDSENDING, MF_ENABLED);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);

    //
    // enable abort button and progress bar
    //
    SetWindowText(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), "Abort Tx");
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_SHOW);

    if (!GetTransferSizes(hFile, &dwPacketSize, &dwMaxPackets, &dwFileSize)) {
        TransferRepeatDestroy();
        return;
    }

    // Allocate a buffer
    lpBuf = HeapAlloc(ghWriterHeap, 0, dwFileSize);
    if (lpBuf == NULL) {
        ErrorReporter("HeapAlloc (data block from writer heap).\r\nFile is too large");
        TransferRepeatDestroy();
        return;
    }

    // fill the buffer
    if (!ReadFile(hFile, lpBuf, dwFileSize, &dwRead, NULL)) {
        ErrorReporter("Can't read from file\n");
        TransferRepeatDestroy();
    }

    if (dwRead != dwFileSize)
        ErrorReporter("Didn't read entire file\n");
        
    mmTimer = timeSetEvent((UINT) dwFrequency, 10, TransferRepeatDo, dwRead, TIME_PERIODIC);
    if (mmTimer == (MMRESULT) NULL) {
        ErrorReporter("Could not create mm timer");
        TransferRepeatDestroy();
    }
    else {
        REPEATING(TTYInfo) = TRUE;
        OutputDebugString("Timer setup.\n");
    }

    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferRepeatDestroy( void )

PURPOSE: Stops a repeated text file transfer (send)

COMMENTS: Kills the repeated-send timer.

HISTORY:   Date:      Author:     Comment:
            1/29/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void TransferRepeatDestroy()
{
    HMENU hMenu;
    DWORD MenuFlags;
    MMRESULT mmRes;

    if (mmTimer != (MMRESULT) NULL) {
        mmRes = timeKillEvent(mmTimer);
        if (mmRes != TIMERR_NOERROR)
            ErrorReporter("Can't kill mm timer");
        mmTimer = (MMRESULT) NULL;
    }

    // close the file
    CloseHandle(hFile);

    // inform writer to abort all pending write requests
    if (!WriterAddFirstNodeTimeout(WRITE_ABORT, 0, 0, NULL, NULL, NULL, 500))
        ErrorReporter("Couldn't inform writer to abort sending.");

    // free the buffer
    if (!HeapFree(ghWriterHeap, 0, lpBuf))
        ErrorReporter("HeapFree (data block from writer heap)");
    
    REPEATING(TTYInfo) = FALSE;
    OutputDebugString("Repeated transfer destroyed.\r\n");

    //
    // enable transfer menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_ENABLED;
    EnableMenuItem(hMenu, ID_TRANSFER_ABORTREPEATEDSENDING, MF_DISABLED | MF_GRAYED);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);

    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_HIDE);
    
    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferRepeatDo( void )

PURPOSE: Performs a single text file transfer (send)

COMMENTS: Allocates a block to hold the file.
          Prepares the writer packet.

HISTORY:   Date:      Author:     Comment:
            1/29/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void CALLBACK TransferRepeatDo( UINT uTimerId, 
                                        UINT uRes, 
                                        DWORD dwFileSize, 
                                        DWORD dwRes1, 
                                        DWORD dwRes2)
{
    if (!WriterAddNewNodeTimeout(WRITE_BLOCK, dwFileSize, 0, lpBuf, 0, 0, 10))
        PostMessage(ghwndMain, WM_COMMAND, ID_TRANSFER_ABORTSENDING, MAKELPARAM(IDC_ABORTBTN, 0) );
    
    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferFileLynxStart(LPCTSTR)

PURPOSE: Prepares program for a Lynx file transfer (send)

PARAMETERS:
    lpstrFileName - name of file selected to send

COMMENTS: Modifies menus and dialog control, then restores them

HISTORY:   Date:      Author:     Comment:
            7/24/01   KarriK      Wrote it

-----------------------------------------------------------------------------*/
void TransferFileLynxStart(LPCTSTR lpstrFileName)
{
    DWORD dwThreadId;
    HMENU hMenu;
    UINT  MenuFlags ;
	DWORD dwPacketSize, dwMaxPackets, dwFileSize;

    //
    // open the file
    //
    hFile = OpenTheFile(lpstrFileName);
    if (hFile == INVALID_HANDLE_VALUE)
        return;
    if (!GetLynxTransferSizes(hFile, &dwPacketSize, &dwMaxPackets, &dwFileSize))
        return;
	if ((uLynxBlockSize == 512) && !hFile2Handle) {
		if (hFile2Handle)
			CloseHandle(hFile2Handle);
		hFile2Handle = hFile;
		hFile = 0;
        OutputABufferToWindow(ghWndTTY, "You have chosen a 512 bytes/block file. The block structure of this flash\r\n", 75);
        OutputABufferToWindow(ghWndTTY, "is 1024 bytes so you need to choose one more 512 bytes/block file.\r\n", 68);
		return;
	}
    switch (uLynxBlockSize) {
	case 1:
        OutputABufferToWindow(ghWndTTY, "You are downloading a Lynx binary to RAM.\r\n", 43);
		break;
	case 512:
        OutputABufferToWindow(ghWndTTY, "You are flashing two 512 bytes/block files.\r\n", 45);
		break;
	case 1024:
        OutputABufferToWindow(ghWndTTY, "You are flashing a 1024 bytes/block file.\r\n", 43);
		break;
	case 2048:
        OutputABufferToWindow(ghWndTTY, "You are flashing a 2048 bytes/block file. S6 should be ON.\r\n", 60);
		break;
	}
    //
    // modify transfer menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_DISABLED | MF_GRAYED;
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_ABORTSENDING, MF_ENABLED);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);

    
    //
    // enable abort button and progress bar
    //
    gfAbortTransfer = FALSE;
    SetWindowText(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), "Abort Tx");
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_SHOW);
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS), SW_SHOW);

    // start the transfer thread
    hTransferAbortEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (hTransferAbortEvent == NULL)
        ErrorReporter("CreateEvent(Transfer Abort Event)");

    hTransferThread = CreateThread(NULL, 0, 
                                TransferThreadProc, 
                                (LPVOID) hFile, 0, &dwThreadId);

    if (hTransferThread == NULL) {
        ErrorReporter("CreateThread (Transfer Thread)");
        TransferFileTextEnd();
    }
    else
        TRANSFERRING(TTYInfo) = TRUE;

    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferFileTextStart(LPCTSTR)

PURPOSE: Prepares program for a text file transfer (send)

PARAMETERS:
    lpstrFileName - name of file selected to send

COMMENTS: Modifies menus and dialog control, then restores them

HISTORY:   Date:      Author:     Comment:
            1/26/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void TransferFileTextStart(LPCTSTR lpstrFileName)
{
    DWORD dwThreadId;
    HMENU hMenu;
    UINT  MenuFlags ;

    //
    // open the file
    //
    hFile = OpenTheFile(lpstrFileName);
    if (hFile == INVALID_HANDLE_VALUE)
        return;

    //
    // modify transfer menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_DISABLED | MF_GRAYED;
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_ABORTSENDING, MF_ENABLED);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);
    
    //
    // enable abort button and progress bar
    //
    gfAbortTransfer = FALSE;
    SetWindowText(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), "Abort Tx");
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_SHOW);
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS), SW_SHOW);

    // start the transfer thread
    uLynxBlockSize = 0;
    hTransferAbortEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (hTransferAbortEvent == NULL)
        ErrorReporter("CreateEvent(Transfer Abort Event)");

    hTransferThread = CreateThread(NULL, 0, 
                                TransferThreadProc, 
                                (LPVOID) hFile, 0, &dwThreadId);

    if (hTransferThread == NULL) {
        ErrorReporter("CreateThread (Transfer Thread)");
        TransferFileTextEnd();
    }
    else
        TRANSFERRING(TTYInfo) = TRUE;

    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferFileTextEnd()

PURPOSE: Stops a text file transfer (send)

COMMENTS: Modifies menus and dialog control, then restores them

HISTORY:   Date:      Author:     Comment:
           1/26/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void TransferFileTextEnd()
{
    HMENU hMenu;
    UINT MenuFlags ;

    // stop the transfer thread
    SetEvent(hTransferAbortEvent);

    OutputDebugString("Waiting for transfer thread...\n");

    if (WaitForSingleObject(hTransferThread, 3000) != WAIT_OBJECT_0) {
        ErrorReporter("TransferThread didn't stop.");
        TerminateThread(hTransferThread, 0);
    }
    else
        OutputDebugString("Transfer thread exited\n");

    CloseHandle(hTransferAbortEvent);
    CloseHandle(hTransferThread);

    TRANSFERRING(TTYInfo) = FALSE;

    //
    // enable transfer menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_ENABLED;
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_ABORTSENDING, MF_DISABLED | MF_GRAYED);
    
    //
    // disable abort button and progress bar
    //
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_HIDE);
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS), SW_HIDE);

    //
    // close the file
    //
    CloseHandle(hFile);
	if (hFile2Handle)
		CloseHandle(hFile2Handle);
}


/*-----------------------------------------------------------------------------

FUNCTION: ReceiveFileText(LPCTSTR)

PURPOSE: Prepares program for a text file transfer (receive)

PARAMETERS:
    lpstrFileName - name of file selected for receiving 

COMMENTS: Modifies menus and control, then restores them

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void ReceiveFileText(LPCTSTR lpstrFileName)
{
    HMENU hMenu;
    UINT MenuFlags ;

    //
    // create the file
    //
    ghFileCapture = CreateTheFile(lpstrFileName);
    if (ghFileCapture == INVALID_HANDLE_VALUE)
        return;

    /*
        setup transfer
        disable file menu
    */
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_DISABLED | MF_GRAYED;
    EnableMenuItem(hMenu, ID_FILE_CONNECT, MenuFlags);
    EnableMenuItem(hMenu, ID_FILE_DISCONNECT, MenuFlags);
    
    //
    // disable transfer menu
    //
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);
    
    //
    // enable abort button and progress bar
    //
    gfAbortTransfer = FALSE;
    SetWindowText(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), "Close Capture");
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_SHOW);
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS), SW_SHOW);

    //
    // send file until done or abort
    //
    CaptureFile(ghFileCapture, GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS));

    //
    // enable menu
    //
    hMenu = GetMenu(ghwndMain);
    MenuFlags = MF_ENABLED;
    ChangeConnection(ghwndMain, CONNECTED(TTYInfo));
    
    //
    // enable transfer menu
    //
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILELYNX, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_RECEIVEFILETEXT, MenuFlags);
    EnableMenuItem(hMenu, ID_TRANSFER_SENDREPEATEDLY, MenuFlags);

    //
    // hide abort button and progress bar
    //
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_ABORTBTN), SW_HIDE);
    ShowWindow(GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS), SW_HIDE);
    
    gfAbortTransfer = FALSE;

    CloseHandle(ghFileCapture);

    return; // returns when file transfer is complete or aborted
}

/*-----------------------------------------------------------------------------

FUNCTION: OpenTheFile(LPCTSTR)

PURPOSE: Open a file and return the file handle

PARAMETERS:
    lpFName - name of file to open

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
HANDLE OpenTheFile(LPCTSTR lpFName)
{
    HANDLE hTemp;

    hTemp = CreateFile(lpFName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0,NULL);

    if (hTemp == INVALID_HANDLE_VALUE)
        ErrorReporter("CreateFile");

    return hTemp;
}

/*-----------------------------------------------------------------------------

FUNCTION: CreateTheFile(LPCTSTR)

PURPOSE: Creates a file and returns the file handle

PARAMETERS:
    lpFName - name of file to create

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
HANDLE CreateTheFile(LPCTSTR lpFName)
{
    HANDLE hTemp;

    hTemp = CreateFile(lpFName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0,NULL);

    if (hTemp == INVALID_HANDLE_VALUE)
        ErrorReporter("CreateFile");

    return hTemp;
}

/*-----------------------------------------------------------------------------

FUNCTION: GetTransferSizes(HANDLE, DWORD *, DWORD *, DWORD *)

PURPOSE: Examines file and determines packet size, number of packets,
         and file size.

PARAMETERS:
    hFile - handle of file to get size information from
    pdwDataPacketSize - size of an individual data packet
    pdwNumPackets     - total number of packets
    pdwFileSize       - size of file

RETURN:
    TRUE  - all metrics could be determined
    FALSE - something wrong with the file metrics, can't transfer

COMMENTS:
    This module can't handle files that are extremely large, so
    it may return FALSE if a large file is specified.

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
BOOL GetTransferSizes(HANDLE hFile, DWORD * pdwDataPacketSize, DWORD * pdwNumPackets, DWORD * pdwFileSize)
{
    BY_HANDLE_FILE_INFORMATION fi;

    if (!GetFileInformationByHandle(hFile, &fi)) {
        ErrorReporter("GetFileInformationByHandle");
        return FALSE;
    }
    else {
        if (fi.nFileSizeHigh) {
            MessageBox(ghwndMain, "File is too large to transfer.", "File Transfer Error", MB_OK);
            return FALSE;
        }

        //
        // setup packet size, file size and compute the number of packets
        //
        *pdwDataPacketSize = MAX_WRITE_BUFFER;
        *pdwFileSize = fi.nFileSizeLow;
        *pdwNumPackets = *pdwFileSize / *pdwDataPacketSize;

        if (*pdwNumPackets > 65534) {
            MessageBox(ghwndMain, "File is too large for buffer size.", "File Transfer Error", MB_OK);
            return FALSE;
        }
    }
    return TRUE;
}

/*-----------------------------------------------------------------------------

FUNCTION: GetLynxTransferSizes(HANDLE, HANDLE, DWORD *, DWORD *, DWORD *)

PURPOSE: Examines file and determines packet size, number of packets,
         and file size.

PARAMETERS:
    hFile - handle of file to get size information from
    pdwDataPacketSize - size of an individual data packet
    pdwNumPackets     - total number of packets
    pdwFileSize       - size of file

RETURN:
    TRUE  - all metrics could be determined
    FALSE - something wrong with the file metrics, can't transfer

COMMENTS:
    This module can't handle files that are extremely large, so
    it may return FALSE if a large file is specified.

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
BOOL GetLynxTransferSizes(HANDLE hFile, DWORD * pdwDataPacketSize, DWORD * pdwNumPackets, DWORD * pdwFileSize)
{
    BY_HANDLE_FILE_INFORMATION fi;
    char lpDataBuf[64];
	DWORD dwRead;

    if (!GetFileInformationByHandle(hFile, &fi)) {
        ErrorReporter("GetFileInformationByHandle");
        return FALSE;
    }
    else {
        if (fi.nFileSizeHigh) {
            MessageBox(ghwndMain, "File is too large to transfer.", "File Transfer Error", MB_OK);
            return FALSE;
        }

		//
		// see if block size is given in the file - default to 1024 bytes
        //
        SetFilePointer(hFile, 0, 0, FILE_BEGIN);
        if (ReadFile(hFile, lpDataBuf, 64, &dwRead, NULL)) {
            if (strcmp(lpDataBuf,"LYNX")) {
				if (lpDataBuf[0]== -128 && lpDataBuf[1]==8 &&
					lpDataBuf[6]==0x42 && lpDataBuf[7]==0x53) {
					// Lynx object file
                    SetFilePointer(hFile, 0, 0, FILE_BEGIN);
    				dwRead = 4;
				    uLynxBlockSize = 1;
				} else {
					// Raw ROM image
                    SetFilePointer(hFile, 0, 0, FILE_BEGIN);
				    uLynxBlockSize = 1024;
					dwRead = 0;
				}
			} else {
				// Lynx Handy ROM format
				dwRead = 64;
                uLynxBlockSize = ((short *)(lpDataBuf))[2];
			}
		}
		if ((uLynxBlockSize != 1) && (uLynxBlockSize != 512) && (uLynxBlockSize != 1024) && (uLynxBlockSize != 2048)) {
            MessageBox(ghwndMain, "Unsupported blocksize.", "File Transfer Error", MB_OK);
            return FALSE;
        }
		if (uLynxBlockSize == 512) {
			dwLynxTransfers = 16;
			fi.nFileSizeLow = 256 * 1024 + dwRead;
		} else {
			if (hFile2Handle) {
				CloseHandle(hFile2Handle);
				hFile2Handle = 0;
			}
			dwLynxTransfers = (fi.nFileSizeLow - dwRead + 16383) / 16384;
		}

        //
        // setup packet size, file size and compute the number of packets
        //
        *pdwDataPacketSize = MAX_WRITE_BUFFER;
		if (uLynxBlockSize == 1)
			*pdwFileSize = fi.nFileSizeLow-dwRead;
		else
			*pdwFileSize = fi.nFileSizeLow-dwRead+dwLynxTransfers*(uploadheader_len+head_len-10+tail_len);
		if (uLynxBlockSize == 1) {
	        *pdwNumPackets = (*pdwFileSize / *pdwDataPacketSize) + 1;
			dwLynxTransfers = *pdwNumPackets;
		} else
	        *pdwNumPackets = (*pdwFileSize / *pdwDataPacketSize) + dwLynxTransfers;

        if (*pdwNumPackets > 65534) {
            MessageBox(ghwndMain, "File is too large for buffer size.", "File Transfer Error", MB_OK);
            return FALSE;
        }
    }
    return TRUE;
}

/*-----------------------------------------------------------------------------

FUNCTION: ShowTransferStatistics(DWORD, DWORD, DWORD)

PURPOSE: Displays bytes transferred and bytes per second

PARAMETERS:
    dwEnd              - ending time in milliseconds
    dwStart            - starting time
    dwBytesTransferred - bytes sent

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void ShowTransferStatistics(DWORD dwEnd, DWORD dwStart, DWORD dwBytesTransferred)
{
    char szTemp[100];
    DWORD dwSecs;

    dwSecs = (dwEnd - dwStart) / 1000;

    //
    // display only if dwSecs != 0; if dwSecs == 0, then divide by zero occurs.
    //
    if (dwSecs != 0) {
        wsprintf(szTemp, "Bytes transferred: %d\r\nBytes/Second: %d\r\n", dwBytesTransferred, dwBytesTransferred / dwSecs);
        UpdateStatus(szTemp);
    }

    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: CheckForMessages

PURPOSE: Check for a message and dispatch it.

RETURN:
    If the WM_CLOSE message or the WM_SYSCOMMAND (SC_CLOSE) is 
    retrieved, WM_CLOSE is posted, and WM_CLOSEQUIT is 
    returned by the function.  This allows the caller
    to detect this as an abort condition and exit properly.  When
    the caller exits, the main message loop in the WinMain function
    should be entered again and the WM_CLOSE message will be 
    handled properly.
           
    If there is a message other than those above, it is dispatched 
    and TRUE is returned indicating that a message was dispatched.
            
    If no message is found, FALSE is returned.

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
UINT CheckForMessages()
{
    MSG msg;

    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
        if (msg.message == WM_CLOSE) {
            PostMessage(ghwndMain, WM_CLOSE, 0, 0);
            return WM_CLOSE;
        }

        if (msg.message == WM_SYSCOMMAND && msg.wParam == SC_CLOSE) {            
            PostMessage(ghwndMain, WM_CLOSE, 0, 0);
            return WM_CLOSE;
        }

        if (!TranslateAccelerator( ghwndMain, ghAccel, &msg )) {
            TranslateMessage( &msg ) ;
            DispatchMessage( &msg ) ;
        }

        return TRUE ;
    }
    
    return FALSE ;
}

/*-----------------------------------------------------------------------------

FUNCTION: CaptureFile(HANDLE, HWND)

PURPOSE: Receives a file

PARAMETERS:
    hFile - handle of file to receive the data being captured (not used)
    hWndProgress - window handle of progress bar (not used)

COMMENTS: Sets the receive state and waits for capture to end

HISTORY:   Date:      Author:     Comment:
           10/27/95   AllenD      Wrote it

-----------------------------------------------------------------------------*/
void CaptureFile(HANDLE hFile, HWND hWndProgress)
{
    UINT uMsgResult;
    gdwReceiveState = RECEIVE_CAPTURED;

    while ( !gfAbortTransfer ) {

        uMsgResult = CheckForMessages();

        //
        // If WM_CLOSE is retrieved, then exit.
        // If no message is retrieved, then sleep a little.
        // If any other message is retrieved, check for another one.
        //
        switch(uMsgResult)
        {
            case WM_CLOSE:  gfAbortTransfer = TRUE; break;
            case FALSE:     Sleep(200);             break;
            case TRUE:                              break;
        }
    }

    gdwReceiveState = RECEIVE_TTY;

    return;
}

/*-----------------------------------------------------------------------------

FUNCTION: ReadLynxFile(HANDLE, LPVOID, DWORD, *DWORD)

PURPOSE:  create the actual flash program

PARAMETERS:
          whatever

COMMENTS: 

HISTORY:   Date:      Author:     Comment:
           29/07/01   KarriK      Wrote it

-----------------------------------------------------------------------------*/
UINT uPtr;
UINT uPtrEnd;

DWORD ReadLynxFile(HANDLE hFileHandle, char *lpDataBuf, DWORD dwPacketSize, DWORD *dwRead)
{
	DWORD dwRead2;
	DWORD ret;
	short blindex;

	blindex = (short)(dwTransferNr << 4);
	if (blindex >= 16*16)
		blindex -= 16*16;
	ret = 0;
	*dwRead = 0;
	do {
		if (uPtr < uploadheader_len)
			*lpDataBuf++ = ((char *)uploadheader)[uPtr++];
		else if (uPtr < uploadheader_len+head_len-10)
			*lpDataBuf++ = ((char *)head)[uPtr++ - uploadheader_len + 10];
		else if (uPtr < uploadheader_len+head_len-10+1) {
			*lpDataBuf++ = blindex & 0x00ff;
			uPtr++;
		} else if (uPtr < uploadheader_len+head_len-10+2) {
			*lpDataBuf++ = (blindex >> 8);
			uPtr++;
		} else if (uPtr < uploadheader_len+head_len-10+2+16*1024) {
			switch (uLynxBlockSize) {
			default:
			    ReadFile(hFileHandle, lpDataBuf++, 1, &dwRead2, NULL);
				break;
			case 512:
				if (((uPtr - (uploadheader_len+head_len-10+2)) % 1024) > 511)
					ReadFile(hFile2Handle, lpDataBuf++, 1, &dwRead2, NULL);
				else
					ReadFile(hFileHandle, lpDataBuf++, 1, &dwRead2, NULL);
				break;
			case 2048:
				ReadFile(hFileHandle, lpDataBuf++, 1, &dwRead2, NULL);
				if (((uPtr - (uploadheader_len+head_len-10+2)) % 1024) == 1023) {
                    SetFilePointer(hFile, 1024, 0, FILE_CURRENT);
				}
				break;
			}
			uPtr++;
		} else
			*lpDataBuf++ = ((char *)tail)[uPtr++ - uploadheader_len - (head_len - 10) - 2 - 16*1024];
		(*dwRead)++;
		dwPacketSize--;
		if (uPtr > (uploadheader_len + head_len - 10 + 2 + 16*1024 + tail_len)) {
			ret = 1;
			uPtr = 0;
		}
	} while (dwPacketSize && !ret);
	return ret;
}

DWORD ReadLynxObjFile(HANDLE hFileHandle, char *lpDataBuf, DWORD dwPacketSize, DWORD *dwRead)
{
	DWORD dwRead2;
	DWORD ret;
	unsigned short n,m;

	*dwRead = 0;
	do {
		if (uPtr == 0) {
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
			*lpDataBuf++ = (unsigned char)0x81;
			*lpDataBuf++ = 'P';
			ReadFile(hFileHandle, &m, 2, &dwRead2, NULL);
			n = (m << 8) + ((m >> 8) & 0xff);
			*lpDataBuf++ = (n >> 8) & 0xff;
			*lpDataBuf++ = n & 0xff;
			ReadFile(hFileHandle, &m, 2, &dwRead2, NULL);
			n = (m << 8) + ((m >> 8) & 0xff);
			uPtrEnd = n;
			n = (n-10) ^ 0xffff;
			*lpDataBuf++ = (n >> 8) & 0xff;
			*lpDataBuf++ = n & 0xff;
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
			ReadFile(hFileHandle, lpDataBuf, 1, &dwRead2, NULL);
        	*dwRead = 6;
			uPtr = 10;
			dwPacketSize -= 6;
		} else {
			ReadFile(hFileHandle, lpDataBuf++, 1, &dwRead2, NULL);
			(*dwRead)++;
			uPtr++;
			dwPacketSize--;
			ret = (uPtr >= uPtrEnd);
		}
	} while (dwPacketSize && !ret);
	return ret;
}

/*-----------------------------------------------------------------------------

FUNCTION: TransferThreadProc(LPVOID)

PURPOSE: Worker thread does all the file transfer work

PARAMETERS:
    lpV - actually a HANDLE for the file

COMMENTS: Function allows the hTransferAbortEvent to 
          signal an abort condition.
          If the thread finishes OK, then the thread
          calls the TransferFileTextEnd function itself.

HISTORY:   Date:      Author:     Comment:
           1/26/96   AllenD      Wrote it

-----------------------------------------------------------------------------*/
DWORD WINAPI TransferThreadProc(LPVOID lpV)
{
    DWORD  dwPacketSize, dwMaxPackets, dwFileSize;
    DWORD  dwStartTime;
    HWND   hWndProgress;
    HANDLE hFileHandle;
    HANDLE hDataHeap;
    BOOL fStarted = TRUE;
    BOOL fAborting = FALSE;

    hFileHandle = (HANDLE) lpV;
    hWndProgress = GetDlgItem(ghWndStatusDlg, IDC_TRANSFERPROGRESS);

    // set up transfer metrics
	if (uLynxBlockSize) {
        if (!GetLynxTransferSizes(hFileHandle, &dwPacketSize, &dwMaxPackets, &dwFileSize))
            fAborting = TRUE;
        else {
            SendMessage(hWndProgress, PBM_SETRANGE, 0, MAKELPARAM(0, dwMaxPackets+1));
            SendMessage(hWndProgress, PBM_SETSTEP, (WPARAM) 1, 0);
            SendMessage(hWndProgress, PBM_SETPOS, 0, 0);
		}
	} else {
        if (!GetTransferSizes(hFileHandle, &dwPacketSize, &dwMaxPackets, &dwFileSize))
            fAborting = TRUE;
        else {
            SendMessage(hWndProgress, PBM_SETRANGE, 0, MAKELPARAM(0, dwMaxPackets+1));
            SendMessage(hWndProgress, PBM_SETSTEP, (WPARAM) 1, 0);
            SendMessage(hWndProgress, PBM_SETPOS, 0, 0);
		}
	}

    // set up transfer heaps
    if (!fAborting) {
        SYSTEM_INFO sysInfo;
        GetSystemInfo(&sysInfo);
        hDataHeap = HeapCreate(0, sysInfo.dwPageSize * 2, sysInfo.dwPageSize * 10);
        if (hDataHeap == NULL) {
            ErrorReporter("HeapCreate (Data Heap)");
            fAborting = TRUE;
        }
    }

    // inform writer thread that a file is about to be transferred
    if (!fAborting) {
        if (!WriterAddNewNode(WRITE_FILESTART, dwFileSize, 0, NULL, NULL, NULL))
            fAborting = TRUE;
    }

    OutputDebugString("Xfer: About to start sending data\n");

    // Get Transfer Start Time
    dwStartTime = GetTickCount();
	dwTransferNr = 0;
	uPtr = 0;

    if (WaitForSingleObject(hTransferAbortEvent, 0) == WAIT_OBJECT_0)
        fAborting = TRUE;

    while (!fAborting) {
        char * lpDataBuf;
        PWRITEREQUEST pWrite;

        // transfer file, loop until all blocks of file have been read
        lpDataBuf = HeapAlloc(hDataHeap, 0, dwPacketSize);
        pWrite = HeapAlloc(ghWriterHeap, 0, sizeof(WRITEREQUEST));
        if ((lpDataBuf != NULL) && (pWrite != NULL)) {

            DWORD dwRead;
			char msgstr[80];

			if (uLynxBlockSize) {
				if (uLynxBlockSize == 1) {
					// read from file into new buffer
					if (ReadLynxObjFile(hFileHandle, lpDataBuf, dwPacketSize, &dwRead)) {
						WriterAddExistingNode(pWrite, WRITE_FILE, dwRead, 0, lpDataBuf, hDataHeap, hWndProgress);
						wsprintf(msgstr, "Transferring block %2d\r\n", dwTransferNr);
						OutputABufferToWindow(ghWndTTY, msgstr, 23);

						dwTransferNr++;
						if (dwTransferNr == dwLynxTransfers)
							break;
					} else {
						WriterAddExistingNode(pWrite, WRITE_FILE, dwRead, 0, lpDataBuf, hDataHeap, hWndProgress);
					}
				} else {
					// read from file into new buffer
					if (ReadLynxFile(hFileHandle, lpDataBuf, dwPacketSize, &dwRead)) {
						WriterAddExistingNode(pWrite, WRITE_FILE, dwRead, 0, lpDataBuf, hDataHeap, hWndProgress);
						wsprintf(msgstr, "Transferring block %2d\r\n", dwTransferNr);
						OutputABufferToWindow(ghWndTTY, msgstr, 23);

						dwTransferNr++;
						if (dwTransferNr == dwLynxTransfers)
							break;
						else
							Sleep(((dwTransferNr==1) || dwTransferNr==17)?50000:40000);
						if (dwTransferNr==16) {
							MessageBox(ghwndMain, "Please flip S6 OFF", "Flash burn request", MB_OK);
							SetFilePointer(hFileHandle, 64+1024, 0, FILE_BEGIN);
							uPtr = 0;
						}
					} else {
						WriterAddExistingNode(pWrite, WRITE_FILE, dwRead, 0, lpDataBuf, hDataHeap, hWndProgress);
					}
				}
			} else {
                // read from file into new buffer
                if (ReadFile(hFileHandle, lpDataBuf, dwPacketSize, &dwRead, NULL)) {
                    WriterAddExistingNode(pWrite, WRITE_FILE, dwRead, 0, lpDataBuf, hDataHeap, hWndProgress);

                    if (dwRead != dwPacketSize) // eof
                        break;
				}
			}
        }
        else {
            BOOL fRes;
            /*
                Either the data heap is full, or the writer heap is full.
                Free any allocated block, wait a little and try again.

                Waiting lets the writer thread send some blocks and free
                the data blocks from the data heap and the control
                blocks from the writer heap.
            */
            if (lpDataBuf) {
                EnterCriticalSection(&gcsDataHeap);
                fRes = HeapFree(hDataHeap, 0, lpDataBuf);
                LeaveCriticalSection(&gcsDataHeap);
                if (!fRes)
                    ErrorReporter("HeapFree (Data block)");
            }

            if (pWrite) {
                EnterCriticalSection(&gcsWriterHeap);
                fRes = HeapFree(ghWriterHeap, 0, pWrite);
                LeaveCriticalSection(&gcsWriterHeap);
                if (!fRes)
                    ErrorReporter("HeapFree (Writer block)");
            }

            OutputDebugString("Xfer: A heap is full.  Waiting...\n");

            // wait a little
            // check for abort during the wait
            if (WaitForSingleObject(hTransferAbortEvent, 200) == WAIT_OBJECT_0)
                fAborting = TRUE;
        }

        // has the user aborted?
        if (WaitForSingleObject(hTransferAbortEvent, 0) == WAIT_OBJECT_0)
            fAborting = TRUE;
    }

    OutputDebugString("Xfer: Done sending packets.\n");

    if (fAborting) {
        // inform writer that transfer is aborting

        OutputDebugString("Xfer: Sending Abort Packet to writer\n");
        WriterAddFirstNodeTimeout(WRITE_ABORT, dwFileSize, 0, NULL, NULL, NULL, 500);
    }
    else
        WriterAddNewNodeTimeout(WRITE_FILEEND, dwFileSize, 0, NULL, NULL, NULL, 500);
        
    {
        // wait til writer thread finishes with all blocks
        HANDLE hEvents[2];
        DWORD dwRes;
        BOOL  fTransferComplete;
        
        hEvents[0] = ghTransferCompleteEvent;
        hEvents[1] = hTransferAbortEvent;

        OutputDebugString("Xfer: Waiting for transfer complete signal from writer\n");
        do {
            ResetEvent(hTransferAbortEvent);

            dwRes = WaitForMultipleObjects(2, hEvents, FALSE, INFINITE);
            switch(dwRes) {
            case WAIT_OBJECT_0:      
                fTransferComplete = TRUE;   
                OutputDebugString("Transfer complete signal rec'd\n");
                break;
            case WAIT_OBJECT_0 + 1:  
                fAborting = TRUE;           
                OutputDebugString("Transfer abort signal rec'd\n");
                OutputDebugString("Xfer: Sending Abort Packet to writer\n");
                if (!WriterAddFirstNodeTimeout(WRITE_ABORT, dwFileSize, 0, NULL, NULL, NULL, 500))
                    ErrorReporter("Can't add abort packet\n");
                break;
            case WAIT_TIMEOUT:                                   break;
            default:
                ErrorReporter("WaitForMultipleObjects(Transfer Complete Event and Transfer Abort Event)");
                fTransferComplete = TRUE;
                break;
            }
        } while (!fTransferComplete);
    }

    OutputDebugString("Xfer: transfer complete\n");

    // report statistics
    if (!fAborting)
        ShowTransferStatistics(GetTickCount(), dwStartTime, dwFileSize);

    // break down metrics
    PostMessage(hWndProgress, PBM_SETPOS, 0, 0);

    // break down heaps
    if (hDataHeap != NULL) {
        if (!HeapDestroy(hDataHeap))
            ErrorReporter("HeapDestroy (data heap)");
    }

    // If I am done without user intervention, then post the
    // "abort" message myself.  This will cause the main thread to
    // clean up after the file transfer.
    if (!fAborting)
        PostMessage(ghwndMain, WM_COMMAND, ID_TRANSFER_ABORTSENDING, 0);

    // exit thread

    return 0;
}

