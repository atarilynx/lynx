unsigned short uploadheader[] = {
  0x5081,
  0x0010,
  ((((head_len-10+2+16*1024+tail_len) ^ 0xffff)<<8) & 0xff00) |
  ((((head_len-10+2+16*1024+tail_len) ^ 0xffff)>>8) & 0x00ff)
};

#define uploadheader_len 6
