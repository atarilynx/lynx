MINIMIZE        set 1
IF MINIMIZE
DOUBLEBUFFER    set 0          ; 1 = double-buffering
ELSE
DOUBLEBUFFER    set 1          ; 1 = double-buffering
ENDIF
Columns		set 16
Rows		set 10
StartPeg	set ((Columns*Rows)/2-Columns/2-1)
ComputerCol     set $1e
PlayerCol       set $ac
CursorCol       set $00
CursorBkg       set $06
PegBkg          set $3f

Baudrate        set 9600


                include <macros/help.mac>
                include <macros/if_while.mac>
                include <macros/mikey.mac>
                include <macros/suzy.mac>
;
; essential variables
;


                include <vardefs/mikey.var>
                include <vardefs/suzy.var>

;
; zero-page
;
 BEGIN_ZP
turn            ds 1
cnt             ds 1
length          ds 1
step            ds 1
sum             ds 1
flipit          ds 1
drawit          ds 1
i               ds 1
j               ds 1
n               ds 1
pos		ds 1
posx		ds 1
posy		ds 1
level		ds 1
board           ds Columns*Rows
 END_ZP
;
; main-memory variables
;
 BEGIN_MEM
                align 4
screen0         ds SCREEN.LEN
screen1         ds SCREEN.LEN

 END_MEM
;
; code
;

                run LOMEM

Start::         
		START_UP
                CLEAR_MEM
                CLEAR_ZP +STACK

                INITMIKEY
                INITSUZY


IF DOUBLEBUFFER
                SCRBASE screen0,screen1
ELSE
                SCRBASE screen0
ENDIF

IF DOUBLEBUFFER
                SWITCHBUF
                jsr cls
ENDIF

                SETRGB pal      ; set color

;
; stereo sound
;
		lda #$42
		sta $fd50
		sta $fd24
		lda #$18
		sta $fd25
;
; Initialize board area
; set tactics $02 is the most desired place $80 the least
;
                ldx #Rows*Columns
                lda #$20
		_WHILENE
                  dex
                  sta board,x
                _WEND
		lda #$80
		sta n
		ldx #Columns+1
		lda #Columns-2
		sta i
		lda #Rows-2
		jsr setbox
		lda #$40
		sta n
		ldx #2*Columns+2
		lda #Columns-4
		sta i
		lda #Rows-4
		jsr setbox
;
; set up four starting pegs
;
		stz board+StartPeg
		stz board+StartPeg+Columns+1
		lda #1
		sta board+StartPeg+1
		sta board+StartPeg+Columns
;
; Set cursor at left top
;
                stz pos
                stz posx
                stz posy
;
; player goes first
;
                stz turn
;
; activate advisor
;
NoAdvisor       set 0
Advisor         set $10
IF MINIMIZE
		stz level
ELSE
                lda #NoAdvisor
		sta level
ENDIF
		stz flipit
;
; main-loop
;
.loop
;
; Start draw from i,j
;
                lda #6
                sta i
                sta j
;
; Set board index to 0
;
		stz n
;
; Set board to green
;
                jsr cls
.rows
		lda j
		sta sprite_y
.cols
		lda i
		sta sprite_x
		lda n
		cmp pos
		_IFEQ
                  lda #CursorBkg
                _ELSE
                  lda #PegBkg
                _ENDIF
		sta sprite_bkg
		sta drawit
		ldx n
		lda board,x
		_SWITCH
		_CASE
		  lda #PlayerCol
		_CASE #1
		  lda #ComputerCol
		_ELSES
;
; Cursor color chooser
;
		lda n
		cmp pos
		_IFNE
		  stz drawit
		_ELSE
	          adc sum
                  and #$0f
	          sta $fd21
                  jsr checkpos
                  _IFEQ
		    lda #CursorCol
                  _ELSE
                    lda level
		    sta $fd20
                    bit board,x
                    _IFNE
                      jsr flippos
		      stz level
		      stz $fd20
		      bra .loop
                    _ENDIF
                    lda turn
                    _IFEQ
                      lda #PlayerCol
                    _ELSE
                      lda #ComputerCol
                    _ENDIF
                  _ENDIF
                _ENDIF
		_ENDS
		sta sprite_col
;
; Draw peg
;
                lda drawit
		_IFNE
		  LDAY spriteSCB
                  jsr DrawSprite
		_ENDIF
;
; Increment board index
;
		inc n
;
; Increment i
;
                clc
		lda i
		adc #10
		sta i
		cmp #6+Columns*10
		blt .cols
		lda #6
		sta i
;
; Increment j
;
		lda j
                clc
		adc #10
		sta j
		cmp #6+Rows*10
		_IFLT
		  jmp .rows
		_ENDIF
IF DOUBLEBUFFER
                SWITCHBUF
ENDIF
		lda turn
                _IFEQ
;
; read user input
;
                  WAITKEY         ; see MIKEY.MAC

                  bit #$c0        ; up | down
                  _IFNE
                    bit #$80
                    _IFNE
                      ldx pos
                      cpx #Columns
                      _IFGE
                        txa
                        adc #-Columns-1
                        tax
                        dec posy
                      _ENDIF
                      stx pos
                    _ELSE
                      ldx pos
                      cpx #(Rows-1)*Columns
                      _IFLT
                        txa
                        adc #Columns
                        tax
                        inc posy
                      _ENDIF
                      stx pos
                    _ENDIF
                  _ELSE
                    bit #$30
                    _IFNE
                      bit #$20
                      _IFNE
                        ldx pos
                        _IFNE
                          dex
                          dec posx
                        _ENDIF
                        stx pos
                      _ELSE
                        ldx pos
                        cpx #Rows*Columns-1
                        _IFNE
                          inx
                          inc posx
                        _ENDIF
                        stx pos
                      _ENDIF
                    _ELSE
IF MINIMIZE
                      jsr flippos
                      lda #Advisor
                      sta level
ELSE
                      bit #2
                      _IFNE
                        ldx pos
                        lda board,x
                        inc
                        and #3
                        sta board,x
                      _ELSE
                        jsr flippos
                        lda #Advisor
                        sta level
                      _ENDIF
ENDIF
                    _ENDIF
                  _ENDIF
                _ENDIF
                lda level
                _IFNE
                  cmp #Advisor
                  _IFEQ
                    asl
                    sta level
                    stz posy
                    lda #-1
                    sta posx
                    sta pos
                  _ENDIF
                  inc pos
                  lda posx
                  inc
                  cmp #Columns
                  _IFEQ
                    stz posx
                    lda posy
                    inc
                    cmp #Rows
                    _IFEQ
                      stz pos
                      stz posy
                      lda level
                      asl
                      sta level
		      _IFEQ
                        lda turn
                        eor #1
                        sta turn
		      _ENDIF
                    _ELSE
                      sta posy
                    _ENDIF
                  _ELSE
                    sta posx
                  _ENDIF
                _ENDIF

                jmp .loop


spriteSCB       dc.b $c0,$10,$00
                dc.w 0          ; no linking
                dc.w sprite_data
sprite_x        dc.w 0
sprite_y        dc.w 0
                dc.w $100
                dc.w $100
sprite_bkg	dc.b $3F
sprite_col      dc.b $AC    ; we use only 3 colors !!

checkrow::
		sty step
		sta length
                stz cnt
;
; look at current pos. Should be empty
;
                ldx pos
                lda board,x
                bit #$fe
                _IFEQ
                  rts
                _ENDIF

		lda length
		_WHILENE
		  txa
		  clc
		  adc step
		  tax
		  lda board,x
		  bit #$fe
		  _IFNE
		    rts
		  _ENDIF
		  cmp turn
		  _IFEQ
		    bra .check1
		  _ELSE
		    inc cnt
		  _ENDIF
		  dec length
		_WEND
		rts
.check1
;
; add cnt to total sum for pegs that will flip
;
                lda cnt
		_IFEQ
		  rts
		_ENDIF
                clc
                adc sum
                sta sum
                lda flipit
		_IFEQ
		  rts
		_ENDIF
;
; flip the pegs for this row
;
                ldx pos
.check4
                txa
                clc
                adc step
                tax
                lda board,x
                cmp turn
                beq .check5
                lda turn
                sta board,x
                bra .check4
.check5
                rts
;
; count how many pegs would turn
;
flippos::
		lda #1
		sta flipit
checkpos::
;
; clear peg counter
;
                stz sum
;
; start by checking for left neighbor
;
                ldy #-1
                lda posx
                jsr checkrow
;
; check for right neighbor
;
                ldy #1
                lda #Columns
                clc
                sbc posx
                jsr checkrow
;
; check for top neighbor
;
                ldy #-Columns
                lda posy
                jsr checkrow
;
; check for bottom neighbor
;
                ldy #Columns
                lda #Rows
                clc
                sbc posy
                jsr checkrow
;
; check for top left neighbor
;
                ldy #-Columns-1
                lda posx
		cmp posy
		_IFGE
		  lda posy
		_ENDIF
                jsr checkrow
;
; check for top right neighbor
;
                ldy #-Columns+1
                lda #Columns
                clc
                sbc posx
		cmp posy
		_IFGE
		  lda posy
		_ENDIF
                jsr checkrow
;
; check for bottom left neighbor
;
                ldy #Columns-1
                lda #Rows
                clc
                sbc posy
		cmp posx
		_IFGE
		  lda posx
		_ENDIF
                jsr checkrow
;
; check for bottom right neighbor
;
                ldy #Columns+1
                lda #Rows
                clc
                sbc posy
		sta length
                lda #Columns
                clc
                sbc posx
		cmp length
		_IFGE
		  lda length
		_ENDIF
                jsr checkrow
;
; Flip last peg if needed
;
                lda flipit
                _IFNE
                  lda sum
                  _IFNE
                    lda pos
                    tax
                    lda turn
                    sta board,x
                    inc sum
                  _ENDIF
                _ENDIF
		lda flipit
		_IFNE
		  lda turn
		  eor #1
		  sta turn
		_ENDIF
                stz flipit
		ldx pos
                lda sum
                rts
;
; set box
;
setbox::
		sta posy
		_WHILENE
                  lda n
		  ldy i
		  _WHILENE
                    sta board,x
		    inx
		    dey
                  _WEND
		  txa
		  clc
		  adc #Columns
		  sec
		  sbc i
		  tax
		  dec posy
		_WEND
		rts
;
; clear screen
;
cls::
                LDAY clsSCB
                jmp DrawSprite

clsSCB          dc.b $c0,$90,$00
                dc.w 0,cls_data
                dc.w 0,0
                dc.w 160*$100,102*$100
cls_color       dc.b $03

cls_data        dc.b 2,$10,0



                include <includes/draw_spr.inc>

pal             STANDARD_PAL

;
; 4-quadrant-sprite => action-point is in the middle
;

sprite_data     ibytes "sprite.spr"

