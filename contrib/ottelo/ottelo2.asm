MINIMIZE        set 0
IF MINIMIZE
DOUBLEBUFFER    set 0          ; 1 = double-buffering
ELSE
DOUBLEBUFFER    set 1          ; 1 = double-buffering
ENDIF
Columns		set 10
Rows		set 10
StartPeg	set ((Columns*Rows)/2-Columns/2-1)
ComputerCol     set $1e
PlayerCol       set $ac
CursorCol       set $00
CursorBkg       set $06
PegBkg          set $3f

SWITCHES	equ $fcb1

                include <macros/help.mac>
                include <macros/if_while.mac>
                include <macros/mikey.mac>
                include <macros/suzy.mac>
                include <macros/irq.mac>
;
; essential variables
;


                include <vardefs/mikey.var>
                include <vardefs/suzy.var>

;
; zero-page
;
 BEGIN_ZP
abc_score_ptr0	ds 2
abc_score_ptr1	ds 2
abc_score_ptr2	ds 2
abc_score_ptr3	ds 2
irqs ds 16
FlipFlag ds 1
turn            ds 1
cnt             ds 1
length          ds 1
step            ds 1
sum             ds 1
flipit          ds 1
drawit          ds 1
i               ds 1
j               ds 1
n               ds 1
pos		ds 1
posx		ds 1
posy		ds 1
level		ds 1
Metronome	ds 1
tmp	ds 1
board           ds Columns*Rows
 END_ZP
;
; main-memory variables
;
 BEGIN_MEM
                align 4
screen0         ds SCREEN.LEN
screen1         ds SCREEN.LEN

 END_MEM
;
; code
;

                run LOMEM

Start::         
		START_UP
                CLEAR_MEM
                CLEAR_ZP +STACK
		ldx #$ff
		txs
		cld
                INITSUZY
                INITMIKEY
		INITIRQ irqs
		FRAMERATE 60
		SETIRQ 2,VBL
		cli
		lda #$42
		sta $fd50
		sta $fd24
		lda #$18
		sta $fd25
		jsr init_music
.loop0		lda $fd0a
		bne .loop0

IF DOUBLEBUFFER
                SCRBASE screen0,screen1
ELSE
                SCRBASE screen0
ENDIF

IF 0
                SWITCHBUF
                jsr cls
ENDIF

                SETRGB pal      ; set color

;
; Initialize board area
; set tactics $02 is the most desired place $80 the least
;
                ldx #Rows*Columns
                lda #$20
		_WHILENE
                  dex
                  sta board,x
                _WEND
		lda #$80
		sta n
		ldx #Columns+1
		lda #Columns-2
		sta i
		lda #Rows-2
		jsr setbox
		lda #$40
		sta n
		ldx #2*Columns+2
		lda #Columns-4
		sta i
		lda #Rows-4
		jsr setbox
;
; set up four starting pegs
;
		stz board+StartPeg
		stz board+StartPeg+Columns+1
		lda #1
		sta board+StartPeg+1
		sta board+StartPeg+Columns
;
; Set cursor at left top
;
                stz pos
                stz posx
                stz posy
;
; player goes first
;
                stz turn
;
; activate advisor
;
NoAdvisor       set 0
Advisor         set $10
IF 1
		stz level
ELSE
                lda #Advisor
		sta level
ENDIF
		stz flipit
;
; main-loop
;
.loop
		_IFNE FlipFlag
		  bra .loop
		_ENDIF
                lda sound_channel_busy
                _IFEQ
  lda #<abcmusic0
  ldy #>abcmusic0
  ldx #0
		jsr _abc_set_score
  lda #<abcmusic1
  ldy #>abcmusic1
  ldx #1
		jsr _abc_set_score
  lda #<abcmusic2
  ldy #>abcmusic2
  ldx #2
		jsr _abc_set_score
                _ENDIF
;
; Start draw from i,j
;
                lda #6
                sta i
                sta j
;
; Set board index to 0
;
		stz n
;
; Set board to green
;
                jsr cls
.rows
		lda j
		sta sprite_y
.cols
		lda i
		sta sprite_x
		lda n
		cmp pos
		_IFEQ
                  lda #CursorBkg
                _ELSE
                  lda #PegBkg
                _ENDIF
		sta sprite_bkg
		sta drawit
		ldx n
		lda board,x
		_SWITCH
		_CASE
		  lda #PlayerCol
		_CASE #1
		  lda #ComputerCol
		_ELSES
;
; Cursor color chooser
;
		lda n
		cmp pos
		_IFNE
		  stz drawit
		_ELSE
		  adc sum
		  and #$0f
		  ; sta $fd21
                  jsr checkpos
                  _IFEQ
		    lda #CursorCol
                  _ELSE
                    lda level
                    bit board,x
                    _IFNE
                      jsr flippos
		      stz level
		      jmp .loop
                    _ENDIF
                    lda turn
                    _IFEQ
                      lda #PlayerCol
                    _ELSE
                      lda #ComputerCol
                    _ENDIF
                  _ENDIF
                _ENDIF
		_ENDS
		sta sprite_col
;
; Draw peg
;
                lda drawit
		_IFNE
		  LDAY spriteSCB
                  jsr DrawSprite
		_ENDIF
;
; Increment board index
;
		inc n
;
; Increment i
;
                clc
		lda i
		adc #10
		sta i
		cmp #6+Columns*10
		_IFLT
		  jmp .cols
		_ENDIF
		lda #6
		sta i
;
; Increment j
;
		lda j
                clc
		adc #10
		sta j
		cmp #6+Rows*10
		_IFLT
		  jmp .rows
		_ENDIF
IF DOUBLEBUFFER
                lda #1
		sta FlipFlag
ENDIF
          _IFNE Metronome
            jsr update_music
            stz Metronome
          _ENDIF
		lda turn
                _IFEQ
;
; read user input
;
read_again: jsr readsub

                  bit #$08        ; Opt 1
                  _IFNE
                    lda SWITCHES
                    ror
                    _IFCS
		      sei
		      lda #2
		      sta $fff9
		      brk #0
                    _ENDIF
                    jmp read_again
		  _ENDIF

                  bit #$c0        ; up | down
                  _IFNE
                    bit #$80
                    _IFNE
                      ldx pos
                      cpx #Columns
                      _IFGE
                        txa
                        adc #-Columns-1
                        tax
                        dec posy
                      _ENDIF
                      stx pos
                    _ELSE
                      ldx pos
                      cpx #(Rows-1)*Columns
                      _IFLT
                        txa
                        adc #Columns
                        tax
                        inc posy
                      _ENDIF
                      stx pos
                    _ENDIF
                  _ELSE
                    bit #$30
                    _IFNE
                      bit #$20
                      _IFNE
                        ldx pos
                        _IFNE
                          dex
                          dec posx
                        _ENDIF
                        stx pos
                      _ELSE
                        ldx pos
                        cpx #Rows*Columns-1
                        _IFNE
                          inx
                          inc posx
                        _ENDIF
                        stx pos
                      _ENDIF
                    _ELSE
IF 1
                      jsr flippos
                      lda #Advisor
                      sta level
  lda #<abcmusic3
  ldy #>abcmusic3
  ldx #3
		jsr _abc_set_score
ELSE
                      bit #2
                      _IFNE
                        ldx pos
                        lda board,x
                        inc
                        and #3
                        sta board,x
                      _ELSE
                        jsr flippos
                        lda #Advisor
                        sta level
                      _ENDIF
ENDIF
                    _ENDIF
                  _ENDIF
                _ENDIF
                lda level
                _IFNE
                  cmp #Advisor
                  _IFEQ
                    asl
                    sta level
                    stz posy
                    lda #-1
                    sta posx
                    sta pos
                  _ENDIF
                  inc pos
                  lda posx
                  inc
                  cmp #Columns
                  _IFEQ
                    stz posx
                    lda posy
                    inc
                    cmp #Rows
                    _IFEQ
                      stz pos
                      stz posy
                      lda level
                      asl
                      sta level
		      _IFEQ
                        lda turn
                        eor #1
                        sta turn
		      _ENDIF
                    _ELSE
                      sta posy
                    _ENDIF
                  _ELSE
                    sta posx
                  _ENDIF
                _ENDIF

                jmp .loop


spriteSCB       dc.b $c0,$10,$00
                dc.w 0          ; no linking
                dc.w sprite_data
sprite_x        dc.w 0
sprite_y        dc.w 0
                dc.w $100
                dc.w $100
sprite_bkg	dc.b $3F
sprite_col      dc.b $AC    ; we use only 3 colors !!

checkrow::
		sty step
		sta length
                stz cnt
;
; look at current pos. Should be empty
;
                ldx pos
                lda board,x
                bit #$fe
                _IFEQ
                  rts
                _ENDIF

		lda length
		_WHILENE
		  txa
		  clc
		  adc step
		  tax
		  lda board,x
		  bit #$fe
		  _IFNE
		    rts
		  _ENDIF
		  cmp turn
		  _IFEQ
		    bra .check1
		  _ELSE
		    inc cnt
		  _ENDIF
		  dec length
		_WEND
		rts
.check1
;
; add cnt to total sum for pegs that will flip
;
                lda cnt
		_IFEQ
		  rts
		_ENDIF
                clc
                adc sum
                sta sum
                lda flipit
		_IFEQ
		  rts
		_ENDIF
;
; flip the pegs for this row
;
                ldx pos
.check4
                txa
                clc
                adc step
                tax
                lda board,x
                cmp turn
                beq .check5
                lda turn
                sta board,x
                bra .check4
.check5
                rts
;
; count how many pegs would turn
;
flippos::
		lda #1
		sta flipit
checkpos::
;
; clear peg counter
;
                stz sum
;
; start by checking for left neighbor
;
                ldy #-1
                lda posx
                jsr checkrow
;
; check for right neighbor
;
                ldy #1
                lda #Columns-1
                sec
                ;clc
                sbc posx
                jsr checkrow
;
; check for top neighbor
;
                ldy #-Columns
                lda posy
                jsr checkrow
;
; check for bottom neighbor
;
                ldy #Columns
                lda #Rows-1
                sec
                ;clc
                sbc posy
                jsr checkrow
;
; check for top left neighbor
;
                ldy #-Columns-1
                lda posx
		cmp posy
		_IFGE
		  lda posy
		_ENDIF
                jsr checkrow
;
; check for top right neighbor
;
                ldy #-Columns+1
                lda #Columns-1
                sec
                ;clc
                sbc posx
		cmp posy
		_IFGE
		  lda posy
		_ENDIF
                jsr checkrow
;
; check for bottom left neighbor
;
                ldy #Columns-1
                lda #Rows-1
                sec
                ;clc
                sbc posy
		cmp posx
		_IFGE
		  lda posx
		_ENDIF
                jsr checkrow
;
; check for bottom right neighbor
;
                ldy #Columns+1
                lda #Rows-1
                sec
                ;clc
                sbc posy
		sta length
                lda #Columns-1
                sec
                ;clc
                sbc posx
		cmp length
		_IFGE
		  lda length
		_ENDIF
                jsr checkrow
;
; Flip last peg if needed
;
                lda flipit
                _IFNE
                  lda sum
                  _IFNE
                    lda pos
                    tax
                    lda turn
                    sta board,x
                    inc sum
                  _ENDIF
                _ENDIF
		lda flipit
		_IFNE
		  lda turn
		  eor #1
		  sta turn
		_ENDIF
                stz flipit
		ldx pos
                lda sum
                rts
;
; set box
;
setbox::
		sta posy
		_WHILENE
                  lda n
		  ldy i
		  _WHILENE
                    sta board,x
		    inx
		    dey
                  _WEND
		  txa
		  clc
		  adc #Columns
		  sec
		  sbc i
		  tax
		  dec posy
		_WEND
		rts

readsub:
wk0:      _IFNE Metronome
            jsr update_music
            stz Metronome
          _ENDIF
            lda $fcb0
                beq wk0
          sta tmp
wk1:     _IFNE Metronome
            jsr update_music
            stz Metronome
          _ENDIF
          lda tmp
          cmp $fcb0
          beq wk1
          rts
; ABC music definition file

abcmusic0   db $49,8,80,$3a,36,4,2
            db "T",18
            db "e","d","e","5","A","f","e","f","5","e","d","c"
            db "B","3","A","G","2","e","d","e","5","e","d","c"
            db "c","3","A","c","2","e","2","d","5","c","B","A"
            db "A","4","A","c","B","A","B","4","z","2","c","B"
            db "c","3","B","c","d","e","f","g","5","g","f","e"
            db "f","3","d","c","B","c","d","e","6","e","d"
            db "e","5","A","f","e","f","5","e","d","c"
            db "B","3","A","G","2","e","d","e","5","e","d","c"
            db "c","3","A","c","2","e","2","d","5","f","e","d"
            db "c","2","d","e","E","2","E","2","A","6","z","P",0

abcmusic1   db $49,8,80,$3a,36,4,1
            db "T",18
            db "z","2","A","8","D","8","G","8","C","8"
            db "F","8","D","8","D","8","E","8"
            db "F","8","F","8","D","8","E","8"
            db "A","8","D","8","G","8","C","8"
            db "F","8","D","8","A","4","E","4","A","6","z","P",0

abcmusic2   db $49,8,80,$3a,16,64,1
            db "T",18
            db "z","2","A","c","e","c","A","c","e","c"
            db "D","F","A","F","D","F","A","F"
            db "G","B","d","B","G","B","d","B"
            db "C","E","G","E","C","E","G","E"
            db "F","A","c","A","F","A","c","A"
            db "D","F","A","F","D","F","A","F"
            db "D","F","A","F","D","F","A","F"
            db "E","G","B","G","E","z","3"
            db "F","A","c","A","F","A","c","A"
            db "E","G","B","G","E","G","B","G"
            db "D","F","A","F","D","F","A","F"
            db "E","=","G","B","=","G","E","=","G","B","=","G"
            db "A","c","E","c","A","c","e","c"
            db "D","F","A","F","D","F","A","F"
            db "G","B","d","B","G","B","d","B"
            db "C","E","G","E","C","E","G","E"
            db "F","A","c","A","F","A","c","A"
            db "D","F","A","F","D","F","A","F"
            db "A","c","e","c","E","=","G","B","=","G"
            db "A","c","e","c","A","c","e","c","z","P",0

abcmusic3   db $49,8,80,$3a,36,4,1
            db "T",9
            db "b","a","g","f","e","d","c","B","A","G","z","P",0

abcsilence  db 0,0

; Set all scores to silent at startup
init_music::
  stz sound_channel_busy
  stz sound_channel_busy+1
  stz sound_channel_busy+2
  stz sound_channel_busy+3
  lda #<abcsilence
  ldy #>abcsilence
  ldx #0
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #1
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #2
  jsr _abc_set_score
  lda #<abcsilence
  ldy #>abcsilence
  ldx #3
  jsr _abc_set_score
  stz sound_channel_busy
  stz sound_channel_busy+1
  stz sound_channel_busy+2
  stz sound_channel_busy+3
  rts

; Activate score on channel X
; A - low address
; Y - high address
_abc_set_score::
  inx
  dex
  _IFEQ
    ldx sound_channel_busy
    _IFEQ
      sta abc_score_ptr0
      sty abc_score_ptr0+1
      stz abc_music_ptr
      stz sound_channel_duration
      lda #1
      sta sound_channel_busy
    _ENDIF
    rts
  _ENDIF
  dex
  _IFEQ
    ldx sound_channel_busy+1
    _IFEQ
      sta abc_score_ptr1
      sty abc_score_ptr1+1
      stz abc_music_ptr+1
      stz sound_channel_duration+1
      lda #1
      sta sound_channel_busy+1
    _ENDIF
    rts
  _ENDIF
  dex
  _IFEQ
    ldx sound_channel_busy+2
    _IFEQ
      sta abc_score_ptr2
      sty abc_score_ptr2+1
      stz abc_music_ptr+2
      stz sound_channel_duration+2
      lda #1
      sta sound_channel_busy+2
    _ENDIF
    rts
  _ENDIF
  ldx sound_channel_busy+3
  _IFEQ
    sta abc_score_ptr3
    sty abc_score_ptr3+1
    stz abc_music_ptr+3
    stz sound_channel_duration+3
    lda #1
    sta sound_channel_busy+3
  _ENDIF
  rts

; Once at each frame we can update the music
; You should call this routine frequently.
; Once in a frame is a good idea.
update_music::
  ldx #0
update_channel_x:
  lda sound_channel_duration,x
  _IFEQ
    ; note has ended, fetch next
    lda abc_music_ptr,x
    tay
    bra parse_abc
  _ENDIF
  ; note is playing
  cmp #255 ; Duration 255 is forever, good for engines
  _IFNE
    dec
    sta sound_channel_duration,x
  _ENDIF
update_channel_tail:
  lda sound_channel_maxlen,x
  _IFNE
    dec
    sta sound_channel_maxlen,x
    _IFEQ
      sta sound_channel_max_volume,x
    _ENDIF
  _ENDIF
  lda sound_channel_max_volume,x
  _IFEQ
    ; silence
    lda sound_channel_volume,x
    _IFNE
      ; decay time still going on
      sec
      sbc abc_instrument_decr,x
      _IFCC
        ; silence
        lda #0
      _ENDIF
      sta sound_channel_volume,x
    _ENDIF
  _ENDIF
  lda sound_channel_volume,x
  cmp sound_channel_max_volume,x
  _IFLO
    ; attack time
    clc
    adc abc_instrument_incr,x
    _IFCS
      ; desired volume reached
      lda sound_channel_max_volume,x
    _ENDIF
    cmp sound_channel_max_volume,x
    _IFHI
      ; desired volume reached
      lda sound_channel_max_volume,x
    _ENDIF
    sta sound_channel_volume,x
  _ENDIF
  lda sound_channel_volume,x
  phx
  pha
  txa
  clc
  rol
  clc
  rol
  clc
  rol
  tax
  pla
  sta $fd20,x
  plx
update_continue:
  inx
  txa
  cmp #4
  bne update_channel_x
  rts

; Parse score enough to get next note
; X - channel to use
; Y - abc music pointer
parse_abc::
  jsr abc_read_char
  _IFEQ
    sta sound_channel_busy,x
    jmp update_channel_tail
  _ENDIF
  cmp #$49 ;'I' shift loshift backup flags attack decay
  _IFEQ
    ; Lynx-specific abc-command to set up the instrument
    lda #$42
    sta $fd50
    jsr abc_read_char
    phx
    pha
    txa
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    pla
    sta $fd21,x
    lda #0
    sta $fd23,x
    plx
    jsr abc_read_char
    phx
    pha
    txa
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    pla
    sta $fd24,x
    plx
    jsr abc_read_char
    phx
    pha
    txa
    clc
    rol
    clc
    rol
    clc
    rol
    tax
    pla
    sta $fd25,x
    plx
    jsr abc_read_char
    sta abc_instrument_incr,x
    jsr abc_read_char
    sta abc_instrument_maxlen,x
    jsr abc_read_char
    sta abc_instrument_decr,x
    bra parse_abc
  _ENDIF
  cmp #$7c ;'|'
  _IFEQ
    jsr abc_read_char
    cmp #$3a ;':'
    _IFEQ
      tya
      sta abc_repeat_offs,x
      lda #0
      sta abc_repeat_cnt,x
    _ELSE
      dey
    _ENDIF
    jmp parse_abc
  _ENDIF
  cmp #$3a ;':'
  _IFEQ
    phy
    lda abc_repeat_offs,x
    tay
    pla
    sta abc_repeat_offs,x
    jmp parse_abc
  _ENDIF
  cmp #$50 ;'P' clear sound
  _IFEQ
    lda #0
    sta sound_channel_busy,x
    jmp parse_abc
  _ENDIF
  cmp #$56 ;'V' volume
  _IFEQ
    jsr abc_read_char
    sta abc_note_volume,x
    jmp parse_abc
  _ENDIF
  cmp #$54 ;'T' tempo
  _IFEQ
    jsr abc_read_char
    sta abc_note_length,x
    jmp parse_abc
  _ENDIF
  cmp #$7a ;'z'
  _IFEQ
    lda #0
    bra set_music_ptr
  _ENDIF
  ; Find out the pitch of the note
  stz cur_note
  inc cur_note
  cmp #$3d ;'='
  _IFEQ
    inc cur_note
    jsr abc_read_char
  _ENDIF
  cmp #$7e ;'~'
  _IFEQ
    dec cur_note
    jsr abc_read_char
  _ENDIF
  sec
  sbc #$41 ;'A'
  cmp #8 ;'H'-'A'
  _IFHI
    sec
    sbc #24 ;'a'-'A' + 15
  _ENDIF
  clc
  asl
  clc
  adc cur_note
  sta cur_note
  phy
  tay
  lda _delays,y
  phx
  pha
  txa
  clc
  rol
  clc
  rol
  clc
  rol
  tax
  pla
  sta $fd24,x
  plx
  ply
  ; Find out the volume of the note
  lda abc_note_volume,x
set_music_ptr:
  sta sound_channel_max_volume,x
  ; Find out the duration of the note
  jsr abc_read_char
  cmp #$38 ; "8"
  _IFEQ
    lda abc_note_length,x
    clc
    rol
    clc
    rol
    clc
    rol
  _ELSE
    cmp #$37 ; "7"
    _IFEQ
      lda abc_note_length,x
      clc
      rol
      clc
      rol
      clc
      adc abc_note_length,x
      adc abc_note_length,x
      adc abc_note_length,x
    _ELSE
      cmp #$36 ; "6"
      _IFEQ
        lda abc_note_length,x
        clc
        rol
        clc
        rol
        clc
        adc abc_note_length,x
        adc abc_note_length,x
      _ELSE
        cmp #$35 ; "5"
        _IFEQ
          lda abc_note_length,x
          clc
          rol
          clc
          rol
          clc
          adc abc_note_length,x
        _ELSE
          cmp #$34 ; "4"
          _IFEQ
            lda abc_note_length,x
            clc
            rol
            clc
            rol
          _ELSE
            cmp #$33 ; "3"
            _IFEQ
              lda abc_note_length,x
              clc
              rol
              clc
              adc abc_note_length,x
            _ELSE
              cmp #$32 ; "2"
              _IFEQ
                lda abc_note_length,x
                clc
                rol
              _ELSE
                dey
                lda abc_note_length,x
              _ENDIF
            _ENDIF
          _ENDIF
        _ENDIF
      _ENDIF
    _ENDIF
  _ENDIF
  sta sound_channel_duration,x
parsetail:
  tya
  sta abc_music_ptr,x
  lda abc_instrument_maxlen,x
  sta sound_channel_maxlen,x
  jmp update_channel_x

; This table is used to cover the delays needed for 2 octaves
_delays db 161 ; Ab
        db 152 ; A
        db 143 ; A# Bb
        db 135 ; B
        db 128 ;
        db 255 ; C
        db 241 ; C# Db
        db 227 ; D
        db 214 ; D# Eb
        db 202 ; E
        db 191 ;
        db 191 ; F
        db 180 ; F# Gb
        db 170 ; G
        db 161 ; G#
        db 152 ; A
_delays2
        db 80  ; ab
        db 76  ; a
        db 72  ; a# bb
        db 68  ; b
        db 128 ;
        db 128 ; c
        db 120 ; c# db
        db 114 ; d
        db 107 ; d# eb
        db 101 ; e
        db 96  ;
        db 96  ; f
        db 90  ; f# gb
        db 85  ; g
        db 80  ; g#

; Read a character from the score. Advance ptr if it is not 0
; X - channel
; Y - score offset
abc_read_char::
  txa
  inc
  dec
  _IFEQ
    lda (abc_score_ptr0),y
  _ELSE
    dec
    _IFEQ
      lda (abc_score_ptr1),y
    _ELSE
      dec
      _IFEQ
        lda (abc_score_ptr2),y
      _ELSE
        lda (abc_score_ptr3),y
      _ENDIF
    _ENDIF
  _ENDIF
  php
  _IFNE
    iny
    cpx $5aa5
    cpx $ffff
  _ENDIF
  plp
  rts

abc_music_ptr   db 0,0,0,0
abc_repeat_offs db 0,0,0,0
abc_repeat_cnt  db 0,0,0,0
abc_note_length db 6,6,6,6
abc_note_volume db 64,64,64,64
abc_instrument_incr   db 4,4,4,4
abc_instrument_maxlen db 4,4,4,4
abc_instrument_decr   db 4,4,4,4
sound_channel_busy    db 0,0,0,0
sound_channel_max_volume db 127,127,127,127
sound_channel_volume   db 4,4,4,4
sound_channel_maxlen   db 4,4,4,4
sound_channel_duration db 0,0,0,0
cur_note    dc.b 0

;
; clear screen
;
cls::
                LDAY clsSCB
                jmp DrawSprite

clsSCB          dc.b $c0,$90,$00
                dc.w 0,cls_data
                dc.w 0,0
                dc.w 160*$100,102*$100
cls_color       dc.b $03

cls_data        dc.b 2,$10,0



                include <includes/draw_spr.inc>

pal             STANDARD_PAL

;
; 4-quadrant-sprite => action-point is in the middle
;

sprite_data     ibytes "sprite.spr"


;***************
;* Vertical Blank
;***************
VBL::           
                _IFNE FlipFlag
                  lda ScreenBase
                  ldx ScreenBase2
                  sta ScreenBase2
                  stx ScreenBase
                  sta $fd94
                  lda ScreenBase+1
                  sta $fd95
                  ldx ScreenBase2+1
                  sta ScreenBase2+1
                  stx ScreenBase+1
                  stz FlipFlag
                _ENDIF
                inc Metronome
                END_IRQ

                include <includes/irq.inc>

                END
