#!/bin/bash

# This script compares two ROMs by computing their MD5 checksums
# and seeing if they match.

ROM1=$1
ROM2=$2

MD51=$(md5sum $ROM1|cut -d " " -f1)
MD52=$(md5sum $ROM2|cut -d " " -f1)

echo "ROM1 MD5 : " $MD51
echo "ROM2 MD5 : " $MD52

if [[ $MD51 == $MD52 ]]
then
	echo TEST PASS
else
	echo TEST FAIL
fi

