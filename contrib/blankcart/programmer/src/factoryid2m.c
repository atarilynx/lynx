/*
  Program a Lynx cart
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;

int rom = 0;

void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(4, OUTPUT); // AUDIN chip select
  digitalWrite(4, LOW);
  pinMode(5, OUTPUT); // OE/ pin18
  digitalWrite(5, HIGH);
  pinMode(6, OUTPUT); // OE2/ pin22
  digitalWrite(6, HIGH);
  pinMode(25, OUTPUT); // WR/ pin26
  digitalWrite(25, HIGH);
}

void outputEnable()
{
  digitalWrite(25, HIGH);
  switch (rom) {
  case 0:
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 2:
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 1:
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  case 3:
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  }
}

void outputDisable()
{
  switch (rom) {
  case 0:
  case 1:
    digitalWrite(4, LOW);
    break;
  case 2:
  case 3:
    digitalWrite(4, HIGH);
    break;
  }
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(25, HIGH);
}

void writeEnable()
{
  digitalWrite(25, LOW);
  switch (rom) {
  case 0:
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 2:
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 1:
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  case 3:
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  }
}

void writeDisable()
{
  switch (rom) {
  case 0:
  case 1:
    digitalWrite(4, LOW);
    break;
  case 2:
  case 3:
    digitalWrite(4, HIGH);
    break;
  }
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(25, HIGH);
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  wiringPiSPIDataRW(0, buf, 3);
}

void writeregdbg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  printf("w %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  wiringPiSPIDataRW(0, buf, 3);
  return buf[2];
}

unsigned char readregdbg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  printf("r %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  printf("Set chip modes\n");
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  writereg(0, 0, 0xff); // IODIRA
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

unsigned char readByte(int addr)
{
  unsigned char data, val;
  writeDisable();
  val = addr & 0xff;
  writereg(0, 0x1a, val); // GPIOB
  if (readreg(0, 0x19) != val) printf("Problem 0\n"); // GPIOB
  val = (addr >> 8) & 0xff;
  writereg(1, 0x0a, val); // GPIOA
  if (readreg(1, 0x09) != val) printf("Problem 1\n"); // GPIOB
  val = (addr >> 16) & 0xff;
  writereg(1, 0x1a, val); // GPIOB
  if (readreg(1, 0x19) != val) printf("Problem 2\n"); // GPIOB
  outputEnable();
  //delay(1);
  data = readreg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

unsigned char readBytedbg(int addr)
{
  unsigned char data, val;
  writeDisable();
  val = addr & 0xff;
  writeregdbg(0, 0x1a, val); // GPIOB
  if (readreg(0, 0x19) != val) printf("Problem 0\n"); // GPIOB
  val = (addr >> 8) & 0xff;
  writeregdbg(1, 0x0a, val); // GPIOA
  if (readreg(1, 0x09) != val) printf("Problem 1\n"); // GPIOB
  val = (addr >> 16) & 0xff;
  writeregdbg(1, 0x1a, val); // GPIOB
  if (readreg(1, 0x19) != val) printf("Problem 2\n"); // GPIOB
  outputEnable();
  //delay(1);
  data = readregdbg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

void dumpRegisters()
{
  int i;
  unsigned char a;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(0, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
  printf("Chip 1\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(1, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
}


void writeAddrAndData(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writereg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != ((addr >> 8) & 0xff)) printf("Problem 1\n"); // GPIOB
  writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != ((addr >> 16) & 0xff)) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writereg(0, 0x0a, data); // GPIOA
  writeEnable();
  delay(1); // 20us
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeDisable();
  setDataInputs();
}

void writeAddrAndDatadbg(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writeregdbg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writeregdbg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != ((addr >> 8) & 0xff)) printf("Problem 1\n"); // GPIOB
  writeregdbg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != ((addr >> 16) & 0xff)) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writeregdbg(0, 0x0a, data); // GPIOA
  writeEnable();
  delay(1); // 20us
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeDisable();
  setDataInputs();
}

void byteProgram(int addr, unsigned char data)
{
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAddrAndDatadbg(0x2AAA, 0x55);
  writeAddrAndDatadbg(0x5555, 0xA0);
  writeAddrAndDatadbg(addr, data);
}

void softwareId()
{
  unsigned char manufacturer, flashtype;
  printf("enter special mode\n");
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAddrAndDatadbg(0x2AAA, 0x55);
  writeAddrAndDatadbg(0x5555, 0x90);
  delay(100); // 100ms
  printf("read data\n");
  manufacturer = readBytedbg(0);
  delay(100); // 100ms
  flashtype = readBytedbg(1);
  printf("exit special mode\n");
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0xF0);
  printf("Manufacturer %02x chip %02x\n", manufacturer, flashtype);
}

int main (void)
{
  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  setChipModes();
  setAddressOutputs();
  setAudinOutput();
  writeAudinLow();
  outputDisable();
  rom = 0;
  softwareId();
  rom = 1;
  softwareId();
  rom = 2;
  softwareId();
  rom = 3;
  softwareId();
  return 0 ;
}

