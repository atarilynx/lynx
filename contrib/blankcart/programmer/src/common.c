#include <errno.h>
#include <time.h>
#include "common.h"
#include "console.h"

/*

  Pinout for the programmer board connections. Table shows both sides of the
  Raspberry Pi GPIO header and how they map to the WiringPi and Lynx Cart pins.

  Lynx Cart | WiringPi | RPi GPIO | WiringPi | Lynx Cart
  ----------+----------+----------+----------+----------
  -         | -        | 1      2 | -        | -
  -         | -        | 3      4 | -        | -
  -         | -        | 5      6 | -        | -
  -         | -        | 7      8 | -        | -
  -         | -        | 9     10 | -        | -
  -         | -        | 11    12 | -        | -
  -         | -        | 13    14 | -        | -
  -         | -        | 15    16 | 4        | AUDIN
  -         | -        | 17    18 | 5        | OE
  -         | 12  *SPI | 19    20 | -        | -
  -         | 13  *SPI | 21    22 | 6        | WE
  -         | 14  *SPI | 23    24 | 10  *SPI | -
  -         | -        | 25    26 | -        | -
  -         | -        | 27    28 | -        | -
  -         | -        | 29    30 | -        | -
  -         | -        | 31    32 | -        | -
  -         | -        | 33    34 | -        | -
  -         | -        | 35    36 | -        | -
  SWVCC     | 25       | 37    38 | -        | -
  -         | -        | 39    40 | -        | -

  See also http://wiringpi.com/pins/

 */

int blocksize = 0;
int exitStatus = EXIT_SUCCESS;
int argEnableSwVCC = 0;
int argNoHeader = 0;
int argTurboMode = 0;

static int swVccEnable = 0;

void delay20us()
{
    struct timespec sleeper, dummy;
    sleeper.tv_sec = 0;
    sleeper.tv_nsec = (long)(20000);
    nanosleep(&sleeper, &dummy);
}

void initialise()
{
  echoln(VERBOSE, "Initialising SPI @ %dMhz", SPI_SPEED / 1000000);
  wiringPiSetup() ;
  if (wiringPiSPISetup (0, SPI_SPEED) < 0)
  {
    echoln(NORMAL, "Can't open the SPI bus: %s\n", strerror(errno));
    exitStatus = EXIT_FAILURE;
    cleanUpAndExit(0);
  }

  // reset LED states to start
  pinMode(PIN_LED_ACT, OUTPUT);
  pinMode(PIN_LED_ERR, OUTPUT);
  digitalWrite(PIN_LED_ERR, LOW);

  disableActivityLed();
  enableActivityLed();

  echoln(VERBOSE, "Setting up programmer board");
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  setAudinOutput();
}

void cleanUpAndExit(int sig) {
  if (swVccEnable) {
    swvccDisable();
  }
  disableActivityLed();
  
  exit(exitStatus);
}

void outputEnable()
{
  digitalWrite(PIN_LYNX_CART_OE, LOW);
  if (!argTurboMode) delay20us();
}

void outputDisable()
{
  digitalWrite(PIN_LYNX_CART_OE, HIGH);
}

void setControlOutputs()
{
  pinMode(PIN_LYNX_CART_OE, OUTPUT);
  pinMode(PIN_LYNX_CART_WE, OUTPUT);
}

void writeEnable()
{
  digitalWrite(PIN_LYNX_CART_WE, LOW);
}

void writeDisable()
{
  digitalWrite(PIN_LYNX_CART_WE, HIGH);
}

void setAudinInput()
{
  pinMode(PIN_LYNX_CART_AUDIN, INPUT);
}

void setAudinOutput()
{
  pinMode(PIN_LYNX_CART_AUDIN, OUTPUT);
}

void writeAudinLow()
{
  digitalWrite(PIN_LYNX_CART_AUDIN, LOW);
}

void writeAudinHigh()
{
  digitalWrite(PIN_LYNX_CART_AUDIN, HIGH);
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  wiringPiSPIDataRW(0, buf, 3);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  wiringPiSPIDataRW(0, buf, 3);
  return buf[2];
}

void setChipModes()
{
  int chip;
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, REG_IOCON, 0xa8);
  }
}

void setDataInputs()
{
  writereg(0, REG_IODIRA, 0xff);
  writereg(0, REG_GPPUA, 0xff);
}

void setDataOutputs()
{
  writereg(0, REG_IODIRA, 0);
  writereg(0, REG_GPPUA, 0); // TODO: check with Karri if this should be set to 0
}

void setAddressOutputs()
{
  writereg(0, REG_IODIRB, 0);
  writereg(1, REG_IODIRA, 0);
  writereg(1, REG_IODIRB, 0);
}

void swvccEnable()
{
  pinMode(PIN_LYNX_CART_SWVCC, OUTPUT);
  digitalWrite(PIN_LYNX_CART_SWVCC, HIGH);

  swVccEnable = 1;
}

void swvccDisable() {
  pinMode(PIN_LYNX_CART_SWVCC, OUTPUT);
  digitalWrite(PIN_LYNX_CART_SWVCC, LOW);

  swVccEnable = 0;
}

void enableActivityLed()
{
  digitalWrite(PIN_LED_ACT, HIGH);
}

void disableActivityLed()
{
  digitalWrite(PIN_LED_ACT, LOW);
}

unsigned char readBytedbg(int addr)
{
  unsigned char data, val;
  writeDisable();
  if ((addr & 1024) == 0) {
    writeAudinLow();
  } else {
    writeAudinHigh();
  }
  val = addr & 0xff;
  writereg(0, 0x1a, val); // GPIOB
  if (readreg(0, 0x19) != val) echoln(NORMAL, "Problem 0"); // GPIOB
  val = (addr >> 8) & 0xff;
  writereg(1, 0x0a, val); // GPIOA
  if (readreg(1, 0x09) != val) echoln(NORMAL, "Problem 1"); // GPIOB
  val = (addr >> 16) & 0xff;
  writereg(1, 0x1a, val); // GPIOB
  if (readreg(1, 0x19) != val) echoln(NORMAL, "Problem 2\n"); // GPIOB
  outputEnable();
  data = readreg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

void writeAddrAndData(int addr, unsigned char data)
{
  unsigned char realdata;

  outputDisable();
  if (argTurboMode) {
    writereg(0, 0x1a, addr & 0xff); // GPIOB
    writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
    writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
    
    setDataOutputs();
    if ((addr & 1024) == 0) {
      writeAudinLow();
    } else {
      writeAudinHigh();
    }
    writereg(0, 0x0a, data); // GPIOA
    writeEnable();
    
    realdata = readreg(0, 0x9);
    if (realdata != data) echoln(NORMAL, "Problem data %02x %02x", realdata, data); // GPIOB
  }
  else {
    writereg(0, 0x1a, addr & 0xff); // GPIOB
    if (readreg(0, 0x19) != (addr & 0xff)) echoln(NORMAL, "Problem 0"); // GPIOB
    
    writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
    if (readreg(1, 0x09) != ((addr >> 8) & 0xff)) echoln(NORMAL, "Write Problem 1"); // GPIOB

    writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
    if (readreg(1, 0x19) != ((addr >> 16) & 0xff)) echoln(NORMAL, "Problem 2"); // GPIOB
    
    setDataOutputs();
    if ((addr & 1024) == 0) {
      writeAudinLow();
    } else {
      writeAudinHigh();
    }
    writereg(0, 0x0a, data); // GPIOA
    writeEnable();
    
    delay20us();

    realdata = readreg(0, 0x9);
    if (realdata != data) echoln(NORMAL, "Problem data %02x %02x", realdata, data); // GPIOB
  }  

  writeDisable();
  setDataInputs();
}

void getFlashInfo(unsigned char* flashInfo) {
  echoln(VERBOSE, "Determining chip ID");
  
  echoln(DEBUG, "Entering special mode");
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x90);
  delay(100); // 100ms

  echoln(DEBUG, "Reading chip data");
  flashInfo[0] = readBytedbg(0); // manufacturer
  delay(100); // 100ms
  flashInfo[1] = readBytedbg(1); // flashtype

  echoln(DEBUG, "Exiting special mode");
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0xF0);

  echoln(NORMAL, "Chip manufacturer ID is %02x, flash ID is %02x", flashInfo[0], flashInfo[1]);
  if (flashInfo[0] == 0xff && flashInfo[1] == 0xff) {
    echoln(NORMAL, "Cartridge may not be making good contact with slot connector");
  }
}
