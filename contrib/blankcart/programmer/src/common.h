#ifndef __PROGRAMMER_COMMON__
#define __PROGRAMMER_COMMON__

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>

#define ELAPSED_SECONDS curTime.tv_sec - startTime.tv_sec

// max SPI speed for MCP23S17 is 10Mhz
#define SPI_SPEED 10000000

// pins controlled directly via RPi GPIO
#define PIN_LYNX_CART_AUDIN 4
#define PIN_LYNX_CART_OE 5
#define PIN_LYNX_CART_WE 6
#define PIN_LYNX_CART_SWVCC 25
#define PIN_LED_ACT 2
#define PIN_LED_ERR 0

// channels controlled via the MCP23S17-E/SO chip
#define REG_IOCON 0xa
#define REG_IODIRA 0
#define REG_GPPUA 6
#define REG_IODIRB 0x10
#define REG_GPIOA 0x09
#define REG_GPIOB 0x19

extern int blocksize;
extern int exitStatus;
extern int argEnableSwVCC;
extern int argTurboMode;
extern int argNoHeader;

void cleanUpAndExit(int sig);
void delay20us();
void initialise();
void outputEnable();
void outputDisable();
void setControlOutputs();
void setAudinOutput();
void setAudinInput();
void writeEnable();
void writeDisable();
void writeAudinLow();
void writeAudinHigh();
void writereg(int chip, int reg, unsigned char data);
unsigned char readreg(int chip, int reg);
void setChipModes();
void setDataInputs();
void setDataOutputs();
void setAddressOutputs();
void swvccEnable();
void swvccDisable();
void enableActivityLed();
void disableActivityLed();
void getFlashInfo(unsigned char* flashInfo);
unsigned char readBytedbg(int addr);
void writeAddrAndData(int addr, unsigned char data);

#endif // __PROGRAMMER_COMMON__