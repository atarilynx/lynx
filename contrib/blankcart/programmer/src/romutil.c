/**
 * ROM Utility. 
 */

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "console.h"
#include "lnxheader.h"

// LNX file header information as per: https://bitbucket.org/atarilynx/lynx/src/cd1c78cf3a25b8e9cb22c930d6204fbe8c6bf3c6/tools/cc65/libsrc/lynx/exehdr.s?at=master&fileviewer=file-view-default
//
// Byte  0 -  3  | String literal | "LYNX"
//       4 -  5  | 16-bit uchar   | Bank 0 page size
//       6 -  7  | 16-bit uchar   | Bank 1 page size
//       8 -  9  | 16-bit uchar   | Version number
//      10 - 41  | String + \0    | Cart name
//      42 - 57  | String + \0    | Manufacturer
//           58  | uchar          | Rotation (1=left, 2=right)
//           59  | uchar          | AUDIN Used (1=yes)
//           60  | uchar          | EEPROM detail, see below
//      61 - 63  | uchar          | Unused
//
// EEPROM byte-bits when set encode details of EEPROM used
// bit  1   2   3   4   5   6   7   8
//      |       |   |       |   |   |
//      +---+---+   +---+---+   |  16/8 BIT
//          |           |     REAL/SD
//     EEPROM TYPE    UNUSED 
//
// Example: 0x40 (00000010) encodes a no-EEPROM, SD card saves
//
// EEPROM Type values - 0: no EEPROM
//                      1: 93C46
//                      2: 93C56
//                      3: 93C66
//                      4: 93C76
//                      5: 93C86
// EEPROM size is 2^(type+9) e.g. 2^(1+9) = 1024 bits for 93C46

#define EEPROM_TYPE_MASK 0x7
#define USE_SD_EEPROM_MASK 0x40
#define EEPROM_16_8_BIT_MASK 0x80

static char* romFile = 0;
static char* romFile2 = 0;

typedef enum {
  CMD_NONE,
  CMD_INFO,
  CMD_2LYX,
  CMD_2LNX,
  CMD_COMP
} CMD_TYPE;
static CMD_TYPE argCommand = CMD_NONE;

/**
 * Shows the help/usage summary and exits.
 */
void showHelp() {
  echoln(NORMAL, "Usage: romutil -2lyx|-2lnx|-cmp|-i <romfile> [<romfile2>]");
  echoln(NORMAL, "\t-2lyx\tConvert LNX ROM romfile to LYX ROM romfile2");
  echoln(NORMAL, "\t-2lnx\tConvert LYX ROM romfile to LNX ROM romfile2 adding default header");
  echoln(NORMAL, "\t-cmp\tCompare two ROMs, input can be LNX or LYX files");
  echoln(NORMAL, "\t-h\tShow this summary and exit");
  echoln(NORMAL, "\t-i\tShow LNX ROM header information");

  exit(0);
}

/**
 * Processes command line arguments and sets global variables to reflect the
 * chosen options.
 */
void processArgs(int argc, char *argv[])
{
  int i;

  if (argc == 1) {
    showHelp();
    return;
  }

  for (i = 1; i < argc; i++) {
    // dash options
    if (argv[i][0] == '-') {
      if (!strcmp("-h", argv[i]) || !strcmp("--help", argv[i])) { showHelp(); break; }
      else if (!strcmp("-i", argv[i])) { argCommand = CMD_INFO; }
      else if (!strcmp("-2lnx", argv[i])) { argCommand = CMD_2LNX; }
      else if (!strcmp("-2lyx", argv[i])) { argCommand = CMD_2LYX; }
      else if (!strcmp("-cmp", argv[i])) { argCommand = CMD_COMP; }
    }
    // rom file name
    else if (romFile == 0) {
      int len = strlen(argv[i]);
      romFile = malloc(len + 1);
      strncpy(romFile, argv[i], len);
    }
    else if (romFile2 == 0) {
      int len = strlen(argv[i]);
      romFile2 = malloc(len + 1);
      strncpy(romFile2, argv[i], len);
    }
  }

  if (romFile == 0) {
    echoln(NORMAL, "ROM file name not provided, run `romutil -h` for usage");
    exit(EXIT_FAILURE);
  }

  if (argCommand > CMD_INFO && romFile2 == 0) {
    echoln(NORMAL, "ROM file name not provided, run `romutil -h` for usage");
    exit(EXIT_FAILURE);
  }
}

/**
 * Displays ROM header information
 */
static void showRomInfo() {
  unsigned char lnx[64];
  char cartName[32];
  char manufacturer[16];
  int eepromType, eepromSize;
  FILE *fp = fopen(romFile, "r"); // Open lnx file
  
  fread(lnx, 64, 1, fp);
  fclose(fp);

  if (lnx[0] != 'L' || lnx[1] != 'Y' || lnx[2] != 'N' || lnx[3] != 'X') {
    echoln(NORMAL, "Not a LYNX ROM file!");
    return;
  }
  else {
    echoln(NORMAL, "LYNX ROM file detected");
  }

  strncpy(cartName, (char*) &lnx[10], 32);
  echoln(NORMAL, "Cart name\t\t%s", cartName);

  strncpy(manufacturer, (char*) &lnx[42], 16);
  echoln(NORMAL, "Manufacturer\t\t%s", manufacturer);

  echoln(NORMAL, "Version number\t\t%d", lnx[8] | (lnx[9] << 8));

  echoln(NORMAL, "Bank 0 page size\t%d", lnx[4] | (lnx[5] << 8));
  echoln(NORMAL, "Bank 1 page size\t%d", lnx[6] | (lnx[7] << 8));

  if (lnx[58] == 0) { echoln(NORMAL, "Rotation\t\tUnspecified"); }
  else if (lnx[58] == 1) { echoln(NORMAL, "Rotation\t\tLeft"); }
  else if (lnx[58] == 2) { echoln(NORMAL, "Rotation\t\tRight"); }
  else echoln(NORMAL, "Rotation\t\tInvalid value set");

  if (lnx[59] == 0) { echoln(NORMAL, "AUDIN Addressing\tNo"); }
  else if (lnx[59] == 1) { echoln(NORMAL, "AUDIN Addressing\tYes"); }
  else echoln(NORMAL, "AUDIN Addressing\tInvalid value set");

  echoln(NORMAL, "EEPROM Flag\t\t%X", lnx[60]); 
  eepromType = lnx[60] & EEPROM_TYPE_MASK;
  eepromSize = pow(2, (eepromType + 9));
  switch (eepromType) {
    case 0: echoln(NORMAL, "EEPROM\t\t\tNo"); break;
    case 1: echoln(NORMAL, "EEPROM\t\t\t93C46 [%d bits]", eepromSize); break;
    case 2: echoln(NORMAL, "EEPROM\t\t\t93C56 [%d bits]", eepromSize); break;
    case 3: echoln(NORMAL, "EEPROM\t\t\t93C66 [%d bits]", eepromSize); break;
    case 4: echoln(NORMAL, "EEPROM\t\t\t93C76 [%d bits]", eepromSize); break;
    case 5: echoln(NORMAL, "EEPROM\t\t\t93C86 [%d bits]", eepromSize); break;
  }
  
  if (lnx[60]) {
    if (lnx[60] & USE_SD_EEPROM_MASK) { echoln(NORMAL, "Real EEPROM\t\tNo - SD card save file"); }
    else {
      echoln(NORMAL, "Real EEPROM\t\tYes");
      
      if (lnx[60] & EEPROM_16_8_BIT_MASK) { echoln(NORMAL, "EEPROM Mode\t\t8-bit"); }
      else { echoln(NORMAL, "EEPROM Mode\t\t16-bit"); }
    }
  }
}

static void lnxToLYX() {
  unsigned char lnx[64];
  FILE *fpr = fopen(romFile, "r");
  FILE *fpw;
  char buffer[1];

  fread(lnx, 64, 1, fpr);

  if (lnx[0] != 'L' || lnx[1] != 'Y' || lnx[2] != 'N' || lnx[3] != 'X') {
    echoln(NORMAL, "Not a LYNX ROM file!");
    fclose(fpr);
    exit(EXIT_FAILURE);
  }

  fpw = fopen(romFile2, "w");
  while (fread(&buffer, 1, 1, fpr)) {
    fwrite(&buffer, 1, 1, fpw);
  }
  
  fclose(fpr);
  fclose(fpw);
}

static void lyxToLNX() {
  FILE *fpr = fopen(romFile, "r");
  FILE *fpw = fopen(romFile2, "w");
  char buffer[1];

  fwrite(LXN_HDR, 1, 64, fpw);

  while (fread(&buffer, 1, 1, fpr)) {
    fwrite(&buffer, 1, 1, fpw);
  }
  
  fclose(fpr);
  fclose(fpw);
}

static void compareRoms() {
  FILE *fp1 = fopen(romFile, "r");
  FILE *fp2 = fopen(romFile2, "r");
  unsigned char lnx[64], lnx2[64];
  char buffer[1], buffer2[1];
  char match = 1;

  fread(lnx, 64, 1, fp1);
  fread(lnx2, 64, 1, fp2);

  if (lnx[0] != 'L' || lnx[1] != 'Y' || lnx[2] != 'N' || lnx[3] != 'X') {
    rewind(fp1);
  }

  if (lnx2[0] != 'L' || lnx2[1] != 'Y' || lnx2[2] != 'N' || lnx2[3] != 'X') {
    rewind(fp2);
  }

  while (fread(&buffer, 1, 1, fp1)) {
    if (fread(&buffer2, 1, 1, fp2)) {
      if (buffer[0] != buffer2[0]) {
        match = 0;
        break;
      }
    }
    else {
      match = 0;
      break;
    }
  }

  if (fread(&buffer2, 1, 1, fp2)) {
    match = 0;
  }

  fclose(fp1);
  fclose(fp2);

  if (match) {
    echoln(NORMAL, "ROMs match!");
  }
  else {
    echoln(NORMAL, "ROMs do not match!");
    exit(EXIT_FAILURE);
  }
}

int main (int argc, char *argv[])
{
  processArgs(argc, argv);

  switch (argCommand) {
    case CMD_NONE: showHelp(); break;
    case CMD_INFO: showRomInfo(); break;
    case CMD_2LNX: lyxToLNX(); break;
    case CMD_2LYX: lnxToLYX(); break;
    case CMD_COMP: compareRoms(); break;
  }
}