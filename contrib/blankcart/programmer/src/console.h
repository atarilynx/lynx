#ifndef __PROGRAMMER_CONSOLE__
#define __PROGRAMMER_CONSOLE__

typedef enum {
  NORMAL = 1,
  VERBOSE,
  DEBUG
} ECHO_LEVEL;

void setEchoPriority(ECHO_LEVEL priority);
void echo(ECHO_LEVEL priority, const char *format, ...);
void echoln(ECHO_LEVEL priority, const char *format, ...);

#endif // __PROGRAMMER_CONSOLE__