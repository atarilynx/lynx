/*
  Verify a Lynx cart
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <time.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;

void delay20us()
{
    struct timespec sleeper, dummy;

    sleeper.tv_sec = 0;
    sleeper.tv_nsec = (long)(20000);
    nanosleep(&sleeper, &dummy);
}

void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(5, OUTPUT); // OE/ pin18
  digitalWrite(5, HIGH);
  //printf("OE* = 1\n");
  pinMode(6, OUTPUT); // WR/ pin22
  digitalWrite(6, HIGH);
  //printf("WR* = 1\n");
}

void outputEnable()
{
  digitalWrite(5, LOW);
  //printf("OE* = 0\n");
}

void outputDisable()
{
  digitalWrite(5, HIGH);
  //printf("OE* = 1\n");
}

void writeEnable()
{
  digitalWrite(6, LOW);
  //printf("WE* = 0\n");
}

void writeDisable()
{
  digitalWrite(6, HIGH);
  //printf("WE* = 1\n");
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  //printf("AUDIN = in\n");
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
  //printf("AUDIN = out\n");
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
}

void writeregdbg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  printf("w %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  return buf[2];
}

unsigned char readregdbg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  printf("r %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  //printf("Set chip modes\n");
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  //printf("Set data inputs\n");
  writereg(0, 0, 0xff); // IODIRA
  //printf("Set input pullups\n");
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  //printf("Set data outputs\n");
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  //printf("Set address outputs\n");
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

unsigned char readByte(int addr)
{
  unsigned char data, val;
  writeDisable();
  val = addr & 0xff;
  writereg(0, 0x1a, val); // GPIOB
  if (readreg(0, 0x19) != val) printf("Problem 0\n"); // GPIOB
  val = (addr >> 8) & 0xff;
  writereg(1, 0x0a, val); // GPIOA
  if (readreg(1, 0x09) != val) printf("Problem 1\n"); // GPIOB
  val = (addr >> 16) & 0xff;
  writereg(1, 0x1a, val); // GPIOB
  if (readreg(1, 0x19) != val) printf("Problem 2\n"); // GPIOB
  outputEnable();
  data = readreg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

unsigned char readBytedbg(int addr)
{
  unsigned char data, val;
  writeDisable();
  val = addr & 0xff;
  writeregdbg(0, 0x1a, val); // GPIOB
  if (readreg(0, 0x19) != val) printf("Problem 0\n"); // GPIOB
  val = (addr >> 8) & 0xff;
  writeregdbg(1, 0x0a, val); // GPIOA
  if (readreg(1, 0x09) != val) printf("Problem 1\n"); // GPIOB
  val = (addr >> 16) & 0xff;
  writeregdbg(1, 0x1a, val); // GPIOB
  if (readreg(1, 0x19) != val) printf("Problem 2\n"); // GPIOB
  outputEnable();
  data = readregdbg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

void dumpRegisters()
{
  int i;
  unsigned char a;

  //printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(0, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
  //printf("Chip 1\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(1, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
}


void writeAddrAndData(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writereg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != ((addr >> 8) & 0xff)) printf("Problem 1\n"); // GPIOB
  writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != ((addr >> 16) & 0xff)) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writereg(0, 0x0a, data); // GPIOA
  writeEnable();
  delay20us();
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeDisable();
  setDataInputs();
}

void writeAddrAndDatadbg(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writeregdbg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writeregdbg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != ((addr >> 8) & 0xff)) printf("Write Problem 1\n"); // GPIOB
  writeregdbg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != ((addr >> 16) & 0xff)) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writeregdbg(0, 0x0a, data); // GPIOA
  writeEnable();
  delay20us();
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeDisable();
  setDataInputs();
}

int byteVerify(int addr, unsigned char data)
{
  if (readByte(addr) != data) return 1;
  return 0;
}

void chipErase()
{
  setDataOutputs();
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x80);
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x10);
  delay(100); // 100ms
}

unsigned char softwareId()
{
  unsigned char manufacturer, flashtype;
  printf("softwareId\n");
  printf("enter special mode\n");
  writeAudinHigh();
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAudinLow();
  writeAddrAndDatadbg(0x2AAA, 0x55);
  writeAudinHigh();
  writeAddrAndDatadbg(0x5555, 0x90);
  delay(100); // 100ms
  printf("read data\n");
  writeAudinLow();
  manufacturer = readBytedbg(0);
  delay(100); // 100ms
  flashtype = readBytedbg(1);
  printf("exit special mode\n");
  writeAudinHigh();
  writeAddrAndData(0x5555, 0xAA);
  writeAudinLow();
  writeAddrAndData(0x2AAA, 0x55);
  writeAudinHigh();
  writeAddrAndData(0x5555, 0xF0);
  writeAudinLow();
  printf("Manufacturer %02x chip %02x\n", manufacturer, flashtype);
  if ((manufacturer == 0xbf) && (flashtype == 0xb7)) {
      return 1;
  } else {
      printf("Cannot recognize the right type of chip for this programmer\n");
      return 0;
  }
}

int readHeader(FILE *fp)
{
    int blocksize;
    unsigned char lnx_header[64];
    fread(lnx_header, 64, 1, fp);
    blocksize = lnx_header[4] | (lnx_header[5] << 8);
    return blocksize;
}

int healthCheckLocation(int addr, unsigned char val)
{
  return 1;
}

int healthCheck()
{
  if (!healthCheckLocation(256*1024, 66)) return 0;
  if (!healthCheckLocation(128*1024, 34)) return 0;
  if (!healthCheckLocation(64*1024, 18)) return 0;
  if (!healthCheckLocation(32*1024, 7)) return 0;
  if (!healthCheckLocation(16*1024, 129)) return 0;
  if (!healthCheckLocation(8*1024, 65)) return 0;
  if (!healthCheckLocation(4*1024, 33)) return 0;
  if (!healthCheckLocation(2*1024, 17)) return 0;
  if (!healthCheckLocation(1024, 9)) return 0;
  if (!healthCheckLocation(512, 5)) return 0;
  if (!healthCheckLocation(256, 3)) return 0;
  if (!healthCheckLocation(128, 128)) return 0;
  if (!healthCheckLocation(64, 64)) return 0;
  if (!healthCheckLocation(32, 32)) return 0;
  if (!healthCheckLocation(16, 16)) return 0;
  if (!healthCheckLocation(8, 8)) return 0;
  if (!healthCheckLocation(4, 4)) return 0;
  if (!healthCheckLocation(2, 2)) return 0;
  if (!healthCheckLocation(1, 1)) return 0;
  return 1;
}

int main (int argc, char *argv[])
{
  int i, addr;
  size_t blocknr;
  int blocksize = 512;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 16000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  setAudinOutput();

  //dumpRegisters();
  if (softwareId()) {
    FILE *fp;
    unsigned char block[2048];
    printf("Verify Lynx cart from %s\n", argv[1]);
    fp = fopen(argv[1], "r"); // Open lnx file
    blocksize = readHeader(fp); // Read header
    printf("Blocksize %d\n", blocksize);
    if (healthCheck()) {
      addr = 0;
      blocknr = 0;
      while (blocknr < 256) {
        printf("Block %d\n", addr/2048);
        fread(block, blocksize, 1, fp);
        for (i = 0; i < 2048; i++) {
          if (i < blocksize) {
            if (byteVerify(addr, block[i])) {
              printf("Verify failed at %d\n", addr);
            }
          }
          addr++;
        }
        blocknr++;
      }
    }
    fclose(fp);
  }
  return 0 ;
}

