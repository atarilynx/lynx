/*
 * Read a Lynx eeprom 93c46
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(5, OUTPUT); // OE/ pin18
  pinMode(6, OUTPUT); // WR/ pin22
}

void outputEnable()
{
  digitalWrite(5, LOW);
  delay(1); // 20us
}

void outputDisable()
{
  digitalWrite(5, HIGH);
}

void writeEnable()
{
  digitalWrite(6, LOW);
}

void writeDisable()
{
  digitalWrite(6, HIGH);
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  pullUpDnControl(4, PUD_OFF);
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
  //printf("0 ");
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
  //printf("1 ");
}

unsigned char readAudin()
{
    if (digitalRead(4) == HIGH) {
        //printf("H ");
        return 1;
    } else {
        //printf("L ");
        return 0;
    }
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  writereg(0, 0, 0xff); // IODIRA
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

void dumpRegisters()
{
  int i;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(0, i);
  }
  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(1, i);
  }
}

void writeAddr(int addr)
{
  writereg(0, 0x19, addr & 0xff); // GPIOB
  //printf("%02x ", readreg(0, 0x19));
}

void setCsHigh()
{
    writeAddr(0x0080);
}

void setCsLow()
{
    writeAddr(0x0000);
}

void setClkHigh()
{
    writeAddr(0x0082);
}

void setClkLow()
{
    writeAddr(0x0080);
}

void sendeeprombit(char bit)
{
    if (bit == 0) {
        writeAudinLow();
    } else {
        writeAudinHigh();
    }
    setClkHigh();
    setClkLow();
}

void sendeepromstart()
{
   setAudinOutput();
   writeAudinLow();
   setCsHigh();
   sendeeprombit(1);
}

void sendeepromread()
{
   sendeepromstart();
   sendeeprombit(1);
   sendeeprombit(0);
}

void sendeepromaddr(int addr)
{
   sendeepromread();
   sendeeprombit((addr >> 5) & 1);
   sendeeprombit((addr >> 4) & 1);
   sendeeprombit((addr >> 3) & 1);
   sendeeprombit((addr >> 2) & 1);
   sendeeprombit((addr >> 1) & 1);
   sendeeprombit(addr & 1);
}

char readeeprombit()
{
    setClkHigh();
    setClkLow();
    return readAudin();
}

unsigned short readeeprom(int addr)
{
    unsigned short val = 0;
    sendeepromaddr(addr);
    setAudinInput();
    val |= readeeprombit() << 15;
    val |= readeeprombit() << 14;
    val |= readeeprombit() << 13;
    val |= readeeprombit() << 12;
    val |= readeeprombit() << 11;
    val |= readeeprombit() << 10;
    val |= readeeprombit() << 9;
    val |= readeeprombit() << 8;
    val |= readeeprombit() << 7;
    val |= readeeprombit() << 6;
    val |= readeeprombit() << 5;
    val |= readeeprombit() << 4;
    val |= readeeprombit() << 3;
    val |= readeeprombit() << 2;
    val |= readeeprombit() << 1;
    val |= readeeprombit();
    setCsLow();
    return val;
}

int main (int argc, char *argv[])
{
  int i;
  unsigned short block[64];
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  writeAddr(0x0000);
  setAudinOutput();

  printf ("Read 128 byte eeprom to eeprom.dat\n");
  writeAudinLow();
  for (i = 0; i < 64; i++) {
    block[i] = readeeprom(i);
    printf("%04x ", block[i]);
  }
  fp = fopen("eeprom.dat", "w");
  fwrite(block, 1, 128, fp);
  fclose(fp);
  
  return 0 ;
}

