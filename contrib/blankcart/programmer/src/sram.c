/*
 * Read a Lynx cart
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(5, OUTPUT); // OE/ pin18
  pinMode(6, OUTPUT); // WR/ pin22
}

void outputEnable()
{
  printf("Output enable\n");
  digitalWrite(5, LOW);
  delay(1); // 20us
}

void outputDisable()
{
  printf("Output disable\n");
  digitalWrite(5, HIGH);
}

void writeEnable()
{
  printf("Write enable\n");
  digitalWrite(6, LOW);
}

void writeDisable()
{
  printf("Write disable\n");
  digitalWrite(6, HIGH);
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  int i;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  status = wiringPiSPIDataRW(0, buf, 3);
}

void writeregdbg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  int i;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  printf("wc %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("wd  %d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  status = wiringPiSPIDataRW(0, buf, 3);
  return buf[2];
}

unsigned char readregdbg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  printf("rc %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("rd %d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  printf("Set chip modes\n");
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  printf("Set data inputs\n");
  writereg(0, 0, 0xff); // IODIRA
  printf("Set input pullups\n");
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  printf("Set data outputs\n");
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  printf("Set address outputs\n");
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

unsigned char readByte(int addr)
{
  unsigned char data;
  writeDisable();
  writereg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != (addr >> 8) & 0xff) printf("Problem 1\n"); // GPIOB
  writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != (addr >> 16) & 0xff) printf("Problem 2\n"); // GPIOB
  outputEnable();
  delay(1);
  data = readreg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

unsigned char readBytedbg(int addr)
{
  unsigned char data;
  writeDisable();
  writeregdbg(0, 0x1a, addr & 0xff); // GPIOB
  if (readregdbg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writeregdbg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readregdbg(1, 0x09) != (addr >> 8) & 0xff) printf("Problem 1\n"); // GPIOB
  writeregdbg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readregdbg(1, 0x19) != (addr >> 16) & 0xff) printf("Problem 2\n"); // GPIOB
  outputEnable();
  delay(1);
  data = readregdbg(0, 0x09); // GPIOA
  outputDisable();
  return data;
}

void dumpRegisters()
{
  int i;
  unsigned char a;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(0, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
  printf("Chip 1\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(1, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
}


void writeAddrAndData(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writereg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != (addr >> 8) & 0xff) printf("Problem 1\n"); // GPIOB
  writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != (addr >> 16) & 0xff) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writereg(0, 0x0a, data); // GPIOA
  delay(1); // 20us
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeEnable();
  delay(1); // 20us
  writeDisable();
  setDataInputs();
}

void writeAddrAndDatadbg(int addr, unsigned char data)
{
  unsigned char realdata;
  outputDisable();
  writeregdbg(0, 0x1a, addr & 0xff); // GPIOB
  if (readreg(0, 0x19) != (addr & 0xff)) printf("Problem 0\n"); // GPIOB
  writeregdbg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  if (readreg(1, 0x09) != (addr >> 8) & 0xff) printf("Problem 1\n"); // GPIOB
  writeregdbg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  if (readreg(1, 0x19) != (addr >> 16) & 0xff) printf("Problem 2\n"); // GPIOB
  setDataOutputs();
  writeregdbg(0, 0x0a, data); // GPIOA
  delay(1); // 20us
  realdata = readreg(0, 0x9);
  if (realdata != data) printf("Problem data %02x %02x\n", realdata, data); // GPIOB
  writeEnable();
  delay(1); // 20us
  writeDisable();
  setDataInputs();
}

void byteProgram(int addr, unsigned char data)
{
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAddrAndDatadbg(0x2AAA, 0x55);
  writeAddrAndDatadbg(0x5555, 0xA0);
  writeAddrAndDatadbg(addr, data);
}

void chipErase()
{
  setDataOutputs();
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x80);
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x10);
  delay(100); // 100ms
}

void softwareId()
{
  unsigned char manufacturer, flashtype;
  printf("softwareId\n");
  printf("enter special mode\n");
  writeAddrAndDatadbg(0x0000, 0x12);
  writeAddrAndDatadbg(0x0001, 0x34);
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAddrAndDatadbg(0x5555, 0xAA);
  writeAddrAndDatadbg(0x2AAA, 0x55);
  writeAddrAndDatadbg(0x5555, 0x90);
  delay(100); // 100ms
  printf("read data\n");
  manufacturer = readBytedbg(0);
  delay(100); // 100ms
  flashtype = readBytedbg(1);
  printf("exit special mode\n");
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0xF0);
  printf("Manufacturer %02x chip %02x\n", manufacturer, flashtype);
}

int main (void)
{
  int i;
  size_t blocknr;
  unsigned char block[2048];
  unsigned char hdr[64];
  int cartmode = 128;
  int blocksize = 512;
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  //dumpRegisters();
  //softwareId();

  printf ("Program Lynx cart from cart.lnx\n") ;
  //chipErase();
  //for (i = 0; i < 2048; i++) {
  //  byteProgram(i, i & 0xff);
  //}
  return 0 ;
}

