/*
 * Read a Lynx eeprom 24aa512
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(5, OUTPUT); // OE/ pin18
  pinMode(6, OUTPUT); // WR/ pin22
}

void outputEnable()
{
  digitalWrite(5, LOW);
  delay(1); // 20us
}

void outputDisable()
{
  digitalWrite(5, HIGH);
}

void writeEnable()
{
  digitalWrite(6, LOW);
}

void writeDisable()
{
  digitalWrite(6, HIGH);
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  pullUpDnControl(4, PUD_OFF);
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
  //printf("0 ");
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
  //printf("1 ");
}

unsigned char readAudin()
{
    if (digitalRead(4) == HIGH) {
        //printf("H ");
        return 1;
    } else {
        //printf("L ");
        return 0;
    }
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  writereg(0, 0, 0xff); // IODIRA
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

void dumpRegisters()
{
  int i;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(0, i);
  }
  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(1, i);
  }
}

void writeAddr(int addr)
{
  writereg(0, 0x19, addr & 0xff); // GPIOB
  //printf("%02x ", readreg(0, 0x19));
}

void setClkHigh()
{
    writeAddr(0x0002);
}

void setClkLow()
{
    writeAddr(0x0000);
}

void sendeeprombit(char bit)
{
   setAudinOutput();
   if (bit == 0) {
       writeAudinLow();
   } else {
       writeAudinHigh();
   }
   setClkHigh();
   // Data or address valid
   delay(10); // 200us
   setClkLow();
}

char readeeprombit()
{
    char val;
    setAudinInput();
    setClkHigh();
    val = readAudin();
    setClkLow();
    return val;
}

void sendeepromstart()
{
   setAudinOutput();
   writeAudinHigh();
   setClkHigh();
   writeAudinLow();
   setClkLow();
}

void sendeepromstop()
{
   setAudinOutput();
   writeAudinLow();
   setClkHigh();
   writeAudinHigh();
   setClkLow();
}

char sendeepromwrite()
{
   sendeepromstart();
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   delay(10); // 200us
   if (readeeprombit() == 1) {
      sendeepromstop();
      printf("ErrorA");
      return 1;
   }
   return 0;
}

char sendeepromaddrdata(int addr, unsigned char data)
{
   sendeepromstop();
       delay(100); // 200us
   if (sendeepromwrite() == 0) {
       sendeeprombit((addr >> 15) & 1);
       sendeeprombit((addr >> 14) & 1);
       sendeeprombit((addr >> 13) & 1);
       sendeeprombit((addr >> 12) & 1);
       sendeeprombit((addr >> 11) & 1);
       sendeeprombit((addr >> 10) & 1);
       sendeeprombit((addr >> 9) & 1);
       sendeeprombit((addr >> 8) & 1);
       delay(100); // 200us
       if (readeeprombit() == 1) {
           printf("ErrorF");
           sendeepromstop();
           printf("ErrorB");
           return 1;
       }
       sendeeprombit((addr >> 7) & 1);
       sendeeprombit((addr >> 6) & 1);
       sendeeprombit((addr >> 5) & 1);
       sendeeprombit((addr >> 4) & 1);
       sendeeprombit((addr >> 3) & 1);
       sendeeprombit((addr >> 2) & 1);
       sendeeprombit((addr >> 1) & 1);
       sendeeprombit(addr & 1);
       delay(200); // 200us
       if (readeeprombit() == 1) {
           printf("ErrorE");
           sendeepromstop();
           printf("ErrorC");
           return 1;
       }
       sendeeprombit((data >> 7) & 1);
       sendeeprombit((data >> 6) & 1);
       sendeeprombit((data >> 5) & 1);
       sendeeprombit((data >> 4) & 1);
       sendeeprombit((data >> 3) & 1);
       sendeeprombit((data >> 2) & 1);
       sendeeprombit((data >> 1) & 1);
       sendeeprombit(data & 1);
       delay(10); // 200us
       if (readeeprombit() == 1) {
           printf("ErrorD");
           delay(10); // 200us
           sendeepromstop();
           return 1;
       }
       sendeepromstop();
       return 0;
   }
   return 1;
}

unsigned short writeeeprom(int addr, unsigned char data)
{
    if (sendeepromaddrdata(addr, data) == 0) {
        return 0;
    }
    return 1;
}

int main (int argc, char *argv[])
{
  int i;
  unsigned char block[128];
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  writeAddr(0x0000);
  setAudinOutput();

  printf ("Write 128 bytes from eeprom.datr to eeprom\n");
  fp = fopen("eeprom.dat", "r");
  fread(block, 1, 128, fp);
  fclose(fp);
  writeAudinLow();
  for (i = 0; i < 20; i++) {
    if (writeeeprom(i, block[i]) == 0) {
        printf("%02x ", block[i]);
    }
  }
  
  return 0 ;
}

