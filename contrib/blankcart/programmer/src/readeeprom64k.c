/*
 * Read a Lynx eeprom 24aa512
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(5, OUTPUT); // OE/ pin18
  pinMode(6, OUTPUT); // WR/ pin22
}

void outputEnable()
{
  digitalWrite(5, LOW);
  delay(1); // 20us
}

void outputDisable()
{
  digitalWrite(5, HIGH);
}

void writeEnable()
{
  digitalWrite(6, LOW);
}

void writeDisable()
{
  digitalWrite(6, HIGH);
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  pullUpDnControl(4, PUD_UP);
  delay(1); // 20us
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
  delay(1); // 20us
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
  //printf("0 ");
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
  //printf("1 ");
}

unsigned char readAudin()
{
    if (digitalRead(4) == HIGH) {
        //printf("H ");
        return 1;
    } else {
        //printf("L ");
        return 0;
    }
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  writereg(0, 0, 0xff); // IODIRA
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

void dumpRegisters()
{
  int i;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(0, i);
  }
  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(1, i);
  }
}

void writeAddr(int addr)
{
  writereg(0, 0x19, addr & 0xff); // GPIOB
  //printf("%02x ", readreg(0, 0x19));
}

void setClkHigh()
{
    writeAddr(0x0002);
}

void setClkLow()
{
    writeAddr(0x0000);
}

void sendeeprombit(char bit)
{
   setAudinOutput();
   if (bit == 0) {
       writeAudinLow();
   } else {
       writeAudinHigh();
   }
   setClkHigh();
   // Data or address valid
   setClkLow();
}

char readeeprombit()
{
    char val;
    setAudinInput();
    setClkHigh();
    val = readAudin();
    setClkLow();
    return val;
}

void sendeepromstart()
{
   setAudinOutput();
   writeAudinHigh();
   setClkHigh();
   writeAudinLow();
   setClkLow();
}

void sendeepromstop()
{
   setAudinOutput();
   writeAudinLow();
   setClkHigh();
   writeAudinHigh();
   setClkLow();
}

char sendeepromread()
{
   sendeepromstart();
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(1);
   if (readeeprombit() == 1) {
      sendeepromstop();
      return 1;
   }
   return 0;
}

char sendeepromwrite()
{
   sendeepromstart();
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(1);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   sendeeprombit(0);
   if (readeeprombit() == 1) {
      sendeepromstop();
      return 1;
   }
   return 0;
}

char sendeepromaddr(int addr)
{
   if (sendeepromwrite() == 0) {
       sendeeprombit((addr >> 15) & 1);
       sendeeprombit((addr >> 14) & 1);
       sendeeprombit((addr >> 13) & 1);
       sendeeprombit((addr >> 12) & 1);
       sendeeprombit((addr >> 11) & 1);
       sendeeprombit((addr >> 10) & 1);
       sendeeprombit((addr >> 9) & 1);
       sendeeprombit((addr >> 8) & 1);
       if (readeeprombit() == 1) {
           sendeepromstop();
           return 1;
       }
       sendeeprombit((addr >> 7) & 1);
       sendeeprombit((addr >> 6) & 1);
       sendeeprombit((addr >> 5) & 1);
       sendeeprombit((addr >> 4) & 1);
       sendeeprombit((addr >> 3) & 1);
       sendeeprombit((addr >> 2) & 1);
       sendeeprombit((addr >> 1) & 1);
       sendeeprombit(addr & 1);
       if (readeeprombit() == 1) {
           sendeepromstop();
           return 1;
       }
       return 0;
   }
   return 1;
}

unsigned char readeeprom(int addr, unsigned char *val)
{
    *val = 0;
    sendeepromstop();
    if (sendeepromaddr(addr) == 0) {
        if (sendeepromread() == 0) {
            *val |= readeeprombit() << 7;
            *val |= readeeprombit() << 6;
            *val |= readeeprombit() << 5;
            *val |= readeeprombit() << 4;
            *val |= readeeprombit() << 3;
            *val |= readeeprombit() << 2;
            *val |= readeeprombit() << 1;
            *val |= readeeprombit();
            delay(10); // 20us
            if (readeeprombit() == 0) {
                return 1;
            }
            sendeepromstop();
            return 0;
        }
    }
    return 1;
}

int main (int argc, char *argv[])
{
  int i;
  unsigned char block[64 * 1024];
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  writeDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  writeAddr(0x0000);
  setAudinOutput();
  writeAudinHigh();

  printf ("Read 64k eeprom to eeprom.dat\n");
  for (i = 0; i < 128; i++) {
    if (readeeprom(i, &block[i]) == 0) {
        printf("%02x ", block[i]);
    }
  }
  fp = fopen("eeprom.dat", "w");
  fwrite(block, 1, 128, fp);
  fclose(fp);
  
  return 0 ;
}

