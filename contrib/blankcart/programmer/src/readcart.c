/*
 * Read a Lynx cart
 */

#include <unistd.h>
#include <stdint.h>
#include <sys/time.h>
#include "common.h"
#include "console.h"
#include "lnxheader.h"

static int argShowInfoOnly = 0;
static int argAutoBlockSize = 0;
static int audinused = 0;
static int cartmode = 128;
static char* romFile = 0;
struct timeval startTime, curTime;

unsigned char readByte(int addr)
{
  unsigned char b;

  outputDisable();
  writereg(0, REG_GPIOB, addr & 0xff);
  writereg(1, REG_GPIOA, (addr >> 8) & 0xff);
  writereg(1, REG_GPIOB, (addr >> 16) & 0xff);
  outputEnable();
  b = readreg(0, REG_GPIOA);

  return b;
}

/**
 * Shows the help/usage summary and exits.
 */
void showHelp() {
  echoln(NORMAL, "Usage: readcart -a|-bN [-h] [-nohdr] [-swvcc] [-turbo] [-v|-vv] [-i|<romfile>]");
  echoln(NORMAL, "\t-a\tAutomatically determine block size");
  echoln(NORMAL, "\t-bN\tManually set block size to N, valid values: 512, 1024, 2048, 1024x2");
  echoln(NORMAL, "\t-h\tShow this summary and exit");
  echoln(NORMAL, "\t-i\tShow cart information and exit (implies -a)");
  echoln(NORMAL, "\t-nohdr\tDo not write the LNX file header to romfile");
  echoln(NORMAL, "\t-swvcc\tEnable SWVCC to allow reading certain cart types");
  echoln(NORMAL, "\t-turbo\tUse turbo mode, may produce corrupt dumps");
  echoln(NORMAL, "\t-v\tSet verbose mode");
  echoln(NORMAL, "\t-vv\tSet verbose debug mode");

  cleanUpAndExit(0);
}

/**
 * Processes command line arguments and sets global variables to reflect the
 * chosen options.
 */
void processArgs(int argc, char *argv[])
{
  int i;

  if (argc == 1) {
    showHelp();
    return;
  }

  for (i = 1; i < argc; i++) {
    // dash options
    if (argv[i][0] == '-') {
      if (!strcmp("-h", argv[i]) || !strcmp("--help", argv[i])) { showHelp(); break; }
      else if (!strncmp("-b", argv[i], 2)) {      
        if (!strcmp("512", argv[i] + 2)) {
          cartmode = 128;
          blocksize = 512;
        } 
        else if (!strcmp("1024", argv[i] + 2)) {
          cartmode = 256;
          blocksize = 1024;
        }
        else if (!strcmp("2048", argv[i] + 2)) {
          cartmode = 512;
          blocksize = 2048;
        }
        else if (!strcmp("1024x2", argv[i] + 2)) {
          cartmode = 512;
          blocksize = 2048;
          audinused = 1;
        }
        else {
          echoln(NORMAL, "Unrecognised block size");
          exitStatus = EXIT_FAILURE;
          cleanUpAndExit(0);
        }
      }
      else if (!strcmp("-a", argv[i])) argAutoBlockSize = 1;
      else if (!strcmp("-i", argv[i])) { argShowInfoOnly = 1; argAutoBlockSize = 1; }
      else if (!strcmp("-nohdr", argv[i])) argNoHeader = 1;
      else if (!strcmp("-swvcc", argv[i])) argEnableSwVCC = 1;
      else if (!strcmp("-turbo", argv[i])) argTurboMode = 1;
      else if (!strcmp("-v", argv[i])) setEchoPriority(VERBOSE);
      else if (!strcmp("-vv", argv[i])) setEchoPriority(DEBUG);
    }
    // rom file name
    else if (romFile == 0) {
      int len = strlen(argv[i]);
      romFile = malloc(len + 1);
      strncpy(romFile, argv[i], len);
    }
  }

  if (blocksize == 0 && argAutoBlockSize != 1) {
    echoln(NORMAL, "Block size not provided or auto block size not set, run `readcart -h` for usage");
    exitStatus = EXIT_FAILURE;
    cleanUpAndExit(0);
  }

  if (romFile == 0 && argShowInfoOnly == 0) {
    echoln(NORMAL, "ROM file name not provided, run `readcart -h` for usage");
    exitStatus = EXIT_FAILURE;
    cleanUpAndExit(0);
  }
}

/**
 * Reads the first 2048 bytes of the Lynx cart to determine block size and
 * AUDIN enable automatically.
 */
void determineBlockSize() {
  int i;
  unsigned char block[2048];
  unsigned char block2[2048];

  echoln(VERBOSE, "Determining cart block size");

  writeAudinLow();
  for (i = 0; i < 2048; i++) block[i] = readByte(i);
  writeAudinHigh();
  for (i = 0; i < 2048; i++) block2[i] = readByte(i);
  for (i = 0; i < 2048; i++) if (block[i] != block2[i]) audinused = 1;

  if (audinused) {
    cartmode = 512;
    blocksize = 2048;
  }
  else {
    for (i = 0; i < 16; i++) {
      if (block[i] != block[i + 512]) {
          if (cartmode == 128) {
            cartmode = 256;
            blocksize = 1024;
          }
      }
      if (block[i] != block[i + 1024]) {
        cartmode = 512;
        blocksize = 2048;
      }
    }
  }
}

/**
 * Writes a dummy LNX file header. The header will contain correct block size
 * information but no other relevant details about the cart. 
 */
void writeHeader(FILE *fp)
{
  LXN_HDR[4] = blocksize & 0xff;
  LXN_HDR[5] = (blocksize >> 8) & 0xff;

  fwrite(LXN_HDR, 1, 64, fp);
}

/**
 * Reads cart data block-by-block and writes it to file.
 */
void writeCartData(FILE *fp)
{
  int i;
  size_t blocknr;
  unsigned char block[2048];

  for (blocknr = 0; blocknr < 256; blocknr++) {
    gettimeofday(&curTime, NULL);

    echo(NORMAL, "\rReading block %3d / 256 [ %6.2f%% ] Elapsed time is %ld seconds",
      blocknr + 1,
      ((blocknr + 1)/ 256.f) * 100,
      ELAPSED_SECONDS
    );

    writeAudinLow();
    for (i = 0; i < blocksize; i++) {
      if (audinused) {
        if (i == blocksize/2) {
          writeAudinHigh();
        }
        if (i < blocksize/2) {
          block[i] = readByte(i + blocknr * 2048);
        } else {
          block[i] = readByte(i - (blocksize/2) + blocknr * 2048);
        }
      } else {
        block[i] = readByte(i + blocknr * 2048);
      }
    }
    fwrite(block, 1, blocksize, fp);
  }
}

/**
 * Main method for readcart.
 */
int main(int argc, char *argv[])
{
  FILE *fp;
  unsigned char flashInfo[2];

  gettimeofday(&startTime, NULL);  
  processArgs(argc, argv);
  signal(SIGINT, cleanUpAndExit);
  initialise();

  if (argTurboMode) {
    echoln(VERBOSE, "Turbo mode enabled");
  }
  if (argEnableSwVCC) {
    echoln(VERBOSE, "Cart SWVCC enabled");
    swvccEnable();
  }
  if (argAutoBlockSize || argShowInfoOnly) {
    determineBlockSize();
  }

  echoln(NORMAL, "Cart size %dk with %db block size", cartmode, blocksize);
  if (audinused) {
    echoln(NORMAL, "Using AUDIN as address line", cartmode, blocksize);
  }

  getFlashInfo(flashInfo);

  if (!argShowInfoOnly) {
    echoln(NORMAL, "Reading Lynx cart data to file %s", romFile);

    // open file and write the header if requested
    fp = fopen(romFile, "w");
    if (!argNoHeader) {
      echoln(VERBOSE, "Writing LNX header");
      writeHeader(fp);
    }
    else echoln(VERBOSE, "NOT Writing LNX header");

    // write cart data and close the file
    writeCartData(fp);
    fclose(fp);

    gettimeofday(&curTime, NULL);
    echoln(NORMAL, "\nRead complete, time taken was %ld seconds", ELAPSED_SECONDS);
  }

  cleanUpAndExit(0);
  return 0;
}
