/*
 * Read a Lynx cart
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

//#include <mcp23s17.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

int rom = 0;

void setControlOutputs()
{
  pinMode(4, OUTPUT); // AUDIN chip select
  digitalWrite(4, LOW);
  pinMode(5, OUTPUT); // OE/ pin18
  digitalWrite(5, HIGH);
  pinMode(6, OUTPUT); // OE2/ pin22
  digitalWrite(6, HIGH);
  pinMode(25, OUTPUT); // WR/ pin26
  digitalWrite(25, HIGH);
}

void outputEnable()
{
  digitalWrite(25, HIGH);
  switch (rom) {
  case 0:
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 2:
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 1:
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  case 3:
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  }
}

void outputDisable()
{
  switch (rom) {
  case 0:
  case 1:
    digitalWrite(4, LOW);
    break;
  case 2:
  case 3:
    digitalWrite(4, HIGH);
    break;
  }
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(25, HIGH);
}

void writeEnable()
{
  digitalWrite(25, LOW);
  switch (rom) {
  case 0:
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 2:
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    break;
  case 1:
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  case 3:
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    break;
  }
}

void writeDisable()
{
  switch (rom) {
  case 0:
  case 1:
    digitalWrite(4, LOW);
    break;
  case 2:
  case 3:
    digitalWrite(4, HIGH);
    break;
  }
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(25, HIGH);
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
}

void writeAudinHigh()
{
  digitalWrite(4, HIGH);
}

void writeAudinLow()
{
  digitalWrite(4, LOW);
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  //printf("AUDIN = in\n");
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  //printf("%02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  if (status) status = 0;
  //printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
  return buf[2];
}

void setChipModes()
{
  int chip;
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  writereg(0, 0, 0xff); // IODIRA
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

unsigned char readByte(int addr)
{
  outputDisable();
  writereg(0, 0x19, addr & 0xff); // GPIOB
  writereg(1, 0x09, (addr >> 8) & 0xff); // GPIOA
  writereg(1, 0x19, (addr >> 16) & 0xff); // GPIOB
  outputEnable();
  return readreg(0, 0x09); // GPIOA
}

void dumpRegisters()
{
  int i;

  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(0, i);
  }
  printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    readreg(1, i);
  }
}


void writeAddrAndData(int addr, unsigned char data)
{
  writeDisable();
  writereg(0, 0x19, addr & 0xff); // GPIOB
  writereg(1, 0x09, (addr >> 8) & 0xff); // GPIOA
  writereg(1, 0x19, (addr >> 16) & 0xff); // GPIOB
  writeEnable();
  writereg(0, 0x09, data); // GPIOA
}

void byteProgram(int addr, unsigned char data)
{
    outputDisable();
    writeAddrAndData(0x5555, 0xAA);
    writeAddrAndData(0x2AAA, 0x55);
    writeAddrAndData(0x5555, 0xA0);
    writeAddrAndData(addr, data);
    writeDisable();
    delay(1); // 20us
}

void chipErase()
{
    outputDisable();
    writeAddrAndData(0x5555, 0xAA);
    writeAddrAndData(0x2AAA, 0x55);
    writeAddrAndData(0x5555, 0x80);
    writeAddrAndData(0x5555, 0xAA);
    writeAddrAndData(0x2AAA, 0x55);
    writeAddrAndData(0x5555, 0x10);
    writeDisable();
    delay(100); // 100ms
}

int main (int argc, char *argv[])
{
  int i;
  size_t blocknr;
  unsigned char block[2048];
  unsigned char block2[2048];
  char hdr[64];
  int cartmode = 128;
  int blocksize = 512;
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 16000000); // up to 32000000
  setControlOutputs();
  outputDisable();
  outputEnable();
  setChipModes();
  setDataInputs();
  setAddressOutputs();
  setAudinOutput();
  writeAudinLow();

  printf ("Read Lynx 2m cart to cart2m.lnx\n");
  rom = 0;
  cartmode = 512;
  blocksize = 2048;
  fp = fopen("cart2m.lnx", "w");
  strcpy(hdr, "LYNX");
  hdr[4] = blocksize & 0xff;
  hdr[5] = (blocksize >> 8) & 0xff;
  hdr[6] = 0;
  hdr[7] = 0;
  hdr[8] = 1; // Version nr
  hdr[9] = 0;
  strcpy(hdr+10, "Cart name                       ");
  strcpy(hdr+42, "Manufacturer    ");
  hdr[58] = 0; // Rotation
  hdr[59] = 0; // Spare
  hdr[60] = 0; // Spare
  hdr[61] = 0; // Spare
  hdr[62] = 0; // Spare
  hdr[63] = 0; // Spare
  fwrite(hdr, 1, 64, fp);
  rom = 0;
  for (blocknr = 0; blocknr < 256; blocknr++) {
    printf("Rom %d Block %d\n", rom, blocknr);
    for (i = 0; i < blocksize; i++) {
        block[i] = readByte(i + blocknr * 2048);
    }
    fwrite(block, 1, blocksize, fp);
  }
  rom = 1;
  for (blocknr = 0; blocknr < 256; blocknr++) {
    printf("Rom %d Block %d\n", rom, blocknr);
    for (i = 0; i < blocksize; i++) {
        block[i] = readByte(i + blocknr * 2048);
    }
    fwrite(block, 1, blocksize, fp);
  }
  rom = 2;
  for (blocknr = 0; blocknr < 256; blocknr++) {
    printf("Rom %d Block %d\n", rom, blocknr);
    for (i = 0; i < blocksize; i++) {
        block[i] = readByte(i + blocknr * 2048);
    }
    fwrite(block, 1, blocksize, fp);
  }
  rom = 3;
  for (blocknr = 0; blocknr < 256; blocknr++) {
    printf("Rom %d Block %d\n", rom, blocknr);
    for (i = 0; i < blocksize; i++) {
        block[i] = readByte(i + blocknr * 2048);
    }
    fwrite(block, 1, blocksize, fp);
  }
  fclose(fp);
  
  return 0 ;
}

