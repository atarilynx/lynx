#include <stdio.h>
#include <stdarg.h>
#include "console.h"

static ECHO_LEVEL globalPriority = NORMAL;

/**
 * Sets global priority mode. Priority mode can only be increased. 
 */
void setEchoPriority(ECHO_LEVEL priority) {
  if (priority > globalPriority) globalPriority = priority;
}

/**
 * Prints text to the standard output and flushes immediatelly. No new line
 * character is emitted. Standard printf() formatting options are accepted.
 */
void echo(ECHO_LEVEL priority, const char *format, ...) {
  va_list args;
  if (priority > globalPriority) return;

  va_start(args, format);
  
  if (priority == DEBUG) printf("! ");
  
  vprintf(format, args);
  fflush(stdout);

  va_end(args);
}

/**
 * Prints a line of text to the standard output. Standard printf() formatting
 * options are accepted. 
 */
void echoln(ECHO_LEVEL priority, const char *format, ...) {
  va_list args;
  if (priority > globalPriority) return;

  va_start(args, format);
  
  if (priority == DEBUG) printf("! ");
  
  vprintf(format, args);
  printf("\n");

  va_end(args);
}