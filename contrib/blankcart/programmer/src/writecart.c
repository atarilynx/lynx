/*
  Program a Lynx cart
*/

#include <unistd.h>
#include <stdint.h>
#include <sys/time.h>
#include "common.h"
#include "console.h"

static int checkChipCompatible = 1;
static char* romFile = 0;
struct timeval startTime, curTime;

unsigned char readByte(int addr)
{
  unsigned char data, val, rval;
  writeDisable();

  if ((addr & 1024) == 0) {
    writeAudinLow();
  } else {
    writeAudinHigh();
  }
  val = addr & 0xff;
  writereg(0, 0x1a, val); // GPIOB
  rval = readreg(0, REG_GPIOB);
  if (rval != val) echoln(NORMAL, "Low addr was %02x expected %02x", rval, val); // GPIOB
  
  val = (addr >> 8) & 0xff;
  writereg(1, 0x0a, val); // GPIOA
  rval = readreg(1, REG_GPIOA);
  if (rval != val) echoln(NORMAL, "Middle address was %02x expected %02x", rval, val); // GPIOB
  
  val = (addr >> 16) & 0xff;
  writereg(1, 0x1a, val); // GPIOB
  rval = readreg(1, REG_GPIOB);
  if (rval != val) echoln(NORMAL, "High address was %02x expected %02x", rval, val); // GPIOB
  
  outputEnable();
  data = readreg(0, REG_GPIOA);
  outputDisable();
  return data;
}

void dumpRegisters()
{
  int i;
  unsigned char a;

  //printf("Chip 0\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(0, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
  //printf("Chip 1\n");
  for (i = 0; i < 0x1B; i++) {
    a = readreg(1, i);
    printf("%02x %02x ", i, a);
    switch (i) {
    case 0:
        printf("Data direction. 00 = output FF=input\n");
        break;
    case 5:
        printf("Config 101x1xx0\n");
        break;
    case 6:
        printf("Pullups. FF = pullups\n");
        break;
    case 9:
        printf("Input values.\n");
        break;
    case 10:
        printf("Output values.\n");
        break;
    default:
        printf("\n");
    }
  }
}

int byteProgram(int addr, unsigned char data)
{
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0xA0);
  writeAddrAndData(addr, data);
  return 0;
}

void chipErase()
{
  setDataOutputs();
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x80);
  writeAddrAndData(0x5555, 0xAA);
  writeAddrAndData(0x2AAA, 0x55);
  writeAddrAndData(0x5555, 0x10);
  delay(1000); // 100ms
}

unsigned char softwareId()
{
  unsigned char flashInfo[2];
  getFlashInfo(flashInfo);
  
  if ((flashInfo[0] == 0xbf) && (flashInfo[1] == 0xb7)) {
    return 1;
  } else {
    echoln(NORMAL, "Expected manufacturer ID bf, flash ID b7");

    if (checkChipCompatible) {
      echoln(NORMAL, "Inserted cartridge cannot be written to");
      return 0;
    }
    else {
      echoln(NORMAL, "Will attempt to write data to incompatible cart");
      return 1;
    }
  }
}

int readHeader(FILE *fp)
{
    int blocksize;
    unsigned char lnx_header[64];
    fread(lnx_header, 64, 1, fp);
    blocksize = lnx_header[4] | (lnx_header[5] << 8);
    return blocksize;
}

int healthCheckLocation(int addr, unsigned char val)
{
  byteProgram(addr, val);
  if (readByte(0) != 0xff) {
    echoln(NORMAL, "Addr %x not connected", addr);
    return 0;
  }
  if (readByte(addr) != val) {
    echoln(NORMAL, "Cannot program address %x", addr);
    return 0;
  }
  return 1;
}

int healthCheck()
{
  chipErase();
  if (!healthCheckLocation(256*1024, 66)) return 0;
  if (!healthCheckLocation(128*1024, 34)) return 0;
  if (!healthCheckLocation(64*1024, 18)) return 0;
  if (!healthCheckLocation(32*1024, 7)) return 0;
  if (!healthCheckLocation(16*1024, 129)) return 0;
  if (!healthCheckLocation(8*1024, 65)) return 0;
  if (!healthCheckLocation(4*1024, 33)) return 0;
  if (!healthCheckLocation(2*1024, 17)) return 0;
  if (!healthCheckLocation(1024, 9)) return 0;
  if (!healthCheckLocation(512, 5)) return 0;
  if (!healthCheckLocation(256, 3)) return 0;
  if (!healthCheckLocation(128, 128)) return 0;
  if (!healthCheckLocation(64, 64)) return 0;
  if (!healthCheckLocation(32, 32)) return 0;
  if (!healthCheckLocation(16, 16)) return 0;
  if (!healthCheckLocation(8, 8)) return 0;
  if (!healthCheckLocation(4, 4)) return 0;
  if (!healthCheckLocation(2, 2)) return 0;
  if (!healthCheckLocation(1, 1)) return 0;
  return 1;
}

/**
 * Shows the help/usage summary and exits.
 */
void showHelp() {
  echoln(NORMAL, "Usage: writecart [-nc] [-nohdr -bN] [-turbo] [-v|-vv] <romfile>");
  echoln(NORMAL, "\t-bN\tManually set block size to N, valid values: 512, 1024, 2048");
  echoln(NORMAL, "\t-h\tShow this summary and exit");
  echoln(NORMAL, "\t-nc\tDo not check if flash chip is compatible - use carefully!");
  echoln(NORMAL, "\t-nohdr\tTreat input ROM as headerless, must provide -bN option");
  echoln(NORMAL, "\t-swvcc\tEnable SWVCC to allow writing certain cart types");
  echoln(NORMAL, "\t-turbo\tUse turbo mode, may produce corrupt carts");
  echoln(NORMAL, "\t-v\tSet verbose mode");
  echoln(NORMAL, "\t-vv\tSet verbose debug mode");

  cleanUpAndExit(0);
}

/**
 * Processes command line arguments and sets global variables to reflect the
 * chosen options.
 */
void processArgs(int argc, char *argv[])
{
  int i;

  if (argc == 1) {
    showHelp();
    return;
  }

  for (i = 1; i < argc; i++) {
    // dash options
    if (argv[i][0] == '-') {
      if (!strcmp("-h", argv[i]) || !strcmp("--help", argv[i])) { showHelp(); break; }
      else if (!strncmp("-b", argv[i], 2)) {      
        if (!strcmp("512", argv[i] + 2)) {
          blocksize = 512;
        } 
        else if (!strcmp("1024", argv[i] + 2)) {
          blocksize = 1024;
        }
        else if (!strcmp("2048", argv[i] + 2)) {
          blocksize = 2048;
        }
        else {
          echoln(NORMAL, "Unrecognised block size");
          exitStatus = EXIT_FAILURE;
          cleanUpAndExit(0);
        }
      }
      else if (!strcmp("-nc", argv[i])) checkChipCompatible = 0;
      else if (!strcmp("-nohdr", argv[i])) argNoHeader = 1;
      else if (!strcmp("-swvcc", argv[i])) argEnableSwVCC = 1;
      else if (!strcmp("-turbo", argv[i])) argTurboMode = 1;
      else if (!strcmp("-v", argv[i])) setEchoPriority(VERBOSE);
      else if (!strcmp("-vv", argv[i])) setEchoPriority(DEBUG);
    }
    // rom file name
    else if (romFile == 0) {
      int len = strlen(argv[i]);
      romFile = malloc(len + 1);
      strncpy(romFile, argv[i], len);
    }
  }

  if (blocksize == 0 && argNoHeader == 1) {
    echoln(NORMAL, "Block size not provided in headerless mode, run `readcart -h` for usage");
    exitStatus = EXIT_FAILURE;
    cleanUpAndExit(0);
  }

  if (romFile == 0) {
    echoln(NORMAL, "ROM file name not provided, run `programcart -h` for usage");
    exitStatus = EXIT_FAILURE;
    cleanUpAndExit(0);
  }
}

int main (int argc, char *argv[])
{
  int i, addr;
  size_t blocknr;
  
  gettimeofday(&startTime, NULL);
  processArgs(argc, argv);
  signal(SIGINT, cleanUpAndExit);
  setEchoPriority(NORMAL);
  initialise();

  if (argTurboMode) {
    echoln(VERBOSE, "Turbo mode enabled");
  }
  if (argEnableSwVCC) {
    echoln(VERBOSE, "Cart SWVCC enabled");
    swvccEnable();
  }

  //dumpRegisters();
  if (softwareId()) {
    FILE *fp;
    unsigned char block[2048];
    echoln(NORMAL, "Flashing Lynx cart from %s", romFile);
    fp = fopen(romFile, "r"); // Open lnx file

    if (!argNoHeader) {
      echoln(VERBOSE, "Reading LNX header");
      blocksize = readHeader(fp); // Read header

      // make sure the header has a valid block size set
      if (blocksize != 512 && blocksize != 1024 && blocksize != 2048) {
        echoln(NORMAL, "Invalid block size in ROM header - %d", blocksize);
        exitStatus = EXIT_FAILURE;
        fclose(fp);
        cleanUpAndExit(0);
      }
    }
    echoln(NORMAL, "Block size is %d", blocksize);
    
    if (healthCheck()) {
      chipErase();
      addr = 0;
      blocknr = 0;
      while (blocknr < 256) {
        gettimeofday(&curTime, NULL);
        echo(NORMAL, "\rWriting block %3d / 256 [ %6.2f%% ] Elapsed time is %ld seconds",
          blocknr + 1,
          ((blocknr + 1)/ 256.f) * 100,
          ELAPSED_SECONDS
        );
        fflush(stdout);

        fread(block, blocksize, 1, fp);
        for (i = 0; i < 2048; i++) {
          if (i < blocksize) {
            if (byteProgram(addr, block[i])) {
              return 1;
            }
          }
          addr++;
        }
        blocknr++;
      }
    }
    fclose(fp);

    gettimeofday(&curTime, NULL);
    echoln(NORMAL, "\nWrite complete, time taken was %ld seconds", ELAPSED_SECONDS);

    cleanUpAndExit(0);
  }

  exitStatus = EXIT_FAILURE;
  cleanUpAndExit(0);
}
