/*
  Test that the cart connector pins are now shorted to each other
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}

void setControlOutputs()
{
  pinMode(4, OUTPUT); // AUDIN pin18
  pinMode(5, OUTPUT); // OE/ pin18
  pinMode(6, OUTPUT); // OE2/ pin22
  pinMode(25, OUTPUT); // WR state
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(25, HIGH);
}

void outputDisable()
{
  digitalWrite(5, HIGH);
  printf("OE* = 1\n");
}

void writeEnable()
{
  digitalWrite(6, LOW);
  printf("WE* = 0\n");
}

void writeDisable()
{
  digitalWrite(6, HIGH);
  printf("WE* = 1\n");
}

void setAudinInput()
{
  pinMode(4, INPUT); // AUDIN pin16
  printf("AUDIN = in\n");
}

void setAudinOutput()
{
  pinMode(4, OUTPUT); // AUDIN pin16
  //printf("AUDIN = out\n");
}

void writereg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  int i;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  status = wiringPiSPIDataRW(0, buf, 3);
}

void writeregdbg(int chip, int reg, unsigned char data)
{
  unsigned char buf[3];
  int status;
  int i;
  buf[0] = (0x40 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = data;
  printf("w %02x, %02x, %02X - ", buf[0], buf[1], buf[2]);
  status = wiringPiSPIDataRW(0, buf, 3);
  printf("%d %02x, %02x, %02X\n", status, buf[0], buf[1], buf[2]);
}

unsigned char readreg(int chip, int reg)
{
  unsigned char buf[3];
  int status;
  buf[0] = (0x41 | (chip << 1)) & 0xff;
  buf[1] = reg & 0xff;
  buf[2] = 0;
  status = wiringPiSPIDataRW(0, buf, 3);
  return buf[2];
}

void setChipModes()
{
  int chip;
  //printf("Set chip modes\n");
  // Set BANK=1 no flip flop of addresses
  for (chip = 0; chip < 8; chip++) {
    writereg(chip, 0xa, 0xa8); // IOCON
  }
}

void setDataInputs()
{
  printf("Set data inputs\n");
  writereg(0, 0, 0xff); // IODIRA
  printf("Set input pullups\n");
  writereg(0, 6, 0xff); // IODIRA
}

void setDataOutputs()
{
  //printf("Set data outputs\n");
  writereg(0, 0, 0); // IODIRA
}

void setAddressOutputs()
{
  //printf("Set address outputs\n");
  writereg(0, 0x10, 0); // IODIRB
  writereg(1, 0, 0); // IODIRA
  writereg(1, 0x10, 0); // IODIRB
}

void bittest(int addr, unsigned char data, unsigned char directpins)
{
  unsigned char realdata;
  unsigned char a0, a1, a2, a3, b0, b1, b2, b3;
  a0 = (addr >>16) & 0xff;
  a1 = (addr >> 8) & 0xff;
  a2 = addr & 0xff;
  a3 = data;
  writereg(0, 0x1a, addr & 0xff); // GPIOB
  writereg(1, 0x0a, (addr >> 8) & 0xff); // GPIOA
  writereg(1, 0x1a, (addr >> 16) & 0xff); // GPIOB
  writereg(0, 0x0a, data); // GPIOA
  delay(1); // 20us
  b0 = readreg(1, 0x19);
  b1 = readreg(1, 0x09);
  b2 = readreg(0, 0x19);
  b3 = readreg(0, 0x09);
  if ((a0 != b0) || (a1 != b1) || (a2 != b2) || (a3 != b3)) {
      printf("%02x%02x%02x%02x ", a0, a1, a2, a3);
      printf("%02x%02x%02x%02x\n", b0, b1, b2, b3);
  }
}

int main (void)
{
  int i;
  size_t blocknr;
  unsigned char block[2048];
  unsigned char hdr[64];
  int cartmode = 128;
  int blocksize = 512;
  FILE *fp;

  wiringPiSetup () ;
  wiringPiSPISetup(0, 8000000); // up to 32000000
  setControlOutputs();
  setAudinOutput();
  setChipModes();
  setAddressOutputs();

  setDataOutputs();
  printf ("Check if a pin is shorted to its neighbour\n") ;
  bittest(0x0000, 0x08, 0); // D3
  bittest(0x0000, 0x04, 0); // D2
  bittest(0x0000, 0x10, 0); // D4
  bittest(0x0000, 0x02, 0); // D1
  bittest(0x0000, 0x20, 0); // D5
  bittest(0x0000, 0x01, 0); // D0
  bittest(0x0000, 0x40, 0); // D6
  bittest(0x0000, 0x80, 0); // D7
  bittest(0x0000, 0x00, 1); // OE
  bittest(0x0002, 0x00, 0); // A1
  bittest(0x0004, 0x00, 0); // A2
  bittest(0x0008, 0x00, 0); // A3
  bittest(0x0040, 0x00, 0); // A6
  bittest(0x0010, 0x00, 0); // A4
  bittest(0x0020, 0x00, 0); // A5
  bittest(0x0001, 0x00, 0); // A0
  bittest(0x0080, 0x00, 0); // A7
  bittest(0x8000, 0x00, 0); // A16
  bittest(0x10000, 0x00, 0); // A17
  bittest(0x20000, 0x00, 0); // A18
  bittest(0x40000, 0x00, 0); // A19
  bittest(0x4000, 0x00, 0); // A15
  bittest(0x2000, 0x00, 0); // A14
  bittest(0x1000, 0x00, 0); // A13
  bittest(0x0800, 0x00, 0); // A12
  bittest(0x0000, 0x00, 2); // WE
  bittest(0x0100, 0x00, 0); // A8
  bittest(0x0200, 0x00, 0); // A9
  bittest(0x0400, 0x00, 0); // A10
  bittest(0x0000, 0x00, 4); // AUDIN
  return 0 ;
}

