#!/bin/bash

# This script is used to test a cart programmer board by writing a ROM
# to a blank cart and then reading the ROM back and comparing the results.

ROM=rand512k.dat
READ=./readcart
WRITE=./writecart

gpio -1 mode 11 out
gpio -1 mode 13 out

gpio -1 write 11 1
gpio -1 write 13 1
sleep 1
gpio -1 write 11 0
gpio -1 write 13 0
sleep 1
rm -f cart.lnx

echo "WRITING ROM"
gpio -1 write 11 1
$WRITE -vv -turbo -b2048 -nohdr $ROM

if [ "$?" == "1" ]
then
  exit
fi

gpio -1 write 11 0

gpio -1 write 13 1
$READ -vv -b2048 -turbo -nohdr cart.tst
gpio -1 write 13 0

MD51=$(md5sum cart.tst|cut -d " " -f1)
MD52=$(md5sum $ROM|cut -d " " -f1)

echo "CART MD5     : " $MD51
echo "CART EXPECTED: " $MD52

if [[ $MD51 == $MD52 ]]
then
	echo TEST PASS
else
	echo TEST FAIL
fi

rm cart.tst
