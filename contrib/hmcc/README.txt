This hmcc is just a straight compilation on a Linux like machine.
The original version was Windows only and it failed running on my wine64 so I decided to compile it natively instead.

Compile it:
gcc -o hmcc *.c -lm

Install it:
sudo cp hmcc /usr/local/bin

Use it:

Add to Common.mk
HMCC=hmcc

--
Karri

All credits go to @TailChao Osman Celimli
