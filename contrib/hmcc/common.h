/****************************************************************
 * common.h
 * Last change 03-14-2012 by Osman Celimli
 ****************************************************************/
/****************************************************************
 * Important Defines
 ****************************************************************/

typedef struct u16p8
{
 char loByte,hiByte,decByte;
} u16p8;

/****************************************************************
 * Function Prototypes
 ****************************************************************/
int parse16p8(struct u16p8 *dest, char *src);
int parseBinString(char *src);
char* altFileExt(char *inFileName,char *newExt);
