# README #

In order to start using the tools within this repository the easiest way is to go to the Download section and get the complete binaries to install on your computer.

You can also create the tools from sources.

### What is this repository for? ###

This repository contains a snapshot of good working development tools for the Atari Lynx. The idea is to have a stable set of tools that are reliable. It helps you concentrate on  building the games without having to debug the tools at the same time.

* Quick summary
  the cc65 toolchain is for programming in C-language
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  apt-get install git checkinstall mednafen
* Configuration
  git clone https://karri@bitbucket.org/atarilynx/lynx.git
  cd lynx/tools
  sudo make -f Makefile.deb
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* contact karri@sipo.fi for help, access to repository or suggestions
* Other community or team contact